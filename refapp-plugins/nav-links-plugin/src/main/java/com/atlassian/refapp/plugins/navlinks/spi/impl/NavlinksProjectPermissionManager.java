package com.atlassian.refapp.plugins.navlinks.spi.impl;

import com.atlassian.plugins.navlink.spi.Project;
import com.atlassian.plugins.navlink.spi.ProjectPermissionManager;

/**
 * Dummy implementation, as Refapp doesn't allow maintenance of Custom Content Links at present
 */
public class NavlinksProjectPermissionManager implements ProjectPermissionManager {
    @Override
    public boolean canAdminister(Project project, String s) {
        return false;
    }
}
