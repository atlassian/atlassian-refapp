package com.atlassian.refapp.plugins.navlinks.spi.impl;

import com.atlassian.plugins.navlink.spi.weights.ApplicationWeights;

/**
 * {@link com.atlassian.plugins.navlink.spi.weights.ApplicationWeights} implementation for Refapp. Returns 1000.
 *
 * @since 2.20
 */
public final class RefappWeights implements ApplicationWeights {
    @Override
    public int getApplicationWeight() {
        return 1000;
    }
}