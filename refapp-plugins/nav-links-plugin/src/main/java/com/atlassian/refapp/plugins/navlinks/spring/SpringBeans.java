package com.atlassian.refapp.plugins.navlinks.spring;

import com.atlassian.plugins.navlink.spi.ProjectManager;
import com.atlassian.plugins.navlink.spi.ProjectPermissionManager;
import com.atlassian.plugins.navlink.spi.weights.ApplicationWeights;
import com.atlassian.refapp.plugins.navlinks.spi.impl.NavlinksProjectManager;
import com.atlassian.refapp.plugins.navlinks.spi.impl.NavlinksProjectPermissionManager;
import com.atlassian.refapp.plugins.navlinks.spi.impl.RefappWeights;
import org.osgi.framework.ServiceRegistration;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.atlassian.plugins.osgi.javaconfig.ExportOptions.as;
import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.exportOsgiService;

/**
 * Transformerless spring bean configuration.
 * See <a href="https://developer.atlassian.com/server/framework/atlassian-sdk/spring-java-config/">Spring Java Config</a>
 * @since 5.1.3
 */
@Configuration
public class SpringBeans {
    @Bean
    public ApplicationWeights weights() {
        return new RefappWeights();
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportApplicationWeights(final ApplicationWeights weights) {
        return exportOsgiService(weights, as(ApplicationWeights.class));
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportProjectManager(final ProjectManager projectManager) {
        return exportOsgiService(projectManager, as(ProjectManager.class));
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportProjectPermissionManager(final ProjectPermissionManager projectPermissionManager) {
        return exportOsgiService(projectPermissionManager, as(ProjectPermissionManager.class));
    }

    @Bean
    public ProjectManager projectManager() {
        return new NavlinksProjectManager();
    }

    @Bean
    public ProjectPermissionManager projectPermissionManager() {
        return new NavlinksProjectPermissionManager();
    }
}
