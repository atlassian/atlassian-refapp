package com.atlassian.refapp.trustedapps.client.spring;

import com.atlassian.sal.api.net.RequestFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.importOsgiService;

@Configuration
public class SpringBeans {
    @Bean
    public RequestFactory requestFactory() {
        return importOsgiService(RequestFactory.class);
    }
}
