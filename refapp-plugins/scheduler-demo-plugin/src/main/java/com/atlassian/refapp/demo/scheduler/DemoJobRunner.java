package com.atlassian.refapp.demo.scheduler;

import com.atlassian.scheduler.JobRunner;
import com.atlassian.scheduler.JobRunnerRequest;
import com.atlassian.scheduler.JobRunnerResponse;
import com.atlassian.scheduler.config.JobRunnerKey;
import com.atlassian.streams.api.ActivityObjectTypes;
import com.atlassian.streams.api.ActivityVerbs;
import com.atlassian.streams.refapp.api.StreamsActivityManager;
import com.atlassian.streams.refapp.api.StreamsEntryRequest;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.text.SimpleDateFormat;

/**
 * A demo job runner that visualize the schedule instances as streams entries.
 */
public class DemoJobRunner implements JobRunner
{
    public static JobRunnerKey RUNNER_KEY = JobRunnerKey.of("RefApp-Streams-Job-Runner");
    private static final String DATE_PATTERN = "dd-MMM-yyyy HH:mm:ss";

    private static final Logger log = LoggerFactory.getLogger(DemoJobRunner.class);

    private final StreamsActivityManager refappStreamsActivityManager;

    public DemoJobRunner(StreamsActivityManager refappStreamsActivityManager) {
        this.refappStreamsActivityManager = refappStreamsActivityManager;
    }

    @Nullable
    @Override
    public JobRunnerResponse runJob(JobRunnerRequest request) {
        final StreamsEntryRequest streamsEntryRequest = createStreamsEntryRequest(request);

        refappStreamsActivityManager.addEntry(streamsEntryRequest);

        log.debug("Activity streams id '{}' was fired", streamsEntryRequest.getId());

        return JobRunnerResponse.success("Successfully registered activity streams job");
    }

    private StreamsEntryRequest createStreamsEntryRequest(JobRunnerRequest request) {
        final DateTime startTime = new DateTime(request.getStartTime());
        final SimpleDateFormat dateFormatter = new SimpleDateFormat(DATE_PATTERN);

        return new StreamsEntryRequest()
                .id(startTime.hashCode())
                .title("triggered as per " + request.getJobConfig().getRunMode() + " job, at " + dateFormatter.format(request.getStartTime()))
                .postedDate(startTime.plusMinutes(2))
                .type(ActivityObjectTypes.article())
                .verb(ActivityVerbs.post())
                .user("Scheduler");
    }
}
