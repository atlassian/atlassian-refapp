package com.atlassian.refapp.demo.scheduler;

import com.atlassian.sal.api.lifecycle.LifecycleAware;
import com.atlassian.scheduler.SchedulerRuntimeException;
import com.atlassian.scheduler.SchedulerService;
import com.atlassian.scheduler.SchedulerServiceException;
import com.atlassian.scheduler.config.JobConfig;
import com.atlassian.scheduler.config.JobId;
import com.atlassian.scheduler.config.Schedule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

import static com.atlassian.scheduler.config.RunMode.RUN_LOCALLY;
import static com.atlassian.scheduler.config.RunMode.RUN_ONCE_PER_CLUSTER;

public class RefAppStreamsSchedulerActivator implements LifecycleAware {
    private static final Logger log = LoggerFactory.getLogger(RefAppStreamsSchedulerActivator.class);

    private final SchedulerService schedulerService;

    private final DemoJobRunner demoJobRunner;

    private static final JobId LOCAL_JOB_ID = JobId.of("Refapp-demo-local-schedule");
    private static final JobId CLUSTERED_JOB_ID = JobId.of("Refapp-demo-clustered-schedule");

    public RefAppStreamsSchedulerActivator(final SchedulerService schedulerService, final DemoJobRunner demoJobRunner) {
        this.schedulerService = schedulerService;
        this.demoJobRunner = demoJobRunner;
    }

    @Override
    public void onStart() {
        registerDemoSchedules();

    }

    @Override
    public void onStop() {
        schedulerService.unregisterJobRunner(DemoJobRunner.RUNNER_KEY);
        schedulerService.unscheduleJob(LOCAL_JOB_ID);
        schedulerService.unscheduleJob(CLUSTERED_JOB_ID);
    }

    private void registerDemoSchedules()
    {
        schedulerService.registerJobRunner(DemoJobRunner.RUNNER_KEY, demoJobRunner);

        try
        {
            JobConfig jobConfig = JobConfig.forJobRunnerKey(DemoJobRunner.RUNNER_KEY)
                    .withSchedule(Schedule.forInterval(60_000L, new Date()));

            // Schedule 2 jobs, one local and one clustered.
            schedulerService.scheduleJob(CLUSTERED_JOB_ID, jobConfig.withRunMode(RUN_ONCE_PER_CLUSTER));
            schedulerService.scheduleJob(LOCAL_JOB_ID, jobConfig.withRunMode(RUN_LOCALLY));

            log.debug("Demo schedules have been registered.");
        }
        catch (SchedulerServiceException sse)
        {
            log.error("Can't scheduler demo schedules.", sse);
            throw new SchedulerRuntimeException("Can't scheduler demo schedules.", sse);
        }
    }

}
