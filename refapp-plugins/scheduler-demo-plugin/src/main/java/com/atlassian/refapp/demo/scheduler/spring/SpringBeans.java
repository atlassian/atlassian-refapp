package com.atlassian.refapp.demo.scheduler.spring;

import com.atlassian.refapp.demo.scheduler.DemoJobRunner;
import com.atlassian.refapp.demo.scheduler.RefAppStreamsSchedulerActivator;
import com.atlassian.sal.api.lifecycle.LifecycleAware;
import com.atlassian.scheduler.SchedulerService;
import com.atlassian.streams.refapp.api.StreamsActivityManager;
import org.osgi.framework.ServiceRegistration;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.atlassian.plugins.osgi.javaconfig.ExportOptions.as;
import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.exportOsgiService;
import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.importOsgiService;

@Configuration
public class SpringBeans {

    @Bean
    public StreamsActivityManager importStreamsActivityManager() {
        return importOsgiService(StreamsActivityManager.class);
    }

    @Bean
    public SchedulerService importSchedulerService() {
        return importOsgiService(SchedulerService.class);
    }

    @Bean
    public DemoJobRunner demoJobRunner(StreamsActivityManager streamsActivityManager) {
        return new DemoJobRunner(streamsActivityManager);
    }

    @Bean
    public RefAppStreamsSchedulerActivator refAppStreamsSchedulerActivator(SchedulerService schedulerService, DemoJobRunner demoJobRunner) {
        return new RefAppStreamsSchedulerActivator(schedulerService, demoJobRunner);
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportStreamsSchedulerActivator(RefAppStreamsSchedulerActivator streamsSchedulerActivator) {
        return exportOsgiService(streamsSchedulerActivator, as(LifecycleAware.class));
    }
}
