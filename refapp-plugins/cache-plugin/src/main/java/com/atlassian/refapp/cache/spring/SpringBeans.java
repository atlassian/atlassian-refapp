package com.atlassian.refapp.cache.spring;

import com.atlassian.cache.CacheFactory;
import com.atlassian.cache.CacheManager;
import com.atlassian.cache.memory.MemoryCacheManager;
import org.osgi.framework.ServiceRegistration;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.atlassian.plugins.osgi.javaconfig.ExportOptions.as;
import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.exportOsgiService;

/**
 * Transformerless spring bean configuration.
 * See <a href="https://developer.atlassian.com/server/framework/atlassian-sdk/spring-java-config/">Spring Java Config</a>
 * @since 5.1.3
 */
@Configuration
public class SpringBeans {
    @Bean
    public CacheManager cacheManager() {
        return new MemoryCacheManager();
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportCacheManager(final CacheManager cacheManager) {
        return exportOsgiService(cacheManager, as(CacheManager.class, CacheFactory.class));
    }
}
