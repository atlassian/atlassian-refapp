package com.atlassian.refapp.trustedapps.internal;

import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.security.auth.trustedapps.ApplicationCertificate;
import com.atlassian.security.auth.trustedapps.UserResolver;

import java.security.Principal;

public class RefAppUserResolverImpl implements UserResolver {
    private final CrowdService crowdService;

    public RefAppUserResolverImpl(CrowdService crowdService) {
        this.crowdService = crowdService;
    }

    public Principal resolve(ApplicationCertificate certificate) {
        return crowdService.getUser(certificate.getUserName());
    }
}
