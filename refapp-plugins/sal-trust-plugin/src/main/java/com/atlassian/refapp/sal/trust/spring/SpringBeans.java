package com.atlassian.refapp.sal.trust.spring;

import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.refapp.trustedapps.internal.KeyFactory;
import com.atlassian.refapp.trustedapps.internal.RefAppTrustedApplicationsManagerImpl;
import com.atlassian.refapp.trustedapps.internal.RefAppUserResolverImpl;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.security.auth.trustedapps.BouncyCastleEncryptionProvider;
import com.atlassian.security.auth.trustedapps.EncryptionProvider;
import com.atlassian.security.auth.trustedapps.TrustedApplicationsConfigurationManager;
import com.atlassian.security.auth.trustedapps.TrustedApplicationsManager;
import com.atlassian.security.auth.trustedapps.UserResolver;
import org.osgi.framework.ServiceRegistration;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.atlassian.plugins.osgi.javaconfig.ExportOptions.as;
import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.exportOsgiService;
import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.importOsgiService;

/**
 * Transformerless spring bean configuration.
 * See <a href="https://developer.atlassian.com/server/framework/atlassian-sdk/spring-java-config/">Spring Java Config</a>
 *
 * @since 5.1.3
 */
@Configuration
public class SpringBeans {
    @Bean
    public EncryptionProvider encryptionProvider() {
        return new BouncyCastleEncryptionProvider();
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportEncryptionProvider(
            EncryptionProvider encryptionProvider
    ) {
        return exportOsgiService(encryptionProvider, as(EncryptionProvider.class));
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportTrustedApplicationsManager(
            TrustedApplicationsManager trustedApplicationsManager
    ) {
        return exportOsgiService(
                trustedApplicationsManager,
                as(TrustedApplicationsManager.class, TrustedApplicationsConfigurationManager.class)
        );
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportUserResolver(UserResolver userResolver) {
        return exportOsgiService(userResolver, as(UserResolver.class));
    }

    @Bean
    public KeyFactory keyFactory(EncryptionProvider encryptionProvider,
                                 PluginSettingsFactory pluginSettingsFactory) {
        return new KeyFactory(encryptionProvider, pluginSettingsFactory);
    }

    @Bean
    public PluginSettingsFactory pluginSettingsFactory() {
        return importOsgiService(PluginSettingsFactory.class);
    }

    @Bean
    public TrustedApplicationsManager trustedApplicationsManager(
            EncryptionProvider encryptionProvider,
            KeyFactory keyFactory,
            PluginSettingsFactory pluginSettingsFactory
                                                                ) {
        return new RefAppTrustedApplicationsManagerImpl(
                encryptionProvider, keyFactory, pluginSettingsFactory
        );
    }

    @Bean
    public UserResolver userResolver(CrowdService crowdService) {
        return new RefAppUserResolverImpl(crowdService);
    }

    @Bean
    public CrowdService crowdService() {
        return importOsgiService(CrowdService.class);
    }
}
