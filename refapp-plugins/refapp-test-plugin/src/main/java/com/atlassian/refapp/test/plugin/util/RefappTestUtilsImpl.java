package com.atlassian.refapp.test.plugin.util;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.refapp.api.EmbeddedCrowdAuthenticator;
import com.atlassian.sal.api.executor.ThreadLocalContextManager;

public class RefappTestUtilsImpl implements RefappTestUtils {
    private final EmbeddedCrowdAuthenticator embeddedCrowdAuthenticator;
    private final ThreadLocalContextManager<User> threadLocalContextManager;

    public RefappTestUtilsImpl(EmbeddedCrowdAuthenticator embeddedCrowdAuthenticator, ThreadLocalContextManager<User> threadLocalContextManager) {
        this.embeddedCrowdAuthenticator = embeddedCrowdAuthenticator;
        this.threadLocalContextManager = threadLocalContextManager;
    }

    public void runAsSysAdmin(Runnable action) {
        User user = embeddedCrowdAuthenticator.authenticateUser("admin", "admin");
        threadLocalContextManager.setThreadLocalContext(user);
        action.run();
        resetUserRole();
    }

    public void runAsAdmin(Runnable action) {
        User user = embeddedCrowdAuthenticator.authenticateUser("betty", "betty");
        threadLocalContextManager.setThreadLocalContext(user);
        action.run();
        resetUserRole();
    }

    public void runAsUser(Runnable action) {
        User user = embeddedCrowdAuthenticator.authenticateUser("barney", "barney");
        threadLocalContextManager.setThreadLocalContext(user);
        action.run();
        resetUserRole();
    }

    public void runAsUnlicensedUser(Runnable action) {
        User user = embeddedCrowdAuthenticator.authenticateUser("michell", "michell");
        threadLocalContextManager.setThreadLocalContext(user);
        action.run();
        resetUserRole();
    }

    private void resetUserRole() {
        threadLocalContextManager.clearThreadLocalContext();
    }

}
