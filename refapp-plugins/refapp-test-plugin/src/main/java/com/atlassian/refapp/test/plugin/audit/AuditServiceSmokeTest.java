package com.atlassian.refapp.test.plugin.audit;

import com.atlassian.audit.api.AuditEntityCursor;
import com.atlassian.audit.api.AuditQuery;
import com.atlassian.audit.api.AuditSearchService;
import com.atlassian.audit.api.AuditService;
import com.atlassian.audit.api.util.pagination.Page;
import com.atlassian.audit.api.util.pagination.PageRequest;
import com.atlassian.audit.entity.AuditEntity;
import com.atlassian.audit.entity.AuditEvent;
import com.atlassian.audit.entity.AuditType;
import com.atlassian.audit.entity.CoverageArea;
import com.atlassian.audit.entity.CoverageLevel;
import com.atlassian.functest.junit.SpringAwareTestCase;
import com.atlassian.refapp.test.plugin.util.RefappTestUtils;
import org.junit.Rule;
import org.junit.Test;

import javax.annotation.Nonnull;
import java.util.concurrent.TimeoutException;
import org.junit.contrib.java.lang.system.ProvideSystemProperty;
import org.slf4j.Logger;

import static java.lang.Thread.sleep;
import static java.util.Objects.requireNonNull;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.slf4j.LoggerFactory.getLogger;

public class AuditServiceSmokeTest extends SpringAwareTestCase {
    private static final Logger logger = getLogger(AuditServiceSmokeTest.class);
    private final AuditService auditService;
    private final AuditSearchService auditSearchService;
    private final RefappTestUtils refappTestUtils;

    public AuditServiceSmokeTest(@Nonnull final AuditService auditService, @Nonnull final AuditSearchService auditSearchService, RefappTestUtils refappTestUtils) {
        requireNonNull(auditService, "auditService");
        requireNonNull(auditSearchService, "auditSearchService");
        requireNonNull(refappTestUtils, "refappTestUtils");
        this.auditService = auditService;
        this.auditSearchService = auditSearchService;
        this.refappTestUtils = refappTestUtils;
    }

    @Rule
    public final ProvideSystemProperty provideSystemPropertyRule = new ProvideSystemProperty("plugin.audit.broker.default.batch.size", "1");

    @Test
    public void smokeTest() {
        final String testAction = "testAction";
        final String testCategory = "testCategory";
        refappTestUtils.runAsSysAdmin(() -> {
            try {
                // Prepare audit event to be generated
                final AuditType auditType = AuditType.fromI18nKeys(
                            CoverageArea.ECOSYSTEM,
                            CoverageLevel.BASE,
                            testCategory,
                            testAction)
                        .build();

                final AuditEvent auditEvent = AuditEvent
                        .builder(auditType)
                        .build();

                // Create audit event
                auditService.audit(auditEvent);

                // Wait 1 second to allow audit log writes this event into ActiveObjects and becomes visible for query
                sleep(1000);

                // Prepare audit event query statement
                AuditQuery auditQuery = AuditQuery
                        .builder()
                        .actions(testAction)
                        .categories(testCategory)
                        .build();

                PageRequest<AuditEntityCursor> auditEntityCursorPageRequest = new PageRequest.Builder<AuditEntityCursor>().offset(0).limit(100).build();

                Page<AuditEntity, AuditEntityCursor> auditEntityCursorPage = auditSearchService.findBy(auditQuery, auditEntityCursorPageRequest);

                // Get number of matched audit events
                final Integer matchingCount = auditEntityCursorPage
                        .getValues()
                        .size();

                assertThat(matchingCount, greaterThan(0));
            } catch (InterruptedException | TimeoutException e) {
                logger.error("Failed to wait for audit event to be generated", e);
                Thread.currentThread().interrupt();
            }
        });
    }

}
