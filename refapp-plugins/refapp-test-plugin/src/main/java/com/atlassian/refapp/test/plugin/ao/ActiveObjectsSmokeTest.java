package com.atlassian.refapp.test.plugin.ao;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.functest.junit.SpringAwareTestCase;
import com.atlassian.refapp.test.plugin.ao.entity.Widget;
import org.junit.Test;

import static java.util.Objects.requireNonNull;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.is;

/**
 * In-product smoke test of the ActiveObjects persistence framework.
 */
public class ActiveObjectsSmokeTest extends SpringAwareTestCase {

    private static final Object[] NO_PARAMS = {};  // to help type inference

    private static final String WIDGET_NAME = "MyWidget";
    private static final boolean WIDGET_FICTIONAL = true;
    private static final int WIDGET_QUANTITY = 42;

    private final ActiveObjects ao;

    public ActiveObjectsSmokeTest(final ActiveObjects ao) {
        this.ao = requireNonNull(ao);
    }

    @Test
    public void smokeTest() {
        deleteAllExistingWidgets();

        final Widget widget = addOneWidget();
        assertWidgetHasTestValues(widget);

        final int rowCount = getWidgetRowCount();
        assertThat(rowCount, is(1));
    }

    private int getWidgetRowCount() {
        return ao.find(Widget.class).length;
    }

    private void assertWidgetHasTestValues(final Widget widget) {
        assertThat(widget.getID(), greaterThanOrEqualTo(0));
        assertThat(widget.getName(), is(WIDGET_NAME));
        assertThat(widget.isFictional(), is(WIDGET_FICTIONAL));
        assertThat(widget.getQuantity(), is(WIDGET_QUANTITY));
    }

    private Widget addOneWidget() {
        return ao.executeInTransaction(() -> {
            final Widget widget = ao.create(Widget.class);
            widget.setName(WIDGET_NAME);
            widget.setFictional(WIDGET_FICTIONAL);
            widget.setQuantity(WIDGET_QUANTITY);
            widget.save();
            return widget;
        });
    }

    private void deleteAllExistingWidgets() {
        ao.deleteWithSQL(Widget.class, null, NO_PARAMS);
    }
}
