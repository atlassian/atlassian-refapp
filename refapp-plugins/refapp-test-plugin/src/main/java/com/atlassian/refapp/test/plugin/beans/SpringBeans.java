package com.atlassian.refapp.test.plugin.beans;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.audit.api.AuditSearchService;
import com.atlassian.audit.api.AuditService;
import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.crowd.manager.application.ApplicationManager;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.refapp.api.ConnectionProvider;
import com.atlassian.refapp.api.EmbeddedCrowdAuthenticator;
import com.atlassian.refapp.test.plugin.util.RefappTestUtils;
import com.atlassian.refapp.test.plugin.util.RefappTestUtilsImpl;
import com.atlassian.sal.api.executor.ThreadLocalContextManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.importOsgiService;

@Configuration
public class SpringBeans {

    @Bean
    public ActiveObjects activeObjects() {
        return importOsgiService(ActiveObjects.class);
    }

    @Bean
    public AuditService auditService() {
        return importOsgiService(AuditService.class);
    }

    @Bean
    public AuditSearchService auditSearchService() {
        return importOsgiService(AuditSearchService.class);
    }

    @Bean
    public ConnectionProvider connectionProvider() {
        return importOsgiService(ConnectionProvider.class);
    }

    @Bean
    public CrowdService crowdService() {
        return importOsgiService(CrowdService.class);
    }

    @Bean
    public ApplicationManager applicationManager() {
        return importOsgiService(ApplicationManager.class);
    }

    @Bean
    public DirectoryManager directoryManager() {
        return importOsgiService(DirectoryManager.class);
    }

    @Bean
    public EmbeddedCrowdAuthenticator embeddedCrowdAuthenticator() {
        return importOsgiService(EmbeddedCrowdAuthenticator.class);
    }

    @Bean
    public ThreadLocalContextManager threadLocalContextManager() {
        return importOsgiService(ThreadLocalContextManager.class);
    }

    @Bean
    public RefappTestUtils exportRefappTestUtils(ThreadLocalContextManager<User> threadLocalContextManager, EmbeddedCrowdAuthenticator embeddedCrowdAuthenticator) {
        return new RefappTestUtilsImpl(embeddedCrowdAuthenticator, threadLocalContextManager);
    }

}
