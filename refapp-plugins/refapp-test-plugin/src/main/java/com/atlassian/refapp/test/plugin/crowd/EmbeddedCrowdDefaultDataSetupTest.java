package com.atlassian.refapp.test.plugin.crowd;

import com.atlassian.crowd.directory.InternalDirectory;
import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.embedded.api.OperationType;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.crowd.exception.DirectoryNotFoundException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.model.user.ImmutableUser;
import com.atlassian.crowd.password.factory.PasswordEncoderFactory;
import com.atlassian.crowd.util.BoundedCount;
import com.atlassian.functest.junit.SpringAwareTestCase;
import com.google.common.collect.Sets;
import org.junit.Assert;
import org.junit.Test;

import static java.util.Objects.requireNonNull;

/**
 * Embedded Crowd tables are created and default data (application, directory, users ,groups) is stored in DB.
 * Refapp data setup:
 * {@code com.atlassian.plugin.refimpl.crowd.embedded.EmbeddedCrowdBootstrap}
 */
public class EmbeddedCrowdDefaultDataSetupTest extends SpringAwareTestCase {
    private static final String INTERNAL_DIRECTORY_NAME = "RefApp Internal Directory";
    private static final String SYSTEM_ADMINISTRATORS_GROUP = "system_administrators";
    private static final String ADMINISTRATORS_GROUP = "administrators";
    private static final String USERS_GROUP = "users";
    private static final String UNLICENSED_GROUP = "unlicensed";
    private static final ImmutableUser SYSTEM_ADMIN_USER_1 = new ImmutableUser(1L, "admin",
            "A. D. Ministrator Sysadmin", "admin@example.com", true, "A. D. Ministrator", "Sysadmin", "1");
    private static final ImmutableUser SYSTEM_ADMIN_USER_2 = new ImmutableUser(2L, "fred",
            "Fred Sysadmin", "fred@example.org", true, "Fred Sysadmin", "Sysadmin", "2");
    private static final ImmutableUser ADMIN_USER = new ImmutableUser(3L, "betty",
            "Betty Admin", "betty@example.com", true, "Betty", "Admin", "2");
    private static final ImmutableUser PLAIN_USER = new ImmutableUser(4L, "barney",
            "Barney User", "barney@example.com", true, "Barney", "Admin", "3");
    private static final ImmutableUser UNLICENSED_USER = new ImmutableUser(5L, "michell", "Michell Unlicensed",
            "michell@example.org", true, "Michell Unlicensed", "Unlicensed", "5");

    private final CrowdService crowdService;
    private final DirectoryManager directoryManager;

    public EmbeddedCrowdDefaultDataSetupTest(CrowdService crowdService, DirectoryManager directoryManager) {
        this.crowdService = requireNonNull(crowdService);
        this.directoryManager = requireNonNull(directoryManager);
    }

    @Test
    public void shouldFindDirectoryByName() throws DirectoryNotFoundException {
        final Directory directory = this.directoryManager.findDirectoryByName(INTERNAL_DIRECTORY_NAME);

        Assert.assertEquals(INTERNAL_DIRECTORY_NAME, directory.getName());
        Assert.assertEquals(INTERNAL_DIRECTORY_NAME, directory.getDescription());
        Assert.assertEquals(DirectoryType.INTERNAL, directory.getType());
        Assert.assertEquals(InternalDirectory.class.getCanonicalName(), directory.getImplementationClass());
        Assert.assertEquals(PasswordEncoderFactory.ATLASSIAN_SECURITY_ENCODER, directory.getEncryptionType());
        Assert.assertEquals(Sets.newHashSet(OperationType.values()), directory.getAllowedOperations());
        Assert.assertTrue(directory.isActive());
    }

    @Test
    public void shouldFindSystemAdminUsers() {
        final User systemAdminUser1 = this.crowdService.getUser(SYSTEM_ADMIN_USER_1.getName());
        Assert.assertEquals(SYSTEM_ADMIN_USER_1.getEmailAddress(), systemAdminUser1.getEmailAddress());
        Assert.assertEquals(SYSTEM_ADMIN_USER_1.getDisplayName(), systemAdminUser1.getDisplayName());
        Assert.assertTrue(systemAdminUser1.isActive());

        final User systemAdminUser2 = this.crowdService.getUser(SYSTEM_ADMIN_USER_2.getName());
        Assert.assertEquals(SYSTEM_ADMIN_USER_2.getEmailAddress(), systemAdminUser2.getEmailAddress());
        Assert.assertEquals(SYSTEM_ADMIN_USER_2.getDisplayName(), systemAdminUser2.getDisplayName());
        Assert.assertTrue(systemAdminUser2.isActive());
    }

    @Test
    public void shouldFindAdminUser() {
        final User adminUser = this.crowdService.getUser(ADMIN_USER.getName());
        Assert.assertEquals(ADMIN_USER.getEmailAddress(), adminUser.getEmailAddress());
        Assert.assertEquals(ADMIN_USER.getDisplayName(), adminUser.getDisplayName());
        Assert.assertTrue(adminUser.isActive());
    }

    @Test
    public void shouldFindPlainUser() {
        final User plainUser = this.crowdService.getUser(PLAIN_USER.getName());
        Assert.assertEquals(PLAIN_USER.getEmailAddress(), plainUser.getEmailAddress());
        Assert.assertEquals(PLAIN_USER.getDisplayName(), plainUser.getDisplayName());
        Assert.assertTrue(plainUser.isActive());
    }

    @Test
    public void shouldFindUnlicensedUser() {
        final User plainUser = this.crowdService.getUser(UNLICENSED_USER.getName());
        Assert.assertEquals(UNLICENSED_USER.getEmailAddress(), plainUser.getEmailAddress());
        Assert.assertEquals(UNLICENSED_USER.getDisplayName(), plainUser.getDisplayName());
        Assert.assertTrue(plainUser.isActive());
    }

    @Test
    public void shouldCountSystemAdminUser() throws DirectoryNotFoundException, OperationFailedException {
        final Directory directory = this.directoryManager.findDirectoryByName(INTERNAL_DIRECTORY_NAME);
        BoundedCount boundedCount = directoryManager.countDirectMembersOfGroup(directory.getId(),
                SYSTEM_ADMINISTRATORS_GROUP, Integer.MAX_VALUE);
        Assert.assertEquals(boundedCount.getCount(), 2);
    }

    @Test
    public void shouldCountAdminUser() throws DirectoryNotFoundException, OperationFailedException {
        final Directory directory = this.directoryManager.findDirectoryByName(INTERNAL_DIRECTORY_NAME);
        BoundedCount boundedCount = directoryManager.countDirectMembersOfGroup(directory.getId(), ADMINISTRATORS_GROUP,
                Integer.MAX_VALUE);
        Assert.assertEquals(boundedCount.getCount(), 1);
    }

    @Test
    public void shouldCountUser() throws DirectoryNotFoundException, OperationFailedException {
        final Directory directory = this.directoryManager.findDirectoryByName(INTERNAL_DIRECTORY_NAME);
        BoundedCount boundedCount =
                directoryManager.countDirectMembersOfGroup(directory.getId(), USERS_GROUP, Integer.MAX_VALUE);
        Assert.assertEquals(boundedCount.getCount(), 1);
    }

    @Test
    public void shouldCountUnlicensedUser() throws DirectoryNotFoundException, OperationFailedException {
        final Directory directory = this.directoryManager.findDirectoryByName(INTERNAL_DIRECTORY_NAME);
        BoundedCount boundedCount =
                directoryManager.countDirectMembersOfGroup(directory.getId(), UNLICENSED_GROUP, Integer.MAX_VALUE);
        Assert.assertEquals(boundedCount.getCount(), 1);
    }
}
