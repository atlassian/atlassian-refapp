package com.atlassian.refapp.test.plugin.ao.entity;

import net.java.ao.Entity;
import net.java.ao.schema.NotNull;

import javax.annotation.Nonnull;

/**
 * A dummy entity managed by ActiveObjects.
 */
public interface Widget extends Entity {

    /**
     * Returns the name of this widget.
     *
     * @return a non-blank name
     */
    String getName();

    /**
     * Sets the name of this widget.
     *
     * @param name the name to set
     */
    void setName(@Nonnull String name);

    /**
     * Indicates whether this widget is fictional.
     *
     * @return see description
     */
    boolean isFictional();

    /**
     * Sets whether this widget is fictional.
     *
     * @param fictional {@code false} if it's a real widget
     */
    void setFictional(boolean fictional);

    /**
     * Returns the quantity of this widget.
     *
     * @return zero or more
     */
    int getQuantity();

    /**
     * Sets the quantity of this widget.
     *
     * @param quantity the quantity to set
     */
    void setQuantity(int quantity);
}
