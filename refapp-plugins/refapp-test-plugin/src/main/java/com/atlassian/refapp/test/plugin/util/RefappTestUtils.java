package com.atlassian.refapp.test.plugin.util;

public interface RefappTestUtils {
    void runAsSysAdmin(Runnable action);

    void runAsAdmin(Runnable action);

    void runAsUser(Runnable action);

    void runAsUnlicensedUser(Runnable action);

}
