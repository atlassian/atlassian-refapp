package com.atlassian.refapp.test.plugin.crowd;

import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.crowd.exception.ApplicationNotFoundException;
import com.atlassian.crowd.exception.DirectoryNotFoundException;
import com.atlassian.crowd.manager.application.ApplicationManager;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.functest.junit.SpringAwareTestCase;
import com.atlassian.refapp.api.ConnectionProvider;
import org.junit.Assert;
import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static java.util.Objects.requireNonNull;

public class EmbeddedCrowdObjectRelationshipIntegrationTest extends SpringAwareTestCase {
    public static final String INTERNAL_DIRECTORY_NAME = "RefApp Internal Directory";
    private static final String EMBEDDED_CROWD_APPLICATION_NAME = "crowd-embedded";
    private static final String SYSTEM_ADMINISTRATORS_GROUP = "system_administrators";
    private static final String ADMINISTRATORS_GROUP = "administrators";
    private static final String USERS_GROUP = "users";
    private static final String UNLICENSED_GROUP = "unlicensed";
    private static final String SYSTEM_ADMIN_USER = "admin";
    private static final String ADMIN_USER = "betty";
    private static final String PLAIN_USER = "Barney";
    private static final String UNLICENSED_USER = "michell";

    private final CrowdService crowdService;
    private final ApplicationManager applicationManager;
    private final DirectoryManager directoryManager;
    private final ConnectionProvider connectionProvider;

    // SpringAwareTestCases use setter injection
    public EmbeddedCrowdObjectRelationshipIntegrationTest(final CrowdService crowdService,
                                                          final ApplicationManager applicationManager,
                                                          final DirectoryManager directoryManager,
                                                          final ConnectionProvider connectionProvider) {
        this.crowdService = requireNonNull(crowdService);
        this.applicationManager = requireNonNull(applicationManager);
        this.directoryManager = requireNonNull(directoryManager);
        this.connectionProvider = requireNonNull(connectionProvider);
    }

    @Test
    public void applicationAndDirectoryMappingRelationshipTest() throws SQLException, DirectoryNotFoundException, ApplicationNotFoundException {
        try (final Connection connection = this.connectionProvider.connection();
             final PreparedStatement prepareStatement = connection.prepareStatement("SELECT * FROM CWD_APP_DIR_MAPPING");
             final ResultSet resultSet = prepareStatement.executeQuery()) {
            if (resultSet.next()) {
                final Long expectedApplicationId = resultSet.getLong("APPLICATION_ID");
                final Long expectedDirectoryId = resultSet.getLong("DIRECTORY_ID");

                final Directory directory = this.directoryManager.findDirectoryByName(INTERNAL_DIRECTORY_NAME);
                final Application application = this.applicationManager.findByName(EMBEDDED_CROWD_APPLICATION_NAME);

                Assert.assertEquals(expectedDirectoryId, directory.getId());
                Assert.assertEquals(expectedApplicationId, application.getId());
            } else {
                throw new IllegalStateException("Expected a single row but found no rows");
            }
        }
    }

    @Test
    public void directoryAndUserMappingRelationshipTest() throws DirectoryNotFoundException {
        final Directory directory = this.directoryManager.findDirectoryByName(INTERNAL_DIRECTORY_NAME);

        final User adminUser = this.crowdService.getUser(SYSTEM_ADMIN_USER);
        Assert.assertEquals(directory.getId().longValue(), adminUser.getDirectoryId());

        final User plainUser = this.crowdService.getUser(ADMIN_USER);
        Assert.assertEquals(directory.getId().longValue(), plainUser.getDirectoryId());
    }

    @Test
    public void directoryAndGroupsMappingRelationshipTest() {
        final Group systemAdministratorsGroup = this.crowdService.getGroup(SYSTEM_ADMINISTRATORS_GROUP);
        Assert.assertNotNull(systemAdministratorsGroup);

        final Group plainUsersGroup = this.crowdService.getGroup(USERS_GROUP);
        Assert.assertNotNull(plainUsersGroup);
    }

    @Test
    public void groupsAndUsersMappingRelationshipTest() {
        Assert.assertTrue(this.crowdService.isUserMemberOfGroup(SYSTEM_ADMIN_USER, SYSTEM_ADMINISTRATORS_GROUP));
        Assert.assertTrue(this.crowdService.isUserMemberOfGroup(ADMIN_USER, ADMINISTRATORS_GROUP));
        Assert.assertTrue(this.crowdService.isUserMemberOfGroup(PLAIN_USER, USERS_GROUP));
        Assert.assertTrue(this.crowdService.isUserMemberOfGroup(UNLICENSED_USER, UNLICENSED_GROUP));
    }
}
