import 'wr-dependency!com.atlassian.refapp.amd:amd';

import React from 'react';
import { render } from 'react-dom';
import registry from '@atlassian/clientside-extensions-registry';

import { useExtensions } from '../generated/header_links';
import SimpleLinkHandler from '../handlers/SimpleLinkHandler';

import type { FC } from 'react';
import type { ExtensionDescriptor } from '@atlassian/clientside-extensions-registry';

type AppType = {
    legacyItems: ExtensionDescriptor[];
};

const App: FC<AppType> = ({ legacyItems }) => {
    const supportedDescriptors = useExtensions(null);

    return <SimpleLinkHandler descriptors={supportedDescriptors} legacyItems={legacyItems} />;
};

registry.registerHandler('header.links', (root, location, legacyItems) => {
    const standardizedWebItems = legacyItems.map(
        (item) =>
            ({
                location,
                key: (item as { completeKey: string }).completeKey,
                attributes: {
                    ...item,
                },
            } as ExtensionDescriptor)
    );

    render(<App legacyItems={standardizedWebItems} />, root);
});
