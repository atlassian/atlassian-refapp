import 'wr-dependency!com.atlassian.refapp.amd:amd';

import React, { FC } from 'react';
import { render } from 'react-dom';
import registry from '@atlassian/clientside-extensions-registry';
import { useExtensions } from '@atlassian/clientside-extensions-components';

import { validate } from '../generated/footer_links_validator';
import SimpleLinkHandler from '../handlers/SimpleLinkHandler';

import type { ExtensionDescriptor } from '@atlassian/clientside-extensions-registry';

type AppType = {
    legacyItems: ExtensionDescriptor[];
    location: string;
};

const App: FC<AppType> = ({ legacyItems, location }) => {
    const supportedDescriptors = useExtensions(location, null, {
        attributeValidator: validate,
    });

    return <SimpleLinkHandler descriptors={supportedDescriptors} legacyItems={legacyItems} />;
};

registry.registerHandler('footer.links', (root, location, legacyItems) => {
    const standardizedWebItems = legacyItems.map(
        (item) =>
            ({
                location,
                key: (item as { completeKey: string }).completeKey,
                attributes: {
                    ...item,
                },
            } as ExtensionDescriptor)
    );

    render(
        <>
            <li> Atlassian RefApp {root.dataset.version as string} </li>
            <App location={location} legacyItems={standardizedWebItems} />
        </>,
        root
    );
});
