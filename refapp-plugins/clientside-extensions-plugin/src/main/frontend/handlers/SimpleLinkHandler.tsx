import React, { useMemo } from 'react';

import ExtensionHandler from '../handlers/ExtensionHandler';

import type { FC } from 'react';
import type { ExtensionDescriptor } from '@atlassian/clientside-extensions-registry';

type AppType = {
    descriptors: ExtensionDescriptor[];
    legacyItems: ExtensionDescriptor[];
};

const SimpleLinkHandler: FC<AppType> = ({ descriptors, legacyItems }) => {
    const legacyKeys = useMemo(() => legacyItems.map((item) => item.key), [legacyItems]);

    // Whatever is passed through as legacy items is "the source of truth".
    // Descriptors of the hook with the same key can be filtered out.
    const cleanedDescriptors = descriptors.filter((item) => !legacyKeys.includes(item.key));

    const weightedItems = [...cleanedDescriptors, ...legacyItems].sort(
        (a, b) => a.attributes.weight - b.attributes.weight
    );
    return (
        <>
            {weightedItems.map((desc) => {
                return (
                    <li key={desc.key}>
                        <ExtensionHandler extension={desc} />
                    </li>
                );
            })}
        </>
    );
};

export default SimpleLinkHandler;
