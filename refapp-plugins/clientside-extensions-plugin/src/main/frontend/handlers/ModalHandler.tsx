import React, { useCallback, useState } from 'react';
import { ModalHandler } from '@atlassian/clientside-extensions-components';

import { LinkHandler } from './LinkHandler';

import type { FunctionComponent } from 'react';
import type { ModalExtension } from '@atlassian/clientside-extensions';

export interface ModalWithActionHandlerProps {
    render: ModalExtension.ModalRenderExtension;
    disabled?: boolean;
    hidden?: boolean;
    __withoutTransition?: boolean;
    __disableFocusLock?: boolean;
}

export const ModalWithActionHandler: FunctionComponent<ModalWithActionHandlerProps> = ({
    // Action Handler Properties
    children,
    disabled,
    hidden,

    // Modal Handler Properties
    render,
    __withoutTransition,
    __disableFocusLock,
}) => {
    const [isOpen, setIsOpen] = useState(false);

    const openModal = useCallback(() => {
        setIsOpen(true);
    }, []);

    const closeModal = useCallback(() => {
        setIsOpen(false);
    }, []);

    if (hidden) {
        return null;
    }

    return (
        <>
            <LinkHandler disabled={disabled} hidden={hidden} onAction={openModal} href={undefined}>
                {children}
            </LinkHandler>
            <ModalHandler
                isOpen={isOpen}
                onClose={closeModal}
                render={render}
                __disableFocusLock={__disableFocusLock}
                __withoutTransition={__withoutTransition}
            />
        </>
    );
};
