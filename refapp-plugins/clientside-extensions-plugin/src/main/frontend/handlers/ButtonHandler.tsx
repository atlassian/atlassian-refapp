import React from 'react';

import type { FunctionComponent } from 'react';

export interface ButtonHandlerProps {
    plain?: boolean;
    onAction?: React.MouseEventHandler;
    hidden?: boolean;
    disabled?: boolean;
    classNames?: string;
    id?: string;
}

export const ButtonHandler: FunctionComponent<ButtonHandlerProps> = ({
    children,
    disabled,
    hidden,
    onAction,
    classNames,
    id
}) => {
    if (hidden) {
        return null;
    }

    return (
        <button
            disabled={disabled}
            onClick={(onAction as unknown) as React.MouseEventHandler}
            className={classNames}
            id={id}
        >
            {children}
        </button>
    );
};
