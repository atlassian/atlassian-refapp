import { LinkHandler } from './LinkHandler';
import React from 'react';

import { ModalWithActionHandler } from './ModalHandler';

import type { FC } from 'react';
import type { ExtensionDescriptor } from '@atlassian/clientside-extensions-registry';
import type { ModalExtension } from '@atlassian/clientside-extensions';


type ExtensionHandlerProps = {
    extension: ExtensionDescriptor;
};

const ExtensionHandler: FC<ExtensionHandlerProps> = ({ extension }) => {
    if (!extension.attributes.type) {
        const { url, styleClass, id, label } = extension.attributes;
        return (
            <LinkHandler href={url as string} classNames={styleClass as string} id={id as string}>
                {label}
            </LinkHandler>
        );
    }

    if (extension.attributes.type === 'link') {
        const { onAction, url, label, disabled, hidden } = extension.attributes;
        return (
            <LinkHandler
                disabled={!!disabled}
                hidden={!!hidden}
                href={url as string}
                onAction={onAction as React.MouseEventHandler}
            >
                {label}
            </LinkHandler>
        );
    }

    if (extension.attributes.type === 'button') {
        const { onAction, label, disabled, hidden } = extension.attributes;
        return (
            <LinkHandler
                disabled={!!disabled}
                hidden={!!hidden}
                href={undefined}
                onAction={onAction as React.MouseEventHandler}
            >
                {label}
            </LinkHandler>
        );
    }

    if (extension.attributes.type === 'modal') {
        const { onAction, label, disabled, hidden } = extension.attributes;
        return (
            <ModalWithActionHandler
                disabled={!!disabled}
                hidden={!!hidden}
                render={onAction as ModalExtension.ModalRenderExtension}
            >
                {label}
            </ModalWithActionHandler>
        );
    }
};

export default ExtensionHandler;