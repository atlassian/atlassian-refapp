import React from 'react';

import type { FunctionComponent } from 'react';

export interface LinkHandlerProps {
    href: string;
    plain?: boolean;
    onAction?: React.MouseEventHandler;
    hidden?: boolean;
    disabled?: boolean;
    classNames?: string;
    id?: string;
}

export const LinkHandler: FunctionComponent<LinkHandlerProps> = ({
    children,
    disabled,
    hidden,
    href,
    onAction,
    classNames,
    id
}) => {
    if (hidden) {
        return null;
    }

    return (
        <a
            href={href ?? 'javascript:void(0)'}
            aria-disabled={disabled}
            onClick={onAction}
            className={classNames}
            id={id}
        >
            {children}
        </a>
    );
};
