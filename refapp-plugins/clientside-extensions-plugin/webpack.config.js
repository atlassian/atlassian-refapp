const path = require('path');
const process = require('process');
const WrmPlugin = require('atlassian-webresource-webpack-plugin');
const CsePlugin = require('@atlassian/clientside-extensions-webpack-plugin');

const mode = process.env.NODE_ENV !== 'development' ? 'production' : 'development';

const projectRootDir = path.resolve(__dirname);
const frontendDir = path.join(projectRootDir, 'src/main/frontend');
const outputDir = path.join(projectRootDir, 'target/classes');

const wrmPlugin = new WrmPlugin({
    pluginKey: 'com.atlassian.refapp.clientside-extensions',
    xmlDescriptors: path.join(outputDir, 'META-INF/plugin-descriptors/webpacked-descriptors.xml'),
    providedDependencies: {
        // https://bitbucket.org/atlassian/atlassian-clientside-extensions/src/master/INSTALLATION.md
        // Add @atlassian/clientside-extensions-registry as a provided dependency
        '@atlassian/clientside-extensions-registry': {
            dependency: 'com.atlassian.plugins.atlassian-clientside-extensions-runtime:runtime',
            import: {
                var: "require('@atlassian/clientside-extensions-registry')",
                amd: '@atlassian/clientside-extensions-registry',
            },
        },
    },
    addEntrypointNameAsContext: true,
});

const extensionsPlugin = new CsePlugin({
    cwd: frontendDir,
    pattern: '**/*.js',
});

module.exports = {
    mode,
    entry: {
        ...extensionsPlugin.generateEntrypoints(),
        'footer-links-handler': path.resolve(frontendDir, 'location-handlers/footer-links.tsx'),
        'header-links-handler': path.resolve(frontendDir, 'location-handlers/header-links.tsx'),
        'system-general-handler': path.resolve(frontendDir, 'location-handlers/system-admin.tsx'),
    },
    optimization: {
        splitChunks: {
            // include all types of chunks
            chunks: 'all',
        },
        runtimeChunk: 'single',
        nodeEnv: mode,
    },
    resolve: {
        extensions: ['.js', '.ts', '.tsx'],
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                loader: 'babel-loader',
                options: {
                    presets: [
                        ['@babel/preset-env', { modules: false }],
                        '@babel/preset-react',
                        '@babel/preset-typescript',
                    ],
                },
            },
            {
                test: /\.graphql$/,
                loader: '@atlassian/clientside-extensions-schema/loader',
                options: {
                    providedTypes: '**/*.type.graphql',
                    strictMode: false,
                },
            },
        ],
    },
    output: {
        path: path.resolve(projectRootDir, 'target/classes'),
    },
    plugins: [wrmPlugin, extensionsPlugin],
};
