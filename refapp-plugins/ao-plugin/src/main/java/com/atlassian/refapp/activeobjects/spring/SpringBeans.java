package com.atlassian.refapp.activeobjects.spring;

import com.atlassian.activeobjects.spi.DataSourceProvider;
import com.atlassian.activeobjects.spi.NoOpTransactionSynchronisationManager;
import com.atlassian.activeobjects.spi.TransactionSynchronisationManager;
import com.atlassian.refapp.activeobjects.RefappDataSourceProvider;
import com.atlassian.refapp.api.ConnectionProvider;
import org.osgi.framework.ServiceRegistration;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.atlassian.plugins.osgi.javaconfig.ExportOptions.as;
import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.exportOsgiService;
import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.importOsgiService;

/**
 * Transformerless spring bean configuration.
 * See <a href="https://developer.atlassian.com/server/framework/atlassian-sdk/spring-java-config/">Spring Java Config</a>
 * @since 5.1.3
 */
@Configuration
public class SpringBeans {
    @Bean
    public ConnectionProvider connectionProvider() {
        return importOsgiService(ConnectionProvider.class);
    }

    @Bean
    public DataSourceProvider tenantAwareDataSourceProvider(ConnectionProvider connectionProvider) {
        return new RefappDataSourceProvider(connectionProvider);
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportDataSourceProvider(DataSourceProvider tenantAwareDataSourceProvider) {
        return exportOsgiService(tenantAwareDataSourceProvider, as(DataSourceProvider.class));
    }

    @Bean
    public TransactionSynchronisationManager transactionSynchronisationManager() {
        return new NoOpTransactionSynchronisationManager();
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportTransactionSynchronisationManager(TransactionSynchronisationManager transactionSynchronisationManager) {
        return exportOsgiService(transactionSynchronisationManager, as(TransactionSynchronisationManager.class));
    }
}