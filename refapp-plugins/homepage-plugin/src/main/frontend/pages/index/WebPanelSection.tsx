import React, { FC } from 'react';
import { PanelHandler } from '@atlassian/clientside-extensions-components';
import type {
    ExtensionDescriptor,
    ExtensionAttributes,
} from '@atlassian/clientside-extensions-registry';
import type { PanelExtension } from '@atlassian/clientside-extensions';

import { useExtensionsAll } from './atl.refapp.index.cse.graphql';

const xmlDescriptorExample = `
<web-panel key="myPanel" location="atl.refapp.index">
    <resource name="view" type="velocity"
    location="/relative/or/absolute/path/to/my/panel.vm"/>
</web-panel>
`;

interface Attributes extends ExtensionAttributes {
    onAction: PanelExtension.PanelRenderExtension;
}

function asDynamicPanels(ext: ExtensionDescriptor<Attributes>) {
    const { label, onAction } = ext.attributes;

    return (
        <React.Fragment key={ext.key}>
            <h3>{label}</h3>
            <PanelHandler render={onAction} />
        </React.Fragment>
    );
}

function asStaticPanels() {
    return 'To be implemented';
}

const loadingIndicator = (
    <p>
        Loading <code>index.links</code>...
    </p>
);

const WebPanelSection: FC = () => {
    const [newerExtensions, olderExtensions, loading] = useExtensionsAll(null);
    return (
        <>
            <h2>Web Panels</h2>

            <div id="js-index-webpanels">
                {loading && loadingIndicator}
                {!loading && newerExtensions.map(asDynamicPanels)}
                {!loading && olderExtensions.map(asStaticPanels)}
            </div>

            <details>
                <summary style={{ display: 'list-item' }}>What about the content above?</summary>

                <p>
                    In addition to Web Items, you can also add <dfn>Web Panels</dfn> here.
                </p>
                <ul>
                    <li>
                        Using Client-Side Extensions.{' '}
                        <a href="https://developer.atlassian.com/server/framework/clientside-extensions/reference/api/panel-api/">
                            Read the docs for Client-Side Extensions: <code>PanelExtension</code>{' '}
                            API
                        </a>
                    </li>
                    <li>
                        Using XML descriptors.
                        <code>
                            <pre>{xmlDescriptorExample}</pre>
                        </code>
                    </li>
                </ul>
            </details>
        </>
    );
};

export default WebPanelSection;
