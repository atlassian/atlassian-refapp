import React, { FC } from 'react';

interface PageProps {
    title: string;
}

const RefappPageContent: FC<PageProps> = ({ title, children }) => {
    return (
        <div className="aui-page-panel">
            <div className="aui-page-panel-inner">
                <section className="aui-page-panel-content">
                    <header className="aui-page-header">
                        <div className="aui-page-header-inner">
                            <div className="aui-page-header-main">
                                <h1>{title}</h1>
                            </div>
                        </div>
                    </header>

                    {children}
                </section>
            </div>
        </div>
    );
};

export default RefappPageContent;
