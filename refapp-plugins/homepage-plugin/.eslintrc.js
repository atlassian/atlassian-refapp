module.exports = {
    overrides: [
        // TypeScript
        {
            files: ['*.ts', '*.tsx'],

            extends: ['plugin:@typescript-eslint/recommended'],
            parser: '@typescript-eslint/parser',
        },
    ],
    rules: {
        'prettier/prettier': [
            'error',
            {
                'endOfLine': 'auto',
            }
        ]
    }
};
