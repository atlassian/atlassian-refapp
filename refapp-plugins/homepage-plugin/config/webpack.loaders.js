const { MY_I18N_FILES } = require('./webpack.constants');

function getLoaders({ isProductionEnv = false }) {
    return [
        {
            test: /\.tsx?$/,
            exclude: /node_modules/,
            use: [
                {
                    loader: '@atlassian/i18n-properties-loader',
                    options: {
                        i18nFiles: MY_I18N_FILES,
                        disabled: isProductionEnv,
                    },
                },
                {
                    loader: 'babel-loader',
                    options: {
                        cacheDirectory: true,
                    },
                },
            ],
        },
        {
            test: /\.cse.graphql$/,
            loader: '@atlassian/clientside-extensions-schema/loader',
        },
    ];
}

module.exports.loaders = (isProduction) => getLoaders(isProduction);
