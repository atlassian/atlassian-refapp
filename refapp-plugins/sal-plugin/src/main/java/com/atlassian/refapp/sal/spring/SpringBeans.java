package com.atlassian.refapp.sal.spring;

import com.atlassian.beehive.ClusterLockService;
import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.refimpl.saldeps.CookieBasedScopeManager;
import com.atlassian.refapp.api.ConnectionProvider;
import com.atlassian.refapp.api.EmbeddedCrowdAuthenticator;
import com.atlassian.refapp.auth.external.WebSudoSessionManager;
import com.atlassian.refapp.sal.DefaultRefAppAccessManager;
import com.atlassian.refapp.sal.RefAppAccessManager;
import com.atlassian.refapp.sal.RefimplApplicationProperties;
import com.atlassian.refapp.sal.RefimplHostContextAccessor;
import com.atlassian.refapp.sal.auth.RefappAuthenticationController;
import com.atlassian.refapp.sal.executor.RefImplThreadLocalDelegateExecutorFactory;
import com.atlassian.refapp.sal.executor.RefimplThreadLocalContextManager;
import com.atlassian.refapp.sal.license.RefimplLicenseHandler;
import com.atlassian.refapp.sal.lifecycle.RefimplLifecycleManager;
import com.atlassian.refapp.sal.message.RefImplHelpPathResolver;
import com.atlassian.refapp.sal.message.RefimplI18nResolver;
import com.atlassian.refapp.sal.message.RefimplLocaleResolver;
import com.atlassian.refapp.sal.message.ResourceBundleResolverImpl;
import com.atlassian.refapp.sal.pluginsettings.RefimplPluginSettingsFactory;
import com.atlassian.refapp.sal.project.RefimplProjectManager;
import com.atlassian.refapp.sal.rdbms.RefImplHostConnectionAccessor;
import com.atlassian.refapp.sal.search.RefimplSearchProvider;
import com.atlassian.refapp.sal.search.query.DefaultSearchQueryParser;
import com.atlassian.refapp.sal.timezone.RefimplTimeZoneManager;
import com.atlassian.refapp.sal.user.RefImplUserManager;
import com.atlassian.refapp.sal.usersettings.RefimplUserSettingsService;
import com.atlassian.refapp.sal.web.context.RefImplHttpContext;
import com.atlassian.refapp.sal.websudo.RefImplWebSudoManager;
import com.atlassian.refapp.sal.xsrf.RefappXsrfTokenValidatorBypassingUPM;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.features.SiteDarkFeaturesStorage;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.sal.api.upgrade.PluginUpgradeTask;
import com.atlassian.sal.api.xsrf.XsrfTokenAccessor;
import com.atlassian.sal.api.xsrf.XsrfTokenValidator;
import com.atlassian.sal.core.auth.OAuthRequestVerifierFactoryImpl;
import com.atlassian.sal.core.auth.SeraphAuthenticationListener;
import com.atlassian.sal.core.auth.SeraphLoginUriProvider;
import com.atlassian.sal.core.component.DefaultComponentLocator;
import com.atlassian.sal.core.features.DefaultDarkFeatureManager;
import com.atlassian.sal.core.features.DefaultSiteDarkFeaturesStorage;
import com.atlassian.sal.core.net.HttpClientRequestFactory;
import com.atlassian.sal.core.permission.DefaultPermissionEnforcer;
import com.atlassian.sal.core.rdbms.DefaultTransactionalExecutorFactory;
import com.atlassian.sal.core.scheduling.TimerPluginScheduler;
import com.atlassian.sal.core.transaction.HostContextTransactionTemplate;
import com.atlassian.sal.core.upgrade.DefaultPluginUpgradeManager;
import com.atlassian.sal.core.xsrf.IndependentXsrfTokenAccessor;
import com.atlassian.sal.core.xsrf.XsrfRequestValidatorImpl;
import com.atlassian.sal.spi.HostConnectionAccessor;
import com.atlassian.sal.spi.HostContextAccessor;
import com.atlassian.seraph.auth.AuthenticationContext;
import com.atlassian.seraph.auth.RoleMapper;
import org.osgi.framework.BundleContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.security.Principal;
import java.util.List;

@Configuration
public class SpringBeans {

    @Bean
    public RefimplApplicationProperties applicationProperties() throws IOException {
        return new RefimplApplicationProperties();
    }

    @Bean
    public HttpClientRequestFactory clientRequestFactory() {
        return new HttpClientRequestFactory();
    }

    @Bean
    public IndependentXsrfTokenAccessor xsrfTokenAccessor() {
        return new IndependentXsrfTokenAccessor();
    }

    @Bean
    public RefImplHttpContext httpContext() {
        return new RefImplHttpContext();
    }

    @Bean
    public CookieBasedScopeManager scopeManager() {
        return new CookieBasedScopeManager();
    }

    @Bean
    public RefImplHelpPathResolver helpPathResolver() {
        return new RefImplHelpPathResolver();
    }

    @Bean
    public RefimplLocaleResolver localeResolver() {
        return new RefimplLocaleResolver();
    }

    @Bean
    public RefimplLicenseHandler licenseHandler() {
        return new RefimplLicenseHandler();
    }

    @Bean
    public OAuthRequestVerifierFactoryImpl oAuthRequestVerifierFactory() {
        return new OAuthRequestVerifierFactoryImpl();
    }

    @Bean
    public SeraphAuthenticationListener authenticationListener() {
        return new SeraphAuthenticationListener();
    }

    @Bean
    public TimerPluginScheduler timerPluginScheduler() {
        return new TimerPluginScheduler();
    }

    @Bean
    public DefaultSearchQueryParser searchQueryParser() {
        return new DefaultSearchQueryParser();
    }

    @Bean
    public RefimplTimeZoneManager timeZoneManager() {
        return new RefimplTimeZoneManager();
    }

    @Bean
    public RefAppAccessManager refAppAccessManage() {
        return new DefaultRefAppAccessManager();
    }

    @Bean
    public RefimplThreadLocalContextManager threadLocalContextManager(AuthenticationContext authenticationContext) {
        return new RefimplThreadLocalContextManager(authenticationContext);
    }

    @Bean
    public RefImplThreadLocalDelegateExecutorFactory<Principal> threadLocalDelegateExecutorFactory(RefimplThreadLocalContextManager manager) {
        return new RefImplThreadLocalDelegateExecutorFactory<>(manager);
    }

    @Bean
    public RefimplI18nResolver i18nResolver(PluginAccessor pluginAccessor, PluginEventManager pluginEventManager) {
        return new RefimplI18nResolver(pluginAccessor, pluginEventManager, new ResourceBundleResolverImpl());
    }

    @Bean
    public RefImplUserManager userManager(AuthenticationContext authenticationContext,
                                          EmbeddedCrowdAuthenticator authenticator,
                                          RefAppAccessManager refAppAccessManager,
                                          CrowdService crowdService) {
        return new RefImplUserManager(authenticationContext, authenticator, refAppAccessManager, crowdService);
    }

    @Bean
    public RefimplPluginSettingsFactory pluginSettingsFactory(RefimplApplicationProperties applicationProperties) {
        return new RefimplPluginSettingsFactory(applicationProperties);
    }

    @Bean
    public SeraphLoginUriProvider loginUriProvider(RefimplApplicationProperties applicationProperties) {
        return new SeraphLoginUriProvider(applicationProperties);
    }

    @Bean
    public DefaultSiteDarkFeaturesStorage darkFeaturesStorage(RefimplPluginSettingsFactory pluginSettingsFactory) {
        return new DefaultSiteDarkFeaturesStorage(pluginSettingsFactory);
    }

    @Bean
    public DefaultDarkFeatureManager darkFeatureManager(com.atlassian.sal.api.user.UserManager userManager,
                                                        SiteDarkFeaturesStorage darkFeaturesStorage) {
        return new DefaultDarkFeatureManager(userManager, darkFeaturesStorage);
    }

    @Bean
    public RefappXsrfTokenValidatorBypassingUPM xsrfTokenValidator(XsrfTokenAccessor xsrfTokenAccessor) {
        return new RefappXsrfTokenValidatorBypassingUPM(xsrfTokenAccessor);
    }

    @Bean
    public XsrfRequestValidatorImpl xsrfRequestValidator(XsrfTokenValidator xsrfTokenValidator) {
        return new XsrfRequestValidatorImpl(xsrfTokenValidator);
    }

    @Bean
    public RefImplWebSudoManager webSudoManager(WebSudoSessionManager webSudoSessionManager) {
        return new RefImplWebSudoManager(webSudoSessionManager);
    }

    @Bean
    public RefimplProjectManager projectManager(PluginSettingsFactory pluginSettingsFactory) {
        return new RefimplProjectManager(pluginSettingsFactory);
    }

    @Bean
    public RefimplHostContextAccessor hostContextAccessor(BundleContext bundleContext) {
        return new RefimplHostContextAccessor(bundleContext);
    }

    @Bean
    public HostContextTransactionTemplate transactionTemplate(HostContextAccessor hostContextAccessor) {
        return new HostContextTransactionTemplate(hostContextAccessor);
    }

    @Bean
    public RefappAuthenticationController authenticationController(RoleMapper roleMapper) {
        return new RefappAuthenticationController(roleMapper);
    }

    @Bean
    public RefimplLifecycleManager lifecycleManager(PluginEventManager pluginEventManager,
                                                    PluginAccessor pluginAccessor,
                                                    BundleContext bundleContext) {
        return new RefimplLifecycleManager(pluginEventManager, pluginAccessor, bundleContext);
    }

    @Bean
    public RefimplSearchProvider searchProvider(ApplicationProperties applicationProperties) {
        return new RefimplSearchProvider(applicationProperties);
    }

    @Bean
    public RefimplUserSettingsService userSettingsService(com.atlassian.sal.api.user.UserManager userManager, ApplicationProperties applicationProperties) {
        return new RefimplUserSettingsService(userManager, applicationProperties);
    }

    @Bean
    public DefaultPermissionEnforcer defaultPermissionEnforcer(com.atlassian.sal.api.user.UserManager userManager) {
        return new DefaultPermissionEnforcer(userManager);
    }

    @Bean
    public DefaultTransactionalExecutorFactory defaultTransactionalExecutorFactory(HostConnectionAccessor hostConnectionAccessor) {
        return new DefaultTransactionalExecutorFactory(hostConnectionAccessor);
    }

    @Bean
    public RefImplHostConnectionAccessor refImplHostConnectionAccessory(ConnectionProvider connectionProvider) {
        return new RefImplHostConnectionAccessor(connectionProvider);
    }

    @Bean
    public DefaultComponentLocator defaultComponentLocator(HostContextAccessor hostContextAccessor) {
        return new DefaultComponentLocator(hostContextAccessor);
    }

    @Bean
    public DefaultPluginUpgradeManager defaultPluginUpgradeManager(List<PluginUpgradeTask> pluginUpgradeTasks,
                                                                   TransactionTemplate transactionTemplate,
                                                                   PluginAccessor pluginAccessor,
                                                                   PluginSettingsFactory pluginSettingsFactory,
                                                                   PluginEventManager pluginEventManager,
                                                                   ClusterLockService clusterLockService) {
        return new DefaultPluginUpgradeManager(pluginUpgradeTasks, transactionTemplate, pluginAccessor, pluginSettingsFactory, pluginEventManager, clusterLockService);
    }
}
