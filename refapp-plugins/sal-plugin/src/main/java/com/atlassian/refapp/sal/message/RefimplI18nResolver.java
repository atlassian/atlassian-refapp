package com.atlassian.refapp.sal.message;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.elements.ResourceDescriptor;
import com.atlassian.plugin.event.PluginEventListener;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.event.events.PluginDisabledEvent;
import com.atlassian.plugin.event.events.PluginEnabledEvent;
import com.atlassian.sal.core.message.AbstractI18nResolver;
import io.atlassian.util.concurrent.ManagedLock;
import io.atlassian.util.concurrent.ManagedLocks;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.springframework.beans.factory.InitializingBean;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static java.util.Objects.requireNonNull;
import static org.slf4j.LoggerFactory.getLogger;

/**
 * Returns the key with args as a string
 */
public class RefimplI18nResolver extends AbstractI18nResolver implements InitializingBean {
    private static final Logger log = getLogger(RefimplI18nResolver.class);
    private final ManagedLock.ReadWrite locks = ManagedLocks.manageReadWrite(new ReentrantReadWriteLock());
    private final Map<Plugin, Iterable<String>> pluginResourceBundleNames = new ConcurrentHashMap<>();

    private final PluginAccessor pluginAccessor;
    private final PluginEventManager pluginEventManager;
    private final ResourceBundleResolver resolver;

    public RefimplI18nResolver(
            final PluginAccessor pluginAccessor,
            final PluginEventManager pluginEventManager,
            final ResourceBundleResolver resolver
    ) {
        this.pluginAccessor = requireNonNull(pluginAccessor);
        this.pluginEventManager = requireNonNull(pluginEventManager);
        this.resolver = requireNonNull(resolver);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        pluginEventManager.register(this);
        locks.write().withLock(() -> addPluginResourceBundles(pluginAccessor.getEnabledPlugins()));
    }

    @Override
    public String getRawText(String key) {
        return StringUtils.defaultString(getPattern(Locale.getDefault(), key), key);
    }

    @Override
    public String getRawText(Locale locale, String key) {
        return StringUtils.defaultString(getPattern(locale, key), key);
    }

    public String resolveText(String key, Serializable[] arguments) {
        String pattern = getPattern(Locale.getDefault(), key);
        if (pattern == null) {
            return key;
        }
        return MessageFormat.format(pattern, (Object[]) arguments);
    }

    @Override
    public String resolveText(final Locale locale, final String key, final Serializable[] arguments) {
        String pattern = StringUtils.defaultString(getPattern(locale, key), getPattern(Locale.getDefault(), key));
        if (pattern == null) {
            return key;
        }
        return MessageFormat.format(pattern, (Object[]) arguments);
    }

    private String getPattern(final Locale locale, final String key) {
        return locks.read().withLock((Supplier<String>) () -> {
            String bundleString = null;
            for (Entry<Plugin, Iterable<String>> pluginBundleNames : pluginResourceBundleNames.entrySet()) {
                for (String bundleName : pluginBundleNames.getValue()) {
                    try {
                        ResourceBundle bundle = getBundle(bundleName, locale, pluginBundleNames.getKey());
                        bundleString = bundle.getString(key);
                    } catch (MissingResourceException e) {
                        // ignore, try next bundle
                    } catch (Exception e) {
                        log.error("Looking through: {} blew up while looking for the message: {}", bundleName, key, e);
                    }
                }
            }
            return bundleString;
        });
    }

        public Map<String, String> getAllTranslationsForPrefix(final String prefix) {
        requireNonNull(prefix);

        return locks.read().withLock((Supplier<Map<String, String>>) () -> {
            Map<String, String> translationsWithPrefix = new HashMap<>();
            for (Entry<Plugin, Iterable<String>> pluginBundleNames : pluginResourceBundleNames.entrySet()) {

                addMatchingTranslationsToMap(
                        prefix, Locale.getDefault(), pluginBundleNames.getKey(), pluginBundleNames.getValue(), translationsWithPrefix);
            }
            return translationsWithPrefix;
        });
    }

    public Map<String, String> getAllTranslationsForPrefix(final String prefix, final Locale locale) {
        requireNonNull(prefix);
        requireNonNull(locale);

        return locks.read().withLock((Supplier<Map<String, String>>) () -> {
            Map<String, String> translationsWithPrefix = new HashMap<>();
            for (Entry<Plugin, Iterable<String>> pluginBundleNames : pluginResourceBundleNames.entrySet()) {
                addMatchingTranslationsToMap(
                        prefix, locale, pluginBundleNames.getKey(), pluginBundleNames.getValue(), translationsWithPrefix);
            }
            return translationsWithPrefix;
        });
    }

    private void addMatchingTranslationsToMap(String prefix, Locale locale, Plugin plugin,
                                              Iterable<String> bundleNames,
                                              Map<String, String> translationsWithPrefix) {
        for (String bundleName : bundleNames) {
            try {
                ResourceBundle bundle = getBundle(bundleName, locale, plugin);
                if (bundle != null) {
                    addMatchingTranslationsToMap(prefix, bundle, translationsWithPrefix);
                }
            } catch (MissingResourceException e) {
                // OK, just ignore
            } catch (Exception e) {
                log.error("Loading translations from {} blew up", bundleName, e);
            }
        }
    }

    private void addMatchingTranslationsToMap(String prefix, ResourceBundle bundle,
                                              Map<String, String> translationsWithPrefix) {
        Enumeration<String> enumeration = bundle.getKeys();
        while (enumeration.hasMoreElements()) {
            String key = enumeration.nextElement();
            if (key.startsWith(prefix)) {
                translationsWithPrefix.put(key, bundle.getString(key));
            }
        }
    }

    @PluginEventListener
    public void onPluginEnabled(final PluginEnabledEvent event) {
        locks.write().withLock(() -> addPluginResourceBundles(event.getPlugin()));
    }

    @PluginEventListener
    public void onPluginDisabled(final PluginDisabledEvent event) {
        locks.write().withLock(() -> removePluginResourceBundles(event.getPlugin()));
    }

    private void addPluginResourceBundles(Iterable<Plugin> plugins) {
        for (Plugin plugin : plugins) {
            addPluginResourceBundles(plugin);
        }
    }

    private void addPluginResourceBundles(Plugin plugin) {
        List<String> bundleNames = plugin.getResourceDescriptors().stream()
                .filter(descriptor -> "i18n".equals(descriptor.getType()))
                .map(ResourceDescriptor::getLocation)
                .collect(Collectors.toList());
        addPluginResourceBundles(plugin, bundleNames);
    }

    private void addPluginResourceBundles(Plugin plugin, List<String> bundleNames) {
        pluginResourceBundleNames.put(plugin, bundleNames);
    }

    private void removePluginResourceBundles(Plugin plugin) {
        pluginResourceBundleNames.remove(plugin);
    }

    private ResourceBundle getBundle(String bundleName, Locale locale, Plugin plugin) {
        return resolver.getBundle(bundleName, locale, plugin.getClassLoader());
    }
}
