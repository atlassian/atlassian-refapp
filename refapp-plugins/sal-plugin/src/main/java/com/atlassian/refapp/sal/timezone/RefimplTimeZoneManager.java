package com.atlassian.refapp.sal.timezone;

import com.atlassian.sal.api.timezone.TimeZoneManager;
import com.atlassian.sal.api.user.UserKey;

import javax.annotation.Nonnull;
import java.util.TimeZone;

import static java.util.Objects.requireNonNull;

public class RefimplTimeZoneManager implements TimeZoneManager {
    @Nonnull
    public TimeZone getUserTimeZone() {
        return getDefaultTimeZone();
    }

    @Nonnull
    public TimeZone getDefaultTimeZone() {
        return TimeZone.getDefault();
    }

    @Override
    @Nonnull
    public TimeZone getUserTimeZone(@Nonnull UserKey user) {
        requireNonNull(user);
        return getDefaultTimeZone();
    }
}
