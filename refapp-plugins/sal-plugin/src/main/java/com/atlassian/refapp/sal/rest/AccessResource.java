package com.atlassian.refapp.sal.rest;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.refapp.sal.RefAppAccessManager;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * Rest endpoints to change the Refapp instance user access permission.
 * Refapp reads value of system property refapp.access.permission.global.anonymous and refapp.access.permission.global.unlicensed
 * when it launches. If system properties are not present, it doesn't allow neither global anonymous nor unlicensed access permission.
 * Using rest end points exposed here during test we can change Refapp instance user access permission.
 *
 * @since 5.5
 */
@AnonymousAllowed
@Path("access")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class AccessResource {
    private final RefAppAccessManager accessManager;

    public AccessResource(final RefAppAccessManager accessManager) {
        this.accessManager = assertNotNull(accessManager, "accessManager");
    }

    @GET
    public String getMe() {
        return "This is admin access resource";
    }

    @GET
    @Path("anonymous")
    public Response getGlobalAnonymousAccessPermission() {
        return Response.ok(accessManager.getGlobalAnonymousAccessPermission()).build();
    }

    @GET
    @Path("unlicensed")
    public Response getGlobalUnlicensedAccessPermission() {
        return Response.ok(accessManager.getGlobalUnlicensedAccessPermission()).build();
    }

    @PUT
    @Path("anonymous/{value}")
    public Response setGlobalAnonymousAccessPermission(@PathParam("value") String value) {
        accessManager.setGlobalAnonymousAccessPermission(Boolean.parseBoolean(value));
        return Response.ok(accessManager.getGlobalAnonymousAccessPermission()).build();
    }

    @PUT
    @Path("unlicensed/{value}")
    public Response setGlobalUnlicensedAccessPermission(@PathParam("value") String value) {
        accessManager.setGlobalUnlicensedAccessPermission(Boolean.parseBoolean(value));
        return Response.ok(accessManager.getGlobalUnlicensedAccessPermission()).build();
    }

    private static <T> T assertNotNull(final T reference, final Object errorMessage) {
        if (reference == null) {
            throw new IllegalArgumentException(String.valueOf(errorMessage));
        }
        return reference;
    }
}