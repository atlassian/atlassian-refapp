package com.atlassian.refapp.sal;

import com.atlassian.plugin.refimpl.saldeps.ServletContextThreadLocal;
import com.atlassian.refapp.sal.license.RefappProduct;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.UrlMode;

import javax.annotation.Nonnull;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Path;
import java.util.Date;
import java.util.Optional;
import java.util.Properties;

/**
 * Implementation of ApplicationProperties
 */
public class RefimplApplicationProperties implements ApplicationProperties {
    private static final Date THEN = new Date();

    private static final String APPLICATION_PROPERTIES = "application.properties";
    private static final String REFAPP_VERSION_PROPERTY = "refapp.version";

    private final Properties applicationProperties;

    public RefimplApplicationProperties() throws IOException {
        applicationProperties = new Properties();
        applicationProperties.load(getClass().getResourceAsStream(APPLICATION_PROPERTIES));
    }

    @Override
    @Deprecated
    public String getBaseUrl() {
        Optional<String> absoluteUrl = getAbsoluteBaseUrlFromRequest();
        return absoluteUrl.orElseGet(this::getCanonicalBaseUrl);
    }

    private Optional<String> getAbsoluteBaseUrlFromRequest() {
        HttpServletRequest request = ServletContextThreadLocal.getRequest();
        if (request != null) {
            return Optional.of(HttpServletRequestBaseUrlExtractor.extractBaseUrl(request));
        }
        return Optional.empty();
    }

    private Optional<String> getContextPathFromRequest() {
        HttpServletRequest request = ServletContextThreadLocal.getRequest();
        if (request != null) {
            return Optional.of(request.getContextPath());
        }
        return Optional.empty();
    }

    private String getCanonicalBaseUrl() {
        String baseurl = System.getProperty("baseurl");
        if (baseurl == null)
            throw new IllegalStateException("baseurl system property is not configured");
        return baseurl;
    }

    private String getCanonicalContextPath() {
        String baseUrl = getCanonicalBaseUrl();
        try {
            return new URL(baseUrl).getPath();
        } catch (MalformedURLException e) {
            throw new IllegalStateException("Base URL misconfigured", e);
        }
    }

    @Override
    @Nonnull
    public String getBaseUrl(UrlMode urlMode) {
        switch (urlMode) {
            case ABSOLUTE: {
                Optional<String> absoluteUrl = getAbsoluteBaseUrlFromRequest();
                return absoluteUrl.orElseGet(this::getCanonicalBaseUrl);
            }
            case CANONICAL:
                return getCanonicalBaseUrl();
            case RELATIVE: {
                Optional<String> contextPath = getContextPathFromRequest();
                return contextPath.orElseGet(this::getCanonicalContextPath);
            }
            case RELATIVE_CANONICAL:
                return getCanonicalContextPath();
            case AUTO: {
                Optional<String> contextPath = getContextPathFromRequest();
                return contextPath.orElseGet(this::getCanonicalBaseUrl);
            }
            default:
                throw new IllegalStateException("Unhandled UrlMode " + urlMode);
        }
    }

    @Override
    @Nonnull
    public String getDisplayName() {
        return RefappProduct.REFAPP.getName();
    }

    @Override
    @Nonnull
    public String getPlatformId() {
        return RefappProduct.REFAPP.getNamespace();
    }

    @Override
    @Nonnull
    public String getVersion() {
        return applicationProperties.getProperty(REFAPP_VERSION_PROPERTY);
    }

    @Override
    @Nonnull
    public Date getBuildDate() {
        return THEN;
    }

    @Override
    @Nonnull
    public String getBuildNumber() {
        return "123";
    }

    @Override
    @Nonnull
    public File getHomeDirectory() {
        return new File(System.getProperty("refapp.home", System.getProperty("java.io.tmpdir")));
    }

    @Nonnull
    @Override
    public Optional<Path> getLocalHomeDirectory() {
        return Optional.of(getHomeDirectory().toPath());
    }

    @Nonnull
    @Override
    public Optional<Path> getSharedHomeDirectory() {
        return Optional.of(getHomeDirectory().toPath());
    }

    @Override
    @Deprecated
    public String getPropertyValue(String s) {
        return null;
    }

    @Override
    @Nonnull
    public String getApplicationFileEncoding() {
        return "UTF-8";
    }
}
