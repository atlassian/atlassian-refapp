package com.atlassian.refapp.sal;

/**
 * Spring beans relating to the Refapp instance user access permission .
 *
 * @since 5.5
 */
public interface RefAppAccessManager {

    /**
     * @return true if the current refapp instance has global anonymous access permission, false otherwise.
     */
    boolean getGlobalAnonymousAccessPermission();

    /**
     * @return true if the current refapp instance has global unlicensed access permission, false otherwise.
     */
    boolean getGlobalUnlicensedAccessPermission();

    /**
     * Set true if the current refapp instance wants to allow global anonymous access permission, false otherwise.
     */
    void setGlobalAnonymousAccessPermission(boolean value);

    /**
     * Set true if the current refapp instance wants to allow global unlicensed access permission, false otherwise.
     */
    void setGlobalUnlicensedAccessPermission(boolean value);
}
