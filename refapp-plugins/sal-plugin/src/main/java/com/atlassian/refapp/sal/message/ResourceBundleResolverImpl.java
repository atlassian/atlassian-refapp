package com.atlassian.refapp.sal.message;

import javax.annotation.Nonnull;
import java.util.Locale;
import java.util.ResourceBundle;

import static java.util.Objects.requireNonNull;

public class ResourceBundleResolverImpl implements ResourceBundleResolver {
    @Override
    public ResourceBundle getBundle(@Nonnull String bundleName, @Nonnull Locale locale, @Nonnull ClassLoader classLoader) {
        requireNonNull(bundleName);
        requireNonNull(locale);
        requireNonNull(classLoader);
        return ResourceBundle.getBundle(bundleName, locale, classLoader);
    }
}
