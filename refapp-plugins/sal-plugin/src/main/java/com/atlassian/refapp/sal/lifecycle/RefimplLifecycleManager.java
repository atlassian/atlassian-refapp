package com.atlassian.refapp.sal.lifecycle;

import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.sal.core.lifecycle.DefaultLifecycleManager;
import org.osgi.framework.BundleContext;

public class RefimplLifecycleManager extends DefaultLifecycleManager {
    public RefimplLifecycleManager(
            final PluginEventManager pluginEventManager,
            final PluginAccessor pluginAccessor,
            final BundleContext bundleContext) {
        super(pluginEventManager, pluginAccessor, bundleContext);
    }

    public boolean isApplicationSetUp() {
        return true;
    }
}
