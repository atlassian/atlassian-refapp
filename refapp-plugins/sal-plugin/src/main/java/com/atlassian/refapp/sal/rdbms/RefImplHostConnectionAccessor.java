package com.atlassian.refapp.sal.rdbms;

import com.atlassian.refapp.api.ConnectionProvider;
import com.atlassian.sal.api.rdbms.ConnectionCallback;
import com.atlassian.sal.api.rdbms.RdbmsException;
import com.atlassian.sal.spi.HostConnectionAccessor;
import com.atlassian.util.profiling.Metrics;
import com.atlassian.util.profiling.Ticker;
import io.atlassian.fugue.Option;

import javax.annotation.Nonnull;
import java.sql.Connection;
import java.sql.SQLException;

@SuppressWarnings("unused")
public class RefImplHostConnectionAccessor implements HostConnectionAccessor {

    private static final String CALLBACK_TAG_KEY = "callback";
    private static final String DB_CONNECTION_EXECUTE_METRIC = "db.connection.execute";
    private static final String ERROR = ".error";
    private final ConnectionProvider connectionProvider;

    public RefImplHostConnectionAccessor(ConnectionProvider connectionProvider) {
        this.connectionProvider = connectionProvider;
    }

    @Override
    public <A> A execute(boolean readOnly, boolean newTransaction, @Nonnull ConnectionCallback<A> callback) {
        try (Ticker ignored = Metrics.metric(DB_CONNECTION_EXECUTE_METRIC)
                .tag(CALLBACK_TAG_KEY, callback.getClass().getCanonicalName())
                .withInvokerPluginKey().withAnalytics().startLongRunningTimer()
        ) {
            // retrieve and sanitise the connection
            try (final Connection connection = connectionProvider.connection()) {
                connection.setAutoCommit(false);
                connection.setReadOnly(readOnly);

                // we can't support execution in existing or new transaction context, because refapp doesn't have one
                return callback.execute(connection);
            } catch (SQLException e) {

                // not much we can do here but throw, so that a rollback occurs
                Metrics.metric(DB_CONNECTION_EXECUTE_METRIC + ERROR)
                        .tag(CALLBACK_TAG_KEY, callback.getClass().getCanonicalName())
                        .withInvokerPluginKey().withAnalytics().histogram().update(1L);
                throw new RdbmsException("refapp unable to execute against database connection", e);
            }
        }
    }

    @Nonnull
    @Override
    public Option<String> getSchemaName() {
        return Option.option(connectionProvider.schema().orElse(null));
    }
}
