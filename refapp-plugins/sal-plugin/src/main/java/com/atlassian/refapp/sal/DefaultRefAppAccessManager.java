package com.atlassian.refapp.sal;

/**
 *
 * Default implementation of spring beans relating to the Refapp instance user access permission.
 * Refapp reads value of system property refapp.access.permission.global.anonymous and refapp.access.permission.global.unlicensed
 * when it launches. If system properties are not present, it doesn't allow neither global anonymous nor unlicensed access permission.
 *
 * @since 5.5
 */
public class DefaultRefAppAccessManager implements RefAppAccessManager {
    public static final String GLOBAL_ANONYMOUS_ACCESS_PERMISSION_KEY = "refapp.access.permission.global.anonymous";
    public static final String GLOBAL_UNLICENSED_ACCESS_PERMISSION_KEY = "refapp.access.permission.global.unlicensed";

    private boolean globalAnonymousAccessPermission;
    private boolean globalUnlicensedAccessPermission;

    public DefaultRefAppAccessManager() {
        this.globalAnonymousAccessPermission = Boolean.parseBoolean(System.getProperty(GLOBAL_ANONYMOUS_ACCESS_PERMISSION_KEY, String.valueOf(false)));
        this.globalUnlicensedAccessPermission = Boolean.parseBoolean(System.getProperty(GLOBAL_UNLICENSED_ACCESS_PERMISSION_KEY, String.valueOf(false)));
    }

    public boolean getGlobalAnonymousAccessPermission() {
        return globalAnonymousAccessPermission;
    }

    public boolean getGlobalUnlicensedAccessPermission() {
        return globalUnlicensedAccessPermission;
    }

    public void setGlobalAnonymousAccessPermission(boolean value) {
        globalAnonymousAccessPermission = value;
    }

    public void setGlobalUnlicensedAccessPermission(boolean value) {
        globalUnlicensedAccessPermission = value;
    }
}