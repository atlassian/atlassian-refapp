package com.atlassian.refapp.sal.executor;

import com.atlassian.sal.api.executor.ThreadLocalContextManager;
import com.atlassian.sal.core.executor.DefaultThreadLocalDelegateExecutorFactory;

public class RefImplThreadLocalDelegateExecutorFactory<C> extends DefaultThreadLocalDelegateExecutorFactory<C> {

    public RefImplThreadLocalDelegateExecutorFactory(ThreadLocalContextManager<C> manager) {
        super(manager);
    }
}
