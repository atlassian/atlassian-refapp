package com.atlassian.refapp.sal.spring;

import com.atlassian.plugin.refimpl.saldeps.CookieBasedScopeManager;
import com.atlassian.plugin.scope.ScopeManager;
import com.atlassian.refapp.sal.DefaultRefAppAccessManager;
import com.atlassian.refapp.sal.RefAppAccessManager;
import com.atlassian.refapp.sal.RefimplApplicationProperties;
import com.atlassian.refapp.sal.RefimplHostContextAccessor;
import com.atlassian.refapp.sal.auth.RefappAuthenticationController;
import com.atlassian.refapp.sal.executor.RefImplThreadLocalDelegateExecutorFactory;
import com.atlassian.refapp.sal.executor.RefimplThreadLocalContextManager;
import com.atlassian.refapp.sal.license.RefimplLicenseHandler;
import com.atlassian.refapp.sal.lifecycle.RefimplLifecycleManager;
import com.atlassian.refapp.sal.message.RefImplHelpPathResolver;
import com.atlassian.refapp.sal.message.RefimplI18nResolver;
import com.atlassian.refapp.sal.message.RefimplLocaleResolver;
import com.atlassian.refapp.sal.pluginsettings.RefimplPluginSettingsFactory;
import com.atlassian.refapp.sal.project.RefimplProjectManager;
import com.atlassian.refapp.sal.search.RefimplSearchProvider;
import com.atlassian.refapp.sal.search.query.DefaultSearchQueryParser;
import com.atlassian.refapp.sal.timezone.RefimplTimeZoneManager;
import com.atlassian.refapp.sal.user.RefImplUserManager;
import com.atlassian.refapp.sal.usersettings.RefimplUserSettingsService;
import com.atlassian.refapp.sal.web.context.RefImplHttpContext;
import com.atlassian.refapp.sal.websudo.RefImplWebSudoManager;
import com.atlassian.refapp.sal.xsrf.RefappXsrfTokenValidatorBypassingUPM;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.auth.AuthenticationController;
import com.atlassian.sal.api.auth.AuthenticationListener;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.auth.OAuthRequestVerifierFactory;
import com.atlassian.sal.api.executor.ThreadLocalContextManager;
import com.atlassian.sal.api.executor.ThreadLocalDelegateExecutorFactory;
import com.atlassian.sal.api.features.DarkFeatureManager;
import com.atlassian.sal.api.license.LicenseHandler;
import com.atlassian.sal.api.lifecycle.LifecycleAware;
import com.atlassian.sal.api.lifecycle.LifecycleManager;
import com.atlassian.sal.api.message.HelpPathResolver;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.message.LocaleResolver;
import com.atlassian.sal.api.net.NonMarshallingRequestFactory;
import com.atlassian.sal.api.net.RequestFactory;
import com.atlassian.sal.api.permission.PermissionEnforcer;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.project.ProjectManager;
import com.atlassian.sal.api.rdbms.TransactionalExecutorFactory;
import com.atlassian.sal.api.scheduling.PluginScheduler;
import com.atlassian.sal.api.search.SearchProvider;
import com.atlassian.sal.api.search.query.SearchQueryParser;
import com.atlassian.sal.api.timezone.TimeZoneManager;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.sal.api.upgrade.PluginUpgradeManager;
import com.atlassian.sal.api.usersettings.UserSettingsService;
import com.atlassian.sal.api.web.context.HttpContext;
import com.atlassian.sal.api.websudo.WebSudoManager;
import com.atlassian.sal.api.xsrf.XsrfRequestValidator;
import com.atlassian.sal.api.xsrf.XsrfTokenAccessor;
import com.atlassian.sal.api.xsrf.XsrfTokenValidator;
import com.atlassian.sal.core.auth.OAuthRequestVerifierFactoryImpl;
import com.atlassian.sal.core.auth.SeraphAuthenticationListener;
import com.atlassian.sal.core.auth.SeraphLoginUriProvider;
import com.atlassian.sal.core.features.DefaultDarkFeatureManager;
import com.atlassian.sal.core.net.HttpClientRequestFactory;
import com.atlassian.sal.core.scheduling.TimerPluginScheduler;
import com.atlassian.sal.core.transaction.HostContextTransactionTemplate;
import com.atlassian.sal.core.upgrade.DefaultPluginUpgradeManager;
import com.atlassian.sal.core.xsrf.IndependentXsrfTokenAccessor;
import com.atlassian.sal.core.xsrf.XsrfRequestValidatorImpl;
import com.atlassian.sal.spi.HostContextAccessor;
import org.osgi.framework.ServiceRegistration;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.security.Principal;

import static com.atlassian.plugins.osgi.javaconfig.ExportOptions.as;
import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.exportOsgiService;

@Configuration
public class ExportedBeans {

    @Bean
    public FactoryBean<ServiceRegistration> exportThreadLocalContextManager(RefimplThreadLocalContextManager threadLocalContextManager) {
        return exportOsgiService(threadLocalContextManager, as(ThreadLocalContextManager.class));
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportThreadLocalDelegateExecutorFactory(RefImplThreadLocalDelegateExecutorFactory<Principal> threadLocalDelegateExecutorFactory) {
        return exportOsgiService(threadLocalDelegateExecutorFactory, as(ThreadLocalDelegateExecutorFactory.class));
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportI18Resolver(RefimplI18nResolver i18nResolver) {
        return exportOsgiService(i18nResolver, as(I18nResolver.class));
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportUserManager(RefImplUserManager userManager) {
        return exportOsgiService(userManager, as(com.atlassian.sal.api.user.UserManager.class));
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportApplicationProperties(RefimplApplicationProperties applicationProperties) {
        return exportOsgiService(applicationProperties, as(ApplicationProperties.class));
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportPluginSettingsFactory(RefimplPluginSettingsFactory pluginSettingsFactory) {
        return exportOsgiService(pluginSettingsFactory, as(PluginSettingsFactory.class));
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportLoginUriProvider(SeraphLoginUriProvider loginUriProvider) {
        return exportOsgiService(loginUriProvider, as(LoginUriProvider.class));
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportClientRequestFactory(HttpClientRequestFactory clientRequestFactory) {
        return exportOsgiService(clientRequestFactory, as(NonMarshallingRequestFactory.class, RequestFactory.class));
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportDarkFeatureManager(DefaultDarkFeatureManager darkFeatureManager) {
        return exportOsgiService(darkFeatureManager, as(DarkFeatureManager.class));
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportXsrfTokenAccessor(IndependentXsrfTokenAccessor xsrfTokenAccessor) {
        return exportOsgiService(xsrfTokenAccessor, as(XsrfTokenAccessor.class));
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportXsrfTokenValidator(RefappXsrfTokenValidatorBypassingUPM xsrfTokenValidator) {
        return exportOsgiService(xsrfTokenValidator, as(XsrfTokenValidator.class));
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportXsrfRequestValidator(XsrfRequestValidatorImpl xsrfRequestValidator) {
        return exportOsgiService(xsrfRequestValidator, as(XsrfRequestValidator.class));
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportWebSudoManager(RefImplWebSudoManager webSudoManager) {
        return exportOsgiService(webSudoManager, as(WebSudoManager.class));
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportHttpContext(RefImplHttpContext httpContext) {
        return exportOsgiService(httpContext, as(HttpContext.class));
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportScopeManager(CookieBasedScopeManager scopeManager) {
        return exportOsgiService(scopeManager, as(ScopeManager.class));
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportHelpPathResolver(RefImplHelpPathResolver helpPathResolver) {
        return exportOsgiService(helpPathResolver, as(HelpPathResolver.class));
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportLocaleResolver(RefimplLocaleResolver localeResolver) {
        return exportOsgiService(localeResolver, as(LocaleResolver.class));
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportProjectManager(RefimplProjectManager projectManager) {
        return exportOsgiService(projectManager, as(ProjectManager.class));
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportHostContextAccessor(RefimplHostContextAccessor hostContextAccessor) {
        return exportOsgiService(hostContextAccessor, as(HostContextAccessor.class));
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportTransactionTemplate(HostContextTransactionTemplate transactionTemplate) {
        return exportOsgiService(transactionTemplate, as(TransactionTemplate.class));
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportAuthenticationController(RefappAuthenticationController authenticationController) {
        return exportOsgiService(authenticationController, as(AuthenticationController.class));
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportLicenseHandler(RefimplLicenseHandler licenseHandler) {
        return exportOsgiService(licenseHandler, as(LicenseHandler.class));
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportLifecycleManager(RefimplLifecycleManager lifecycleManager) {
        return exportOsgiService(lifecycleManager, as(LifecycleManager.class));
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportSearchProvider(RefimplSearchProvider searchProvider) {
        return exportOsgiService(searchProvider, as(SearchProvider.class));
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportSearchQueryParser(DefaultSearchQueryParser searchQueryParser) {
        return exportOsgiService(searchQueryParser, as(SearchQueryParser.class));
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportTimeZoneManager(RefimplTimeZoneManager timeZoneManager) {
        return exportOsgiService(timeZoneManager, as(TimeZoneManager.class));
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportUserSettingsService(RefimplUserSettingsService userSettingsService) {
        return exportOsgiService(userSettingsService, as(UserSettingsService.class));
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportOAuthRequestVerifierFactory(OAuthRequestVerifierFactoryImpl oAuthRequestVerifierFactory) {
        return exportOsgiService(oAuthRequestVerifierFactory, as(OAuthRequestVerifierFactory.class));
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportAuthenticationListener(SeraphAuthenticationListener authenticationListener) {
        return exportOsgiService(authenticationListener, as(AuthenticationListener.class));
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportTimerPluginScheduler(TimerPluginScheduler timerPluginScheduler) {
        return exportOsgiService(timerPluginScheduler, as(PluginScheduler.class));
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportPermissionEnforcer(PermissionEnforcer permissionEnforcer) {
        return exportOsgiService(permissionEnforcer, as(PermissionEnforcer.class));
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportTransactionalExecutorFactory(TransactionalExecutorFactory transactionalExecutorFactory) {
        return exportOsgiService(transactionalExecutorFactory, as(TransactionalExecutorFactory.class));
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportDefaultPluginUpgradeManager(DefaultPluginUpgradeManager defaultPluginUpgradeManager) {
        return exportOsgiService(defaultPluginUpgradeManager, as(LifecycleAware.class, PluginUpgradeManager.class));
    }
}
