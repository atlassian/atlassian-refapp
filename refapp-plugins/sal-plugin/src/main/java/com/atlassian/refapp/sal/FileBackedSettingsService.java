package com.atlassian.refapp.sal;

import io.atlassian.fugue.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.AbstractMap;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

public abstract class FileBackedSettingsService {
    private static final Logger log = LoggerFactory.getLogger(FileBackedSettingsService.class);

    protected final File file;
    protected final Properties properties;

    public FileBackedSettingsService(Pair<File, Properties> fileAndProperties) {
        this.file = fileAndProperties.left();
        this.properties = fileAndProperties.right();
    }

    /**
     * In-memory settings, for testing only.
     */
    protected FileBackedSettingsService() {
        this.file = null;
        this.properties = new Properties();
    }

    @SuppressWarnings("AccessToStaticFieldLockedOnInstance")
    private synchronized void store() {
        if (this.file == null || !this.file.canWrite()) {
            // Read only settings
            return;
        }
        try (OutputStream os = Files.newOutputStream(file.toPath())) {
            properties.storeToXML(os, "SAL Reference Implementation settings");
        } catch (IOException ioe) {
            log.error("Error storing properties", ioe);
        }
    }

    public class SettingsMap extends AbstractMap<String, String> {
        private final String settingsKey;

        public SettingsMap(String settingsKey) {
            if (settingsKey == null) {
                this.settingsKey = "global.";
            } else {
                this.settingsKey = "keys." + settingsKey + ".";
            }
        }

        @Nonnull
        public Set<Entry<String, String>> entrySet() {
            Set<Entry<String, String>> set = new HashSet<>();

            for (Entry<Object, Object> entry : properties.entrySet()) {
                final String key = (String) entry.getKey();
                if (key.startsWith(this.settingsKey)) {
                    set.add(new AbstractMap.SimpleEntry<>(key.substring(this.settingsKey.length()), (String) entry.getValue()));
                }
            }

            return set;
        }

        @Override
        public String get(Object key) {
            return properties.getProperty(settingsKey + key);
        }

        @Override
        public String put(String key, String value) {
            String result = (String) properties.setProperty(settingsKey + key, value);
            store();
            return result;
        }

        @Override
        public String remove(Object key) {
            String result = (String) properties.remove(settingsKey + key);
            store();
            return result;
        }

        @Override
        public void clear() {
            properties.keySet()
                    .removeIf(object -> object instanceof String && ((String) object).startsWith(settingsKey));
            store();
        }
    }
}
