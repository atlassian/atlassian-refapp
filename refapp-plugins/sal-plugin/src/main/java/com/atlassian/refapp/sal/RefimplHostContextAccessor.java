package com.atlassian.refapp.sal;

import com.atlassian.sal.spi.HostContextAccessor;
import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;

import java.util.HashMap;
import java.util.Map;

public class RefimplHostContextAccessor implements HostContextAccessor {
    private final BundleContext bundleContext;

    public RefimplHostContextAccessor(final BundleContext bundleContext) {
        this.bundleContext = bundleContext;
    }

    public Object doInTransaction(HostTransactionCallback callback) {
        return callback.doInTransaction();
    }

    public <T> Map<String, T> getComponentsOfType(Class<T> iface) {
        ServiceReference<?>[] refs;
        try {
            refs = bundleContext.getServiceReferences(iface.getName(), null);
        } catch (InvalidSyntaxException e) {
            // will not happen - we pass null as a filter. Woo for checked exceptions!
            throw new RuntimeException("Invalid filter", e);
        }
        Map<String, T> objs = new HashMap<>();
        if (refs != null) {
            for (ServiceReference<?> ref : refs) {
                objs.put("", (T) bundleContext.getService(ref));
            }
        }
        return objs;
    }
}
