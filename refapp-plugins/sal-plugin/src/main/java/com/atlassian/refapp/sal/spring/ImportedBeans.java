package com.atlassian.refapp.sal.spring;

import com.atlassian.beehive.ClusterLockService;
import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.refapp.api.ConnectionProvider;
import com.atlassian.refapp.api.EmbeddedCrowdAuthenticator;
import com.atlassian.refapp.auth.external.WebSudoSessionManager;
import com.atlassian.sal.api.upgrade.PluginUpgradeTask;
import com.atlassian.seraph.auth.AuthenticationContext;
import com.atlassian.seraph.auth.RoleMapper;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.factoryBeanForOsgiServiceCollection;
import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.importOsgiService;
import static com.atlassian.plugins.osgi.javaconfig.ServiceCollection.list;

@Configuration
public class ImportedBeans {

    @Bean
    public AuthenticationContext importAuthenticationContext() {
        return importOsgiService(AuthenticationContext.class);
    }

    @Bean
    public EmbeddedCrowdAuthenticator importEmbeddedCrowdAuthenticator() {
        return importOsgiService(EmbeddedCrowdAuthenticator.class);
    }

    @Bean
    public PluginAccessor importPluginAccessor() {
        return importOsgiService(PluginAccessor.class);
    }

    @Bean
    public PluginEventManager importPluginEventManager() {
        return importOsgiService(PluginEventManager.class);
    }

    @Bean
    public WebSudoSessionManager importWebSudoSessionManager() {
        return importOsgiService(WebSudoSessionManager.class);
    }

    @Bean
    public RoleMapper importRoleMapper() {
        return importOsgiService(RoleMapper.class);
    }

    @Bean
    public ConnectionProvider connectionProvider() {
        return importOsgiService(ConnectionProvider.class);
    }

    @Bean
    public ClusterLockService clusterLockService() {
        return importOsgiService(ClusterLockService.class);
    }

    @Bean
    public FactoryBean<List<PluginUpgradeTask>> upgradeTasks() {
        return factoryBeanForOsgiServiceCollection(list(PluginUpgradeTask.class));
    }

    @Bean
    public CrowdService crowdService() {
        return importOsgiService(CrowdService.class);
    }
}