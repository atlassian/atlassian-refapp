package com.atlassian.refapp.sal.message;

import com.atlassian.sal.api.message.LocaleResolver;
import com.atlassian.sal.api.user.UserKey;

import javax.servlet.http.HttpServletRequest;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

public class RefimplLocaleResolver implements LocaleResolver {
    @Override
    public Locale getLocale(HttpServletRequest request) {
        String country = request.getParameter("locale.country");
        String lang = request.getParameter("locale.lang");
        String variant = request.getParameter("locale.variant");

        if (lang != null && country != null && variant != null) {
            return new Locale(lang, country, variant);
        } else if (lang != null && country != null) {
            return new Locale(lang, country);
        } else if (lang != null) {
            return new Locale(lang);
        } else if (request.getLocale() != null) {
            return request.getLocale();
        }

        return getLocale();
    }

    @Override
    public Locale getLocale() {
        return Locale.getDefault();
    }

    @Override
    public Locale getLocale(UserKey userKey) {
        return Locale.getDefault();
    }

    @Override
    public Set<Locale> getSupportedLocales() {
        final Set<Locale> ret = new HashSet<>();
        ret.add(new Locale("en", "AU"));
        ret.add(Locale.US);
        ret.add(Locale.ENGLISH);
        ret.add(Locale.FRENCH);
        ret.add(Locale.GERMAN);
        return ret;
    }

    @Override
    public Locale getApplicationLocale() { return Locale.getDefault(); }

}
