package com.atlassian.refapp.sal.xsrf;


import com.atlassian.sal.api.xsrf.XsrfTokenAccessor;
import com.atlassian.sal.api.xsrf.XsrfTokenValidator;
import com.atlassian.sal.core.xsrf.IndependentXsrfTokenValidator;

import javax.servlet.http.HttpServletRequest;

/**
 * A custom XSRF token validator, bypassing the XSRF checks only for UPM
 * <p>
 * TODO: Remove it after upm fixes XSRF (REFAPP-467, UPM-4972)
 */
public class RefappXsrfTokenValidatorBypassingUPM extends IndependentXsrfTokenValidator implements XsrfTokenValidator {
    private static final String UPM_REQUEST_URI = "/refapp/rest/plugins/1.0/";

    public RefappXsrfTokenValidatorBypassingUPM(final XsrfTokenAccessor accessor) {
        super(accessor);
    }

    @Override
    public boolean validateFormEncodedToken(final HttpServletRequest request) {
        if (request.getRequestURI().startsWith(UPM_REQUEST_URI)) {
            return true;
        }
        return super.validateFormEncodedToken(request);
    }
}
