package com.atlassian.refapp.sal.user;

import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.search.builder.Restriction;
import com.atlassian.crowd.search.query.entity.GroupQuery;
import com.atlassian.crowd.search.query.entity.restriction.NullRestrictionImpl;
import com.atlassian.crowd.search.query.entity.restriction.constants.GroupTermKeys;
import com.atlassian.refapp.api.EmbeddedCrowdAuthenticator;
import com.atlassian.refapp.sal.RefAppAccessManager;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserProfile;
import com.atlassian.sal.api.user.UserResolutionException;
import com.atlassian.seraph.auth.AuthenticationContext;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;
import java.security.Principal;

/**
 * Pretends the 'someUser' is logged in and is an admin
 */
public class RefImplUserManager implements com.atlassian.sal.api.user.UserManager {
    private final Logger log = LoggerFactory.getLogger(getClass());

    private final AuthenticationContext authenticationContext;
    private final EmbeddedCrowdAuthenticator authenticator;
    private final RefAppAccessManager accessManager;
    private final CrowdService crowdService;

    public RefImplUserManager(final AuthenticationContext authenticationContext, final EmbeddedCrowdAuthenticator authenticator, final RefAppAccessManager accessManager, CrowdService crowdService) {
        this.authenticationContext = assertNotNull(authenticationContext, "authenticationContext");
        this.authenticator = assertNotNull(authenticator, "authenticator");
        this.accessManager = assertNotNull(accessManager, "accessManager");
        this.crowdService = crowdService;
    }

    public String getRemoteUsername() {
        final Principal user = authenticationContext.getUser();
        if (user == null)
            return null;
        return user.getName();
    }

    @Override
    public UserProfile getRemoteUser() {
        final Principal reqUser = authenticationContext.getUser();
        if (reqUser == null)
            return null;

        return getUserProfile(reqUser.getName());
    }

    @Nullable
    @Override
    public UserKey getRemoteUserKey() {
        final Principal reqUser = authenticationContext.getUser();
        if (reqUser == null)
            return null;

        return new UserKey(reqUser.getName());
    }

    public String getRemoteUsername(final HttpServletRequest request) {
        return request.getRemoteUser();
    }

    @Override
    public UserProfile getRemoteUser(HttpServletRequest request) {
        return getUserProfile(request.getRemoteUser());
    }

    @Nullable
    @Override
    public UserKey getRemoteUserKey(HttpServletRequest request) {
        final String username = request.getRemoteUser();
        return (username != null) ? new UserKey(username) : null;
    }

    public UserProfile getUserProfile(String username) {
        final User user = authenticator.findUserByName(username);

        return user != null ? new RefimplUserProfile(user) : null;
    }

    @Nullable
    @Override
    public UserProfile getUserProfile(@Nullable UserKey userKey) {
        if (userKey == null) {
            return null;
        }
        return getUserProfile(userKey.getStringValue());
    }

    public boolean isSystemAdmin(final String username) {
        return isUserInGroup(username, "system_administrators");
    }

    @Override
    public boolean isSystemAdmin(UserKey userKey) {
        if (userKey != null) {
            return isSystemAdmin(userKey.getStringValue());
        }
        return false;
    }

    public boolean isAdmin(final String username) {
        return isSystemAdmin(username) || isUserInGroup(username, "administrators");
    }

    @Override
    public boolean isAdmin(UserKey userKey) {
        if (userKey != null) {
            return isAdmin(userKey.getStringValue());
        }
        return false;
    }

    @Override
    public boolean isLicensed(@Nullable UserKey userKey) {
        if (userKey != null) {
            String username = userKey.getStringValue();
            return isSystemAdmin(username) || isAdmin(username) || isUserInGroup(username, "users");
        }
        return false;
    }

    public boolean authenticate(final String username, final String password) {
        return authenticator.authenticateUser(username, password) != null;
    }

    public boolean isUserInGroup(final String username, final String group) {
        return crowdService.isUserMemberOfGroup(username, group);
    }

    @Override
    public boolean isUserInGroup(UserKey userKey, String groupName) {
        if (userKey != null) {
            return isUserInGroup(userKey.getStringValue(), groupName);
        }
        return false;
    }

    public Principal resolve(final String username) throws UserResolutionException {
        return authenticator.findUserByName(username);
    }

    @Override
    public Iterable<String> findGroupNamesByPrefix(String prefix, int startIndex, int maxResults) {
        GroupQuery<String> groupQuery = new GroupQuery<String>(String.class,
                                                               GroupType.GROUP,
                                                               StringUtils.isBlank(prefix) ? NullRestrictionImpl.INSTANCE : Restriction.on(GroupTermKeys.NAME).startingWith(prefix),
                                                               startIndex,
                                                               maxResults);

        return crowdService.search(groupQuery);
    }

    @Override
    public boolean isAnonymousAccessEnabled() {
        return accessManager.getGlobalAnonymousAccessPermission();
    }

    @Override
    public boolean isLimitedUnlicensedAccessEnabled() {
        return accessManager.getGlobalUnlicensedAccessPermission();
    }

    /**
     * Check that {@code reference} is not {@code null}. If it is, throw a
     * {@code IllegalArgumentException}.
     *
     * @param reference    reference to check is {@code null} or not
     * @param errorMessage com.atlassian.refapp.sal.message passed to the {@code IllegalArgumentException} constructor
     *                     to give more context when debugging
     * @return {@code reference} so it may be used
     * @throws IllegalArgumentException if {@code reference} is {@code null}
     */
    private static <T> T assertNotNull(final T reference, final Object errorMessage) {
        if (reference == null) {
            throw new IllegalArgumentException(String.valueOf(errorMessage));
        }
        return reference;
    }
}
