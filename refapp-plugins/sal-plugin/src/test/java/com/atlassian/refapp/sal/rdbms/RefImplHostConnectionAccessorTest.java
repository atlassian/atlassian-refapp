package com.atlassian.refapp.sal.rdbms;

import com.atlassian.refapp.api.ConnectionProvider;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.sql.Connection;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class RefImplHostConnectionAccessorTest {
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();
    @Mock
    private ConnectionProvider connectionProvider;
    @Mock
    private Connection connection;
    @InjectMocks
    private RefImplHostConnectionAccessor refImplHostConnectionAccessor;

    @Before
    public void setUp() throws Exception {
        when(connectionProvider.connection()).thenReturn(connection);
    }

    @Test
    public void connectionClosed() throws Exception {
        refImplHostConnectionAccessor.execute(true, false, connection -> null);
        verify(connection).close();
    }
}