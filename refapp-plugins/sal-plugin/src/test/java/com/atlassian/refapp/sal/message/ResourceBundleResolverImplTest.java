package com.atlassian.refapp.sal.message;

import org.junit.Test;

import static java.util.Locale.US;
import static org.mockito.Mockito.mock;

public class ResourceBundleResolverImplTest {
    @Test(expected = NullPointerException.class)
    public void nullBundleNameShouldThrowNpeAsPerApiSpec() {
        new ResourceBundleResolverImpl().getBundle(null, US, mock(ClassLoader.class));
    }

    @Test(expected = NullPointerException.class)
    public void nullLocaleShouldThrowNpeAsPerApiSpec() {
        new ResourceBundleResolverImpl().getBundle("", null, mock(ClassLoader.class));
    }

    @Test(expected = NullPointerException.class)
    public void nullClassLoaderShouldThrowNpeAsPerApiSpec() {
        new ResourceBundleResolverImpl().getBundle("", US, null);
    }
}
