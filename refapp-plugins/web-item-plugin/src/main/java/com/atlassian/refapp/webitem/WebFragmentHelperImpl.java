package com.atlassian.refapp.webitem;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.module.ContainerManagedPlugin;
import com.atlassian.plugin.web.Condition;
import com.atlassian.plugin.web.ContextProvider;
import com.atlassian.plugin.web.WebFragmentHelper;
import com.atlassian.plugin.web.conditions.ConditionLoadingException;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.templaterenderer.TemplateRenderer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import static java.util.Objects.requireNonNull;

public class WebFragmentHelperImpl implements WebFragmentHelper {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final I18nResolver defaultI18nResolver;
    private final TemplateRenderer renderer;

    public WebFragmentHelperImpl(I18nResolver i18nResolver, TemplateRenderer renderer) {
        this.defaultI18nResolver = requireNonNull(i18nResolver);
        this.renderer = requireNonNull(renderer);
    }

    public String getI18nValue(String key, List<?> arguments, Map<String, Object> context) {
        final I18nResolver i18nResolverForContext;
        if (context.containsKey("i18n")) {
            i18nResolverForContext = (I18nResolver) context.get("i18n");
        } else {
            logger.debug("context does not contain an I18nResolver as i18n, falling back to default");
            i18nResolverForContext = this.defaultI18nResolver;
        }
        Serializable[] params = new Serializable[0];
        if (arguments != null) {
            // Just hope it's Serializable...
            params = arguments.toArray(params);
        }
        return i18nResolverForContext.getText(key, params);
    }

    public Condition loadCondition(String className, Plugin plugin) throws ConditionLoadingException {
        try {
            if (plugin instanceof ContainerManagedPlugin) {
                Class conditionClass = plugin.loadClass(className, getClass());
                return (Condition) ((ContainerManagedPlugin) plugin).getContainerAccessor().createBean(conditionClass);
            } else {
                throw new ConditionLoadingException("Plugin " + plugin.getKey() + " is not a ContainerManagedPlugin, could not load condition.");
            }
        } catch (IllegalArgumentException e) {
            throw new ConditionLoadingException(e);
        } catch (ClassNotFoundException e) {
            throw new ConditionLoadingException(e);
        }
    }

    public ContextProvider loadContextProvider(String className, Plugin plugin) throws ConditionLoadingException {
        try {
            if (plugin instanceof ContainerManagedPlugin) {
                Class conditionClass = plugin.loadClass(className, getClass());
                return (ContextProvider) ((ContainerManagedPlugin) plugin).getContainerAccessor().createBean(conditionClass);
            } else {
                throw new ConditionLoadingException("Plugin " + plugin.getKey() + " is not a ContainerManagedPlugin, could not load context.");
            }
        } catch (IllegalArgumentException e) {
            throw new ConditionLoadingException(e);
        } catch (ClassNotFoundException e) {
            throw new ConditionLoadingException(e);
        }
    }

    public String renderVelocityFragment(String fragment, Map<String, Object> context) {
        return renderer.renderFragment(fragment, context);
    }
}
