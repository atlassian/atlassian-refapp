package com.atlassian.refapp.webitem;

import com.atlassian.plugins.osgi.javaconfig.moduletypes.PluginContextModuleDescriptorFactory;

public class RefAppWebSectionModuleDescriptorFactory
        extends PluginContextModuleDescriptorFactory<RefAppWebSectionModuleDescriptor> {
    public RefAppWebSectionModuleDescriptorFactory() {
        super("web-section", RefAppWebSectionModuleDescriptor.class);
    }
}
