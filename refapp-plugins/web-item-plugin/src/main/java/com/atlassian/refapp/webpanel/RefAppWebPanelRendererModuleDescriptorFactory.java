package com.atlassian.refapp.webpanel;

import com.atlassian.plugins.osgi.javaconfig.moduletypes.PluginContextModuleDescriptorFactory;

public class RefAppWebPanelRendererModuleDescriptorFactory
        extends PluginContextModuleDescriptorFactory<RefAppWebPanelRendererModuleDescriptor> {
    public RefAppWebPanelRendererModuleDescriptorFactory() {
        super("web-panel-renderer", RefAppWebPanelRendererModuleDescriptor.class);
    }
}
