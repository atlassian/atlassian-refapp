package com.atlassian.refapp.webitem.spring;

import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.hostcontainer.HostContainer;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.osgi.bridge.external.SpringHostContainer;
import com.atlassian.plugin.web.DefaultWebInterfaceManager;
import com.atlassian.plugin.web.WebFragmentHelper;
import com.atlassian.plugin.web.WebInterfaceManager;
import com.atlassian.plugin.web.api.DynamicWebInterfaceManager;
import com.atlassian.refapp.webitem.RefAppWebItemModuleDescriptorFactory;
import com.atlassian.refapp.webitem.RefAppWebSectionModuleDescriptorFactory;
import com.atlassian.refapp.webitem.WebFragmentHelperImpl;
import com.atlassian.refapp.webpanel.DefaultWebPanelModuleDescriptorFactory;
import com.atlassian.refapp.webpanel.RefAppWebPanelRendererModuleDescriptorFactory;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.templaterenderer.TemplateRenderer;
import org.osgi.framework.ServiceRegistration;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.atlassian.plugins.osgi.javaconfig.ExportOptions.as;
import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.exportAsModuleType;
import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.exportOsgiService;
import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.importOsgiService;

@Configuration
public class SpringBeans {

    @Bean
    public DynamicWebInterfaceManager dynamicWebInterfaceManager(
            final PluginAccessor pluginAccessor, final WebFragmentHelper webFragmentHelper
    ) {
        return new DefaultWebInterfaceManager(pluginAccessor, webFragmentHelper);
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportDefaultWebPanelModuleDescriptor(
            final DefaultWebPanelModuleDescriptorFactory defaultWebPanelModuleDescriptorFactory
    ) {
        return exportAsModuleType(defaultWebPanelModuleDescriptorFactory);
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportRefAppWebItemModuleDescriptor(
            final RefAppWebItemModuleDescriptorFactory refAppWebItemModuleDescriptorFactory
    ) {
        return exportAsModuleType(refAppWebItemModuleDescriptorFactory);
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportRefAppWebPanelModuleDescriptor(
            final RefAppWebPanelRendererModuleDescriptorFactory refAppWebPanelRendererModuleDescriptorFactory
    ) {
        return exportAsModuleType(refAppWebPanelRendererModuleDescriptorFactory);
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportRefAppWebSectionModuleDescriptor(
            final RefAppWebSectionModuleDescriptorFactory refAppWebSectionModuleDescriptorFactory
    ) {
        return exportAsModuleType(refAppWebSectionModuleDescriptorFactory);
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportWebFragmentHelper(
            final WebFragmentHelper webFragmentHelper
    ) {
        return exportOsgiService(webFragmentHelper, as(WebFragmentHelper.class));
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportDynamicWebInterfaceManager(
            final DynamicWebInterfaceManager dynamicWebInterfaceManager
    ) {
        return exportOsgiService(
                dynamicWebInterfaceManager,
                as(WebInterfaceManager.class, DynamicWebInterfaceManager.class)
        );
    }

    @Bean
    public HostContainer hostContainer() {
        // This host container satisfies constructor dependencies using beans in the plugin's Spring ApplicationContext
        return new SpringHostContainer();
    }

    @Bean
    public DefaultWebPanelModuleDescriptorFactory defaultWebPanelModuleDescriptorFactory() {
        return new DefaultWebPanelModuleDescriptorFactory();
    }

    @Bean
    public RefAppWebItemModuleDescriptorFactory refAppWebItemModuleDescriptorFactory() {
        return new RefAppWebItemModuleDescriptorFactory();
    }

    @Bean
    public RefAppWebPanelRendererModuleDescriptorFactory refAppWebPanelRendererModuleDescriptorFactory() {
        return new RefAppWebPanelRendererModuleDescriptorFactory();
    }

    @Bean
    public RefAppWebSectionModuleDescriptorFactory refAppWebSectionModuleDescriptorFactory() {
        return new RefAppWebSectionModuleDescriptorFactory();
    }

    @Bean
    public I18nResolver i18nResolver() {
        return importOsgiService(I18nResolver.class);
    }

    @Bean
    public ModuleFactory moduleFactory() {
        return importOsgiService(ModuleFactory.class);
    }

    @Bean
    public PluginAccessor pluginAccessor() {
        return importOsgiService(PluginAccessor.class);
    }

    @Bean
    public TemplateRenderer templateRenderer() {
        return importOsgiService(TemplateRenderer.class);
    }

    @Bean
    public WebFragmentHelper webFragmentHelper(I18nResolver i18nResolver, TemplateRenderer templateRenderer) {
        return new WebFragmentHelperImpl(i18nResolver, templateRenderer);
    }
}
