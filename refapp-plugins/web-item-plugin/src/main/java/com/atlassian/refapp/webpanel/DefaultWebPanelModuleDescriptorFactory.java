package com.atlassian.refapp.webpanel;

import com.atlassian.plugin.web.descriptors.DefaultWebPanelModuleDescriptor;
import com.atlassian.plugins.osgi.javaconfig.moduletypes.PluginContextModuleDescriptorFactory;

public class DefaultWebPanelModuleDescriptorFactory
        extends PluginContextModuleDescriptorFactory<DefaultWebPanelModuleDescriptor> {
    public DefaultWebPanelModuleDescriptorFactory() {
        super("web-panel", DefaultWebPanelModuleDescriptor.class);
    }
}
