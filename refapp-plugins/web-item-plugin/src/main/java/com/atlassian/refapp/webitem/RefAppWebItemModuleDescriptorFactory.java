package com.atlassian.refapp.webitem;

import com.atlassian.plugins.osgi.javaconfig.moduletypes.PluginContextModuleDescriptorFactory;

public class RefAppWebItemModuleDescriptorFactory
        extends PluginContextModuleDescriptorFactory<RefAppWebItemModuleDescriptor> {
    public RefAppWebItemModuleDescriptorFactory() {
        super("web-item", RefAppWebItemModuleDescriptor.class);
    }
}
