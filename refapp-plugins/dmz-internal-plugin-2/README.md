## dmz-internal-plugin-2 - Description

The purpose of this plugin is to ensure that internal plugins have access to
Atlassian internal packages via OSGi. Additionally, it will be used as showcase
how internal plugins will behave when accessing Atlassian internal packages via
OSGi.

Jackson 2.x components are exported by system bundle and added as Atlassian
internal packages (DMZ functionality).

This plugin is recognized by DMZ functionality as internal plugin, because it is
added to internal plugin list. Internal plugin list can be modified by the 
products. This plugin is added to internal plugin list from RefApp level in the
`com.atlassian.plugin.refimpl.plugins.PackageScannerConfigurationFactory` class.

DMZ logic can be found in atlassian-plugins repository.
