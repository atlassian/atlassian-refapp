package com.atlassian.refapp.audit.core.spi;

import com.atlassian.annotations.VisibleForTesting;
import com.atlassian.audit.core.spi.AuditMethods;
import com.atlassian.audit.core.spi.service.AuditMethodProvider;
import com.atlassian.sal.api.web.context.HttpContext;

import javax.annotation.Nonnull;
import javax.servlet.http.HttpServletRequest;

import static java.util.Objects.requireNonNull;

public class RefimplAuditMethodProvider implements AuditMethodProvider {

    @VisibleForTesting
    static final String HEADER_MOBILE_APP_REQUEST = "mobile-app-request";

    private final HttpContext httpContext;

    public RefimplAuditMethodProvider(@Nonnull final HttpContext httpContext) {
        requireNonNull(httpContext);
        this.httpContext = httpContext;
    }

    @Override
    @Nonnull
    public String currentMethod() {
        try {
            final HttpServletRequest request = httpContext.getRequest();

            if (request == null) {
                return AuditMethods.system();
            }

            if (isMobileAppRequest(request)) {
                return AuditMethods.mobile();
            }

            return AuditMethods.browser();
        } catch (Exception exception) {
            return AuditMethods.unknown();
        }
    }

    private boolean isMobileAppRequest(HttpServletRequest request) {
        return request.getHeader(HEADER_MOBILE_APP_REQUEST) != null;
    }

}
