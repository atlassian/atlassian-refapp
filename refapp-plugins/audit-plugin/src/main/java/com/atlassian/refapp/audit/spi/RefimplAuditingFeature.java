package com.atlassian.refapp.audit.spi;

import com.atlassian.audit.spi.feature.DatabaseAuditingFeature;
import com.atlassian.audit.spi.feature.DelegatedViewFeature;
import com.atlassian.audit.spi.feature.FileAuditingFeature;

public class RefimplAuditingFeature implements DatabaseAuditingFeature, DelegatedViewFeature, FileAuditingFeature {

    @Override
    public boolean isEnabled() {
        return true;
    }
}
