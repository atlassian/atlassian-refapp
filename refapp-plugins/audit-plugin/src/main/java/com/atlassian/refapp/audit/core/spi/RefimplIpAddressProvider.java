package com.atlassian.refapp.audit.core.spi;

import com.atlassian.annotations.VisibleForTesting;
import com.atlassian.audit.core.spi.service.IpAddressProvider;
import com.atlassian.sal.api.web.context.HttpContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

import static java.util.Objects.requireNonNull;

public class RefimplIpAddressProvider implements IpAddressProvider {
    private static final Logger log = LoggerFactory.getLogger(RefimplIpAddressProvider.class);
    @VisibleForTesting
    static final String X_FORWARDED_FOR = "X-Forwarded-For";
    private final HttpContext httpContext;

    public RefimplIpAddressProvider(@Nonnull final HttpContext httpContext) {
        requireNonNull(httpContext);
        this.httpContext = httpContext;
    }

    @Override
    @Nullable
    public String currentIpAddress() {
        try {
            return Optional.ofNullable(httpContext.getRequest())
                    .map(HttpServletRequest::getRemoteAddr)
                    .orElse(null);
        } catch (Exception exception) {
            log.debug("Unable to determine current IP address", exception);
            return null;
        }
    }

    @Nullable
    @Override
    public String remoteIpAddress() {
        return Optional.ofNullable(httpContext.getRequest())
                .map(httpRequest -> httpRequest.getHeader(X_FORWARDED_FOR))
                .orElseGet(() -> Optional.ofNullable(httpContext.getRequest())
                        .map(HttpServletRequest::getRemoteAddr)
                        .orElse(null));
    }

    @Nonnull
    @Override
    public Optional<String> forwarderIpAddress() {
        return Optional.ofNullable(httpContext.getRequest())
                .filter(req -> req.getHeader(X_FORWARDED_FOR) != null)
                .map(HttpServletRequest::getRemoteAddr);
    }
}
