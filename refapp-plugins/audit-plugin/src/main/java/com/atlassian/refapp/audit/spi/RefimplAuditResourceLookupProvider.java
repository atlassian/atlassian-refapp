package com.atlassian.refapp.audit.spi;

import com.atlassian.annotations.VisibleForTesting;
import com.atlassian.audit.api.util.pagination.Page;
import com.atlassian.audit.api.util.pagination.PageRequest;
import com.atlassian.audit.entity.AuditAuthor;
import com.atlassian.audit.entity.AuditResource;
import com.atlassian.audit.spi.lookup.AuditingResourcesLookupService;
import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;
import java.util.StringTokenizer;
import java.util.stream.StreamSupport;

import static java.util.Collections.emptyList;
import static java.util.Collections.list;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;

public class RefimplAuditResourceLookupProvider implements AuditingResourcesLookupService {

    private static final Logger log = LoggerFactory.getLogger(RefimplAuditResourceLookupProvider.class);

    private static final String DELIMS = ". ";
    private final CrowdService crowdService;

    public RefimplAuditResourceLookupProvider(@Nonnull final CrowdService crowdService) {
        requireNonNull(crowdService);
        this.crowdService = crowdService;
    }

    @Override
    public Page<AuditAuthor, String> lookupAuditAuthor(
            @Nullable String searchText,
            @Nonnull PageRequest<String> pageRequest) {
        return getPaginatedUsers(searchText, pageRequest);
    }

    @Override
    public Page<AuditResource, String> lookupAuditResource(
            @Nonnull String resourceType,
            @Nullable String searchText,
            @Nonnull PageRequest<String> pageRequest
    ) {
        return Page.emptyPage();
    }

    private Page<AuditAuthor, String> getPaginatedUsers(final String search, final PageRequest<String> pageRequest) {
        return new Page.Builder<AuditAuthor, String>(getUsers(search, pageRequest), true).build();
    }

    private List<AuditAuthor> getUsers(final String search, final PageRequest<String> pageRequest) {
        try {
            EntityQuery<User> userEntityQuery = QueryBuilder
                    .queryFor(User.class, EntityDescriptor.user())
                    .returningAtMost(Integer.MAX_VALUE);
            return StreamSupport.stream(crowdService.search(userEntityQuery)
                            .spliterator(), false)
                    .filter(user -> authorMatches(search, user))
                    .map(this::toAuditAuthor)
                    .skip(pageRequest.getOffset())
                    .limit(pageRequest.getLimit())
                    .collect(toList());
        } catch (final Exception exception) {
            log.error("Failed to get users from the crowd service", exception);
        }
        return emptyList();
    }

    private AuditAuthor toAuditAuthor(final User user) {
        return AuditAuthor.builder()
                .id(user.getName())
                .name(user.getName())
                .type("user")
                .build();
    }

    @VisibleForTesting
    boolean authorMatches(final String search, final User user) {
        if (search == null || search.trim().isEmpty()) {
            return true;
        }

        final String searchStr = search.trim().toLowerCase();

        final String compositeUserNameAndFullName =
                emptyLeftIffNull(user.getName(), user.getName() + " ")
                        + emptyLeftIffNull(user.getDisplayName(), user.getDisplayName());

        final StringTokenizer usernameTokenizer = new StringTokenizer(compositeUserNameAndFullName.toLowerCase(), DELIMS);

        return list(usernameTokenizer)
                .stream()
                .map(str -> (String) str)
                .anyMatch(str -> str.startsWith(searchStr));
    }

    @Nonnull
    private String emptyLeftIffNull(@Nullable final String left, final String right) {
        return left == null ? "" : right;
    }
}
