package com.atlassian.refapp.audit.spi;

import com.atlassian.audit.spi.permission.ResourceContextPermissionChecker;

import javax.annotation.Nonnull;

import static java.util.Objects.requireNonNull;

public class RefimplResourceContextPermissionChecker implements ResourceContextPermissionChecker {
    @Override
    public boolean hasResourceAuditViewPermission(@Nonnull final String resourceType, @Nonnull final String resourceId) {
        // Even though this will always return true, we want to be able to catch bugs in Atlassian Audit when developing
        // it against RefApp.
        requireNonNull(resourceType);
        requireNonNull(resourceId);
        return true;
    }
}
