package com.atlassian.refapp.audit;

import com.atlassian.audit.api.AuditService;
import com.atlassian.audit.core.AuditServiceFactory;
import com.atlassian.audit.core.spi.service.AuditMethodProvider;
import com.atlassian.audit.core.spi.service.BaseUrlProvider;
import com.atlassian.audit.core.spi.service.ClusterNodeProvider;
import com.atlassian.audit.core.spi.service.CurrentUserProvider;
import com.atlassian.audit.core.spi.service.IpAddressProvider;
import com.atlassian.audit.spi.entity.AuditEntityTransformationService;
import com.atlassian.audit.spi.feature.DatabaseAuditingFeature;
import com.atlassian.audit.spi.feature.DelegatedViewFeature;
import com.atlassian.audit.spi.feature.FileAuditingFeature;
import com.atlassian.audit.spi.lookup.AuditingResourcesLookupService;
import com.atlassian.audit.spi.migration.LegacyAuditEntityMigrator;
import com.atlassian.audit.spi.migration.LegacyRetentionConfigProvider;
import com.atlassian.audit.spi.permission.ResourceContextPermissionChecker;
import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.diagnostics.util.CallingBundleResolver;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.metadata.PluginMetadataManager;
import com.atlassian.refapp.audit.core.spi.RefimplAuditMethodProvider;
import com.atlassian.refapp.audit.core.spi.RefimplBaseUrlProvider;
import com.atlassian.refapp.audit.core.spi.RefimplClusterNodeProvider;
import com.atlassian.refapp.audit.core.spi.RefimplCurrentUserProvider;
import com.atlassian.refapp.audit.core.spi.RefimplIpAddressProvider;
import com.atlassian.refapp.audit.spi.RefimplAuditEntityTransformationService;
import com.atlassian.refapp.audit.spi.RefimplAuditResourceLookupProvider;
import com.atlassian.refapp.audit.spi.RefimplAuditingFeature;
import com.atlassian.refapp.audit.spi.RefimplLegacyAuditEntityMigrator;
import com.atlassian.refapp.audit.spi.RefimplLegacyRetentionConfigProvider;
import com.atlassian.refapp.audit.spi.RefimplResourceContextPermissionChecker;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.web.context.HttpContext;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.atlassian.plugins.osgi.javaconfig.ExportOptions.as;
import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.exportOsgiService;
import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.importOsgiService;

@Configuration
public class SpringConfig {
    @Bean
    public EventPublisher eventPublisher() {
        return importOsgiService(EventPublisher.class);
    }

    @Bean
    public PluginAccessor pluginAccessor() {
        return importOsgiService(PluginAccessor.class);
    }

    @Bean
    public PluginMetadataManager pluginMetadataManager() {
        return importOsgiService(PluginMetadataManager.class);
    }

    @Bean
    public HttpContext httpContext() {
        return importOsgiService(HttpContext.class);
    }

    @Bean
    public ApplicationProperties applicationProperties() {
        return importOsgiService(ApplicationProperties.class);
    }

    @Bean
    public UserManager userManager() {
        return importOsgiService(UserManager.class);
    }

    @Bean
    public RefimplAuditMethodProvider refImplAuditMethodProvider(final HttpContext httpContext) {
        return new RefimplAuditMethodProvider(httpContext);
    }

    @Bean
    public FactoryBean<ServiceRegistration> auditMethodProvider(final RefimplAuditMethodProvider refImplAuditMethodProvider) {
        return exportOsgiService(refImplAuditMethodProvider,
                as(AuditMethodProvider.class));
    }

    @Bean
    public RefimplBaseUrlProvider refImplBaseUrlProvider(final ApplicationProperties applicationProperties) {
        return new RefimplBaseUrlProvider(applicationProperties);
    }

    @Bean
    public FactoryBean<ServiceRegistration> baseUrlProvider(final RefimplBaseUrlProvider refImplBaseUrlProvider) {
        return exportOsgiService(refImplBaseUrlProvider,
                as(BaseUrlProvider.class));
    }

    @Bean
    public RefimplClusterNodeProvider refImplClusterNodeProvider() {
        return new RefimplClusterNodeProvider();
    }

    @Bean
    public FactoryBean<ServiceRegistration> clusterNodeProvider(final RefimplClusterNodeProvider refImplClusterNodeProvider) {
        return exportOsgiService(refImplClusterNodeProvider,
                as(ClusterNodeProvider.class));
    }

    @Bean
    public RefimplCurrentUserProvider refImplCurrentUserProvider(final HttpContext httpContext, final UserManager userManager) {
        return new RefimplCurrentUserProvider(httpContext, userManager);
    }

    @Bean
    public FactoryBean<ServiceRegistration> currentUserProvider(final RefimplCurrentUserProvider refImplCurrentUserProvider) {
        return exportOsgiService(refImplCurrentUserProvider,
                as(CurrentUserProvider.class));
    }

    @Bean
    public RefimplIpAddressProvider refImplIpAddressProvider(final HttpContext httpContext) {
        return new RefimplIpAddressProvider(httpContext);
    }

    @Bean
    public FactoryBean<ServiceRegistration> ipAddressProvider(final RefimplIpAddressProvider refImplIpAddressProvider) {
        return exportOsgiService(refImplIpAddressProvider,
                as(IpAddressProvider.class));
    }

    @Bean
    public RefimplAuditEntityTransformationService refImplAuditEntityTransformationService(final ApplicationProperties applicationProperties) {
        return new RefimplAuditEntityTransformationService(applicationProperties);
    }

    @Bean
    public FactoryBean<ServiceRegistration> auditEntityTransformationService(final RefimplAuditEntityTransformationService refImplAuditEntityTransformationService) {
        return exportOsgiService(refImplAuditEntityTransformationService,
                as(AuditEntityTransformationService.class));
    }

    @Bean
    public RefimplAuditingFeature refImplAuditingFeature() {
        return new RefimplAuditingFeature();
    }

    @Bean
    public FactoryBean<ServiceRegistration> auditingFeature(final RefimplAuditingFeature refImplAuditingFeature) {
        return exportOsgiService(refImplAuditingFeature,
                as(DatabaseAuditingFeature.class, DelegatedViewFeature.class, FileAuditingFeature.class));
    }

    @Bean
    public RefimplAuditResourceLookupProvider refimplAuditResourceLookupProvider(final CrowdService crowdService) {
        return new RefimplAuditResourceLookupProvider(crowdService);
    }

    @Bean
    public FactoryBean<ServiceRegistration> auditingResourcesLookupService(final RefimplAuditResourceLookupProvider refimplAuditResourceLookupProvider) {
        return exportOsgiService(refimplAuditResourceLookupProvider,
                as(AuditingResourcesLookupService.class));
    }

    @Bean
    public RefimplLegacyAuditEntityMigrator refImplLegacyAuditEntityMigrator() {
        return new RefimplLegacyAuditEntityMigrator();
    }

    @Bean
    public FactoryBean<ServiceRegistration> legacyAuditEntityMigrator(final RefimplLegacyAuditEntityMigrator refImplLegacyAuditEntityMigrator) {
        return exportOsgiService(refImplLegacyAuditEntityMigrator,
                as(LegacyAuditEntityMigrator.class));
    }

    @Bean
    public RefimplLegacyRetentionConfigProvider refImplLegacyRetentionConfigProvider() {
        return new RefimplLegacyRetentionConfigProvider();
    }

    @Bean
    public FactoryBean<ServiceRegistration> legacyRetentionConfigProvider(final RefimplLegacyRetentionConfigProvider refImplLegacyRetentionConfigProvider) {
        return exportOsgiService(refImplLegacyRetentionConfigProvider,
                as(LegacyRetentionConfigProvider.class));
    }

    @Bean
    public RefimplResourceContextPermissionChecker refImplResourceContextPermissionChecker() {
        return new RefimplResourceContextPermissionChecker();
    }

    @Bean
    public FactoryBean<ServiceRegistration> resourceContextPermissionChecker(final RefimplResourceContextPermissionChecker refImplResourceContextPermissionChecker) {
        return exportOsgiService(refImplResourceContextPermissionChecker,
                as(ResourceContextPermissionChecker.class));
    }

    @Bean
    public AuditService refImplAuditService(
            final BundleContext bundleContext,
            final EventPublisher eventPublisher,
            final PluginAccessor pluginAccessor,
            final PluginMetadataManager pluginMetadataManager,
            final RefimplCurrentUserProvider refimplCurrentUserProvider,
            final RefimplIpAddressProvider refimplIpAddressProvider,
            final RefimplAuditMethodProvider refimplAuditMethodProvider,
            final RefimplBaseUrlProvider refimplBaseUrlProvider,
            final RefimplClusterNodeProvider refimplClusterNodeProvider
    ) {
        return new AuditServiceFactory(
                () -> bundleContext,
                eventPublisher,
                new CallingBundleResolver(),
                pluginAccessor,
                pluginMetadataManager,
                refimplCurrentUserProvider,
                refimplIpAddressProvider,
                refimplAuditMethodProvider,
                refimplBaseUrlProvider,
                refimplClusterNodeProvider
        ).create();
    }

    @Bean
    public FactoryBean<ServiceRegistration> auditService(@Qualifier("refImplAuditService") final AuditService refImplAuditService) {
        return exportOsgiService(refImplAuditService,
                as(AuditService.class));
    }

    @Bean
    public CrowdService crowdService() {
        return importOsgiService(CrowdService.class);
    }
}
