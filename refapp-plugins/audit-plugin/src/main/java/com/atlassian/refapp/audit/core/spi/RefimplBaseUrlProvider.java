package com.atlassian.refapp.audit.core.spi;

import com.atlassian.audit.core.spi.service.BaseUrlProvider;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.UrlMode;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static java.util.Objects.requireNonNull;

public class RefimplBaseUrlProvider implements BaseUrlProvider {

    private final ApplicationProperties applicationProperties;

    public RefimplBaseUrlProvider(@Nonnull final ApplicationProperties applicationProperties) {
        requireNonNull(applicationProperties);
        this.applicationProperties = applicationProperties;
    }

    @Nullable
    @Override
    public String currentBaseUrl() {
        return applicationProperties.getBaseUrl(UrlMode.ABSOLUTE);
    }
}
