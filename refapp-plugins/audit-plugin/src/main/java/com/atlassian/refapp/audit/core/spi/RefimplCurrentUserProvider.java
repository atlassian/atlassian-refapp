package com.atlassian.refapp.audit.core.spi;

import com.atlassian.audit.core.spi.service.CurrentUserProvider;
import com.atlassian.audit.entity.AuditAuthor;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.user.UserProfile;
import com.atlassian.sal.api.web.context.HttpContext;

import javax.annotation.Nonnull;
import javax.servlet.http.HttpServletRequest;

import static com.atlassian.audit.entity.AuditAuthor.ANONYMOUS_AUTHOR;
import static com.atlassian.audit.entity.AuditAuthor.SYSTEM_AUTHOR;
import static java.util.Objects.requireNonNull;

public class RefimplCurrentUserProvider implements CurrentUserProvider {

    private final HttpContext httpContext;
    private final UserManager userManager;

    public RefimplCurrentUserProvider(@Nonnull final HttpContext httpContext, @Nonnull final UserManager userManager) {
        requireNonNull(httpContext);
        requireNonNull(userManager);
        this.httpContext = httpContext;
        this.userManager = userManager;
    }

    @Nonnull
    @Override
    public AuditAuthor currentUser() {
        try {
            final UserProfile currentUser = userManager.getRemoteUser();
            final HttpServletRequest request = httpContext.getRequest();
            if (request == null) {
                return SYSTEM_AUTHOR;
            }

            if (currentUser == null) {
                return ANONYMOUS_AUTHOR;
            }

            return AuditAuthor
                    .builder()
                    .id(currentUser.getUserKey().getStringValue())
                    .name(currentUser.getUsername())
                    .type("user")
                    .build();
        } catch (IllegalStateException ise) {
            return SYSTEM_AUTHOR;
        }
    }
}
