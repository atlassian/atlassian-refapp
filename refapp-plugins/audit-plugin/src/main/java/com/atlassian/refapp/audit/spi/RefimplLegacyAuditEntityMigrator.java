package com.atlassian.refapp.audit.spi;

import com.atlassian.audit.api.AuditConsumer;
import com.atlassian.audit.spi.migration.LegacyAuditEntityMigrator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;

import static java.util.Objects.requireNonNull;

public class RefimplLegacyAuditEntityMigrator implements LegacyAuditEntityMigrator {
    private static final Logger log = LoggerFactory.getLogger(RefimplLegacyAuditEntityMigrator.class);

    @Override
    public void migrate(@Nonnull final AuditConsumer consumer) {
        // Even though this will do nothing, we want to be able to catch bugs in Atlassian Audit when developing
        // against RefApp.
        requireNonNull(consumer);
        log.info("RefApp doesn't have a legacy auditing feature, but if it did, it be would migrating the old audit" +
                " events now.");
    }
}
