package com.atlassian.refapp.audit.core.spi;

import com.atlassian.audit.core.spi.service.ClusterNodeProvider;

import javax.annotation.Nonnull;
import java.util.Optional;

public class RefimplClusterNodeProvider implements ClusterNodeProvider {
    @Nonnull
    @Override
    public Optional<String> currentNodeId() {
        return Optional.empty();
    }
}
