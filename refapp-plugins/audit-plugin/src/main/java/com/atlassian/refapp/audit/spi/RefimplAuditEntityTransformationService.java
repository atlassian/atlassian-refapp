package com.atlassian.refapp.audit.spi;

import com.atlassian.annotations.VisibleForTesting;
import com.atlassian.audit.entity.AuditAuthor;
import com.atlassian.audit.entity.AuditEntity;
import com.atlassian.audit.spi.entity.AuditEntityTransformationService;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.UrlMode;

import javax.annotation.Nonnull;
import java.util.List;

import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;

public class RefimplAuditEntityTransformationService implements AuditEntityTransformationService {

    private static final String USER_URL_PATH = "plugins/servlet/users/";
    private final ApplicationProperties applicationProperties;

    public RefimplAuditEntityTransformationService(@Nonnull final ApplicationProperties applicationProperties) {
        requireNonNull(applicationProperties);
        this.applicationProperties = applicationProperties;
    }

    @Nonnull
    @Override
    public List<AuditEntity> transform(@Nonnull List<AuditEntity> list) {
        return list.stream()
                .map(entity -> new AuditEntity.Builder(entity)
                        .author(new AuditAuthor
                                .Builder(entity
                                .getAuthor())
                                .uri(constructAuthorUrl(entity))
                                .build())
                        .build())
                .collect(toList());
    }

    @VisibleForTesting
    String constructAuthorUrl(final AuditEntity entity) {
        final String baseUrl = applicationProperties.getBaseUrl(UrlMode.ABSOLUTE);
        return baseUrl + (baseUrl.endsWith("/") ? "" : "/") + USER_URL_PATH + entity.getAuthor().getId();
    }
}
