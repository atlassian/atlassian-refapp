package com.atlassian.refapp.audit.spi;

import com.atlassian.audit.entity.AuditAuthor;
import com.atlassian.audit.entity.AuditEntity;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.UrlMode;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class RefimplAuditEntityTransformationServiceTest {
    @Rule
    public final MethodRule mockitoRule = MockitoJUnit.rule();

    @Mock
    ApplicationProperties applicationProperties;

    @InjectMocks
    RefimplAuditEntityTransformationService underTest;

    @Test
    public void testConstructAuthorUrl_shouldMapToUsersView() {
        when(applicationProperties.getBaseUrl(UrlMode.ABSOLUTE)).thenReturn("baseUrl");

        final String userId = "userId";
        final AuditEntity auditEntity = mock(AuditEntity.class);
        when(auditEntity.getAuthor()).thenReturn(mock(AuditAuthor.class));
        when(auditEntity.getAuthor().getId()).thenReturn(userId);

        assertEquals("baseUrl/plugins/servlet/users/" + userId, underTest.constructAuthorUrl(auditEntity));
    }
}
