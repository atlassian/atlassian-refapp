package com.atlassian.refapp.audit.core.spi;

import com.atlassian.audit.entity.AuditAuthor;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.user.UserProfile;
import com.atlassian.sal.api.web.context.HttpContext;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import javax.servlet.http.HttpServletRequest;

import static com.atlassian.audit.entity.AuditAuthor.ANONYMOUS_AUTHOR;
import static com.atlassian.audit.entity.AuditAuthor.SYSTEM_AUTHOR;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class RefimplCurrentUserProviderTest {
    @Rule
    public final MethodRule mockitoRule = MockitoJUnit.rule();

    @Mock
    HttpContext httpContext;

    @Mock
    UserManager userManager;

    @InjectMocks
    RefimplCurrentUserProvider underTest;

    @Test
    public void givenOutsideOfARequest_thenAttributeTheSystem() {
        when(httpContext.getRequest()).thenReturn(null);

        assertEquals(SYSTEM_AUTHOR, underTest.currentUser());
    }

    @Test
    public void givenInsideOfARequest_whenTheUserCannotBeFound_thenAttributeAnonymous() {
        when(httpContext.getRequest()).thenReturn(mock(HttpServletRequest.class));
        when(userManager.getRemoteUser()).thenReturn(null);

        assertEquals(ANONYMOUS_AUTHOR, underTest.currentUser());
    }

    @Test
    public void givenInsideOfARequest_whenTheUserIsBeFound_thenAttributeThatUser() {
        final String USER_KEY = "userKey";
        final String USER_NAME = "userName";
        final UserProfile userProfile = mock(UserProfile.class);
        when(userProfile.getUserKey()).thenReturn(new UserKey(USER_KEY));
        when(userProfile.getUsername()).thenReturn(USER_NAME);
        when(userManager.getRemoteUser()).thenReturn(userProfile);

        when(httpContext.getRequest()).thenReturn(mock(HttpServletRequest.class));

        final AuditAuthor expectedAuthor = AuditAuthor.builder().id(USER_KEY).name(USER_NAME).type("user").build();

        assertEquals(expectedAuthor, underTest.currentUser());
    }

    @Test
    public void givenOutsideOfARequest_whenUnableToGetTheRequest_thenAttributeTheSystem() {
        when(httpContext.getRequest()).thenThrow(IllegalStateException.class);

        assertEquals(SYSTEM_AUTHOR, underTest.currentUser());
    }

    @Test
    public void testGenericExceptionsPropagate() {
        when(httpContext.getRequest()).thenThrow(RuntimeException.class);

        assertThrows(RuntimeException.class, () -> underTest.currentUser());
    }
}
