package com.atlassian.refapp.audit.core.spi;

import com.atlassian.sal.api.web.context.HttpContext;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;
import java.util.Optional;
import java.util.UUID;

import static com.atlassian.refapp.audit.core.spi.RefimplIpAddressProvider.X_FORWARDED_FOR;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class RefimplIpAddressProviderTest {
    @Rule
    public final MethodRule mockitoRule = MockitoJUnit.rule();

    @Mock
    HttpContext httpContext;

    @InjectMocks
    RefimplIpAddressProvider underTest;

    @Test
    public void currentIpAddress_shouldReturnTheRemoteAddress() {
        final String ip = UUID.randomUUID().toString();
        final HttpServletRequest request = mock(HttpServletRequest.class);
        when(httpContext.getRequest()).thenReturn(request);
        when(request.getRemoteAddr()).thenReturn(ip);

        assertEquals(ip, underTest.currentIpAddress());
        verify(request).getRemoteAddr();
    }

    @Test
    public void currentIpAddress_shouldNotPropagateExceptions() {
        when(httpContext.getRequest()).thenThrow(RuntimeException.class);

        underTest.currentIpAddress();
    }

    @Test
    public void remoteIpAddress_whenRequestNull_shouldReturnNull() {
        // Act
        final String remoteIpAddress = underTest.remoteIpAddress();

        // Assert
        assertThat(remoteIpAddress, nullValue());
    }

    @Test
    public void remoteIpAddress_whenRequestHasNoForwardedForHeaderOrRemoteAddress_shouldReturnNull() {
        assertRemoteIpAddress(null, null, null);
    }

    @Test
    public void remoteIpAddress_whenRequestHasForwardedForHeaderButNoRemoteAddress_shouldReturnItsValue() {
        final String forwardedFor = "theForwardedFor";
        assertRemoteIpAddress(forwardedFor, forwardedFor);
    }

    @Test
    public void remoteIpAddress_whenRequestHasNoForwardedForHeaderButRemoteAddress_shouldReturnItsValue() {
        final String remoteAddress = "theRemoteAddress";
        assertRemoteIpAddress(null, remoteAddress, remoteAddress);
    }

    @Test
    public void remoteIpAddress_whenRequestHasForwardedForHeaderAndRemoteAddress_shouldReturnForwardedFor() {
        final String forwardedFor = "theForwardedFor";
        final String remoteAddress = "theRemoteAddress";
        assertRemoteIpAddress(forwardedFor, forwardedFor);
    }

    @Test
    public void forwarderIpAddress_whenRequestNull_shouldReturnEmpty() {
        assertThat(underTest.forwarderIpAddress(), is(Optional.empty()));
    }

    @Test
    public void forwarderIpAddress_whenRequestHasNoForwardedForHeaderOrRemoteAddress_shouldReturnEmpty() {
        assertForwarderIpAddress(null, null, Optional.empty());
    }

    @Test
    public void forwarderIpAddress_whenRequestHasForwardedForHeaderButNoRemoteAddress_shouldReturnEmpty() {
        final String forwardedFor = "theForwardedFor";
        assertForwarderIpAddress(forwardedFor, null, Optional.empty());
    }

    @Test
    public void forwarderIpAddress_whenRequestHasNoForwardedForHeaderButRemoteAddress_shouldReturnEmpty() {
        final String remoteAddress = "theRemoteAddress";
        assertForwarderIpAddress(null, remoteAddress, Optional.empty());
    }

    @Test
    public void forwarderIpAddress_whenRequestHasForwardedForHeaderAndRemoteAddress_shouldReturnRemoteAddress() {
        final String forwardedFor = "theForwardedFor";
        final String remoteAddress = "theRemoteAddress";
        assertForwarderIpAddress(forwardedFor, remoteAddress, Optional.of(remoteAddress));
    }

    private void assertForwarderIpAddress(
            @Nullable final String forwardedFor,
            @Nullable final Optional<String> expectedForwarderIpAddress
    ) {
        // Arrange
        mockForwardedFor(forwardedFor);

        // Act
        final Optional<String> forwarderIpAddress = underTest.forwarderIpAddress();

        // Assert
        assertThat(forwarderIpAddress, is(expectedForwarderIpAddress));
    }

    private void assertForwarderIpAddress(
            @Nullable final String forwardedFor,
            @Nullable final String remoteAddr,
            @Nullable final Optional<String> expectedForwarderIpAddress
    ) {
        // Arrange
        final HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getHeader(X_FORWARDED_FOR)).thenReturn(forwardedFor);
        when(request.getRemoteAddr()).thenReturn(remoteAddr);
        when(httpContext.getRequest()).thenReturn(request);

        // Act
        final Optional<String> forwarderIpAddress = underTest.forwarderIpAddress();

        // Assert
        assertThat(forwarderIpAddress, is(expectedForwarderIpAddress));
    }

    private void assertRemoteIpAddress(
            @Nullable final String forwardedFor,
            @Nullable final String remoteAddr,
            @Nullable final String expectedRemoteIpAddress
    ) {
        // Arrange
        final HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getHeader(X_FORWARDED_FOR)).thenReturn(forwardedFor);
        when(httpContext.getRequest()).thenReturn(request);
        when(request.getRemoteAddr()).thenReturn(remoteAddr);

        // Act
        final String remoteIpAddress = underTest.remoteIpAddress();

        // Assert
        assertThat(remoteIpAddress, is(expectedRemoteIpAddress));
    }

    private void assertRemoteIpAddress(
            @Nullable final String forwardedFor,
            @Nullable final String expectedRemoteIpAddress
    ) {
        // Arrange
        mockForwardedFor(forwardedFor);

        // Act
        final String remoteIpAddress = underTest.remoteIpAddress();

        // Assert
        assertThat(remoteIpAddress, is(expectedRemoteIpAddress));
    }

    private void mockForwardedFor(@Nullable final String forwardedFor) {
        final HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getHeader(X_FORWARDED_FOR)).thenReturn(forwardedFor);
        when(httpContext.getRequest()).thenReturn(request);
    }
}
