package com.atlassian.refapp.audit.core.spi;

import com.atlassian.audit.core.spi.AuditMethods;
import com.atlassian.sal.api.web.context.HttpContext;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import javax.servlet.http.HttpServletRequest;

import static com.atlassian.refapp.audit.core.spi.RefimplAuditMethodProvider.HEADER_MOBILE_APP_REQUEST;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class RefimplAuditMethodProviderTest {
    @Rule
    public final MethodRule mockitoRule = MockitoJUnit.rule();

    @Mock
    HttpContext httpRequest;

    @InjectMocks
    RefimplAuditMethodProvider underTest;

    @Test
    public void testAuditMethodFallsbackToSystem() {
        when(httpRequest.getRequest()).thenReturn(null);

        assertEquals(AuditMethods.system(), underTest.currentMethod());
    }

    @Test
    public void testMobileTakesPrecedenceOverOtherMethods() {
        final HttpServletRequest mockRequest = mock(HttpServletRequest.class);
        when(mockRequest.getHeader(HEADER_MOBILE_APP_REQUEST)).thenReturn("notNull");
        when(httpRequest.getRequest()).thenReturn(mockRequest);


        assertEquals(AuditMethods.mobile(), underTest.currentMethod());
    }

    @Test
    public void testDefaultWithARequestIsBrowser() {
        final HttpServletRequest mockRequest = mock(HttpServletRequest.class);
        when(httpRequest.getRequest()).thenReturn(mockRequest);

        assertEquals(AuditMethods.browser(), underTest.currentMethod());
    }

    @Test
    public void testThrownExceptionIsMarkedAsUknownMethod() {
        when(httpRequest.getRequest()).thenThrow(IllegalStateException.class);

        assertEquals(AuditMethods.unknown(), underTest.currentMethod());
    }
}
