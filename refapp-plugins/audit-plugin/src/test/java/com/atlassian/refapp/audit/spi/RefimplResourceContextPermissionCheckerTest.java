package com.atlassian.refapp.audit.spi;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnit;

import static java.util.UUID.randomUUID;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

public class RefimplResourceContextPermissionCheckerTest {
    @Rule
    public final MethodRule mockitoRule = MockitoJUnit.rule();

    @InjectMocks
    RefimplResourceContextPermissionChecker underTest;

    @Test
    public void testHasResourceAuditViewPermission_shouldNotAcceptNullValues() {
        assertThrows(NullPointerException.class, () -> underTest.hasResourceAuditViewPermission(null, ""));
        assertThrows(NullPointerException.class, () -> underTest.hasResourceAuditViewPermission("", null));
    }

    @Test
    public void testHasResourceAuditViewPermission_shouldAlwaysBeTrue() {
        final String resourceType = randomUUID().toString();
        final String resourceId = randomUUID().toString();

        assertTrue(underTest.hasResourceAuditViewPermission(resourceType, resourceId));
    }
}
