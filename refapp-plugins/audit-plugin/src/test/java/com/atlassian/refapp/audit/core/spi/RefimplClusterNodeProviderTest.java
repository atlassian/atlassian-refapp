package com.atlassian.refapp.audit.core.spi;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnit;

import java.util.Optional;

import static org.junit.Assert.assertEquals;

public class RefimplClusterNodeProviderTest {
    @Rule
    public final MethodRule mockitoRule = MockitoJUnit.rule();

    @InjectMocks
    RefimplClusterNodeProvider underTest;

    @Test
    public void testShouldReturnEmptyOptionalToSignifySingleNode() {
        assertEquals(Optional.empty(), underTest.currentNodeId());
    }
}
