package com.atlassian.refapp.audit.spi;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class RefimplAuditingFeatureTest {
    @Test
    public void testFeatures_shouldAlwaysBeEnabled() {
        assertTrue(new RefimplAuditingFeature().isEnabled());
    }
}
