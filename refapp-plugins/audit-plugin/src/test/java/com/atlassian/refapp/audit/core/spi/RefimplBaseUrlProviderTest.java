package com.atlassian.refapp.audit.core.spi;

import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.UrlMode;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import static org.mockito.Mockito.verify;

public class RefimplBaseUrlProviderTest {
    @Rule
    public final MethodRule mockitoRule = MockitoJUnit.rule();

    @Mock
    ApplicationProperties applicationProperties;

    @InjectMocks
    RefimplBaseUrlProvider underTest;

    @Test
    public void testCurrentBaseUrlUsesTheCorrectUrlMode() {
        underTest.currentBaseUrl();
        verify(applicationProperties).getBaseUrl(UrlMode.ABSOLUTE);
    }
}
