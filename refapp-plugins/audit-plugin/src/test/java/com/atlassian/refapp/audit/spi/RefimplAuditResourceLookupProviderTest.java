package com.atlassian.refapp.audit.spi;

import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.crowd.embedded.api.User;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class RefimplAuditResourceLookupProviderTest {
    @Rule
    public final MethodRule mockitoRule = MockitoJUnit.rule();

    @Mock
    CrowdService crowdService;

    @InjectMocks
    RefimplAuditResourceLookupProvider underTest;

    /**
     * If there's nothing specified for the author, then every author should match (this is the default case)
     */
    @Test
    public void whenBlankSearch_thenTrue() {
        final User user = mock(User.class);

        assertTrue(underTest.authorMatches(null, user));
        assertTrue(underTest.authorMatches("", user));
        assertTrue(underTest.authorMatches(" ", user));
    }

    // username

    @Test
    public void whenNothingMatches_thenFalse() {
        final User user = mock(User.class);

        assertFalse(underTest.authorMatches("someName", user));
    }

    @Test
    public void givenMatchingCases_whenNameMatches_thenTrue() {
        final User user = mock(User.class);
        when(user.getName()).thenReturn("name");

        assertTrue(underTest.authorMatches("name", user));
    }

    @Test
    public void givenMisMatchingCases_whenNameMatches_thenTrue() {
        final User user = mock(User.class);
        when(user.getName()).thenReturn("NaMe");

        assertTrue(underTest.authorMatches("nAmE", user));
    }

    @Test
    public void givenLeadingSpaces_whenNameMatches_thenTrue() {
        final User user = mock(User.class);
        when(user.getName()).thenReturn(" NaMe");

        assertTrue(underTest.authorMatches(" nAmE", user));
    }

    @Test
    public void givenTrailingSpaces_whenNameMatches_thenTrue() {
        final User user = mock(User.class);
        when(user.getName()).thenReturn("NaMe ");

        assertTrue(underTest.authorMatches("nAmE   ", user));
    }

    @Test
    public void givenPartialUserName_whenNameMatches_thenTrue() {
        final User user = mock(User.class);
        when(user.getName()).thenReturn("user.name");

        assertTrue(underTest.authorMatches("name", user));
    }

    // Full name

    @Test
    public void givenMatchingCases_whenFullNameMatches_thenTrue() {
        final User user = mock(User.class);
        when(user.getDisplayName()).thenReturn("name");

        assertTrue(underTest.authorMatches("name", user));
    }

    @Test
    public void givenMisMatchingCases_whenFullNameMatches_thenTrue() {
        final User user = mock(User.class);
        when(user.getDisplayName()).thenReturn("nAmE");

        assertTrue(underTest.authorMatches("NaMe", user));
    }

    @Test
    public void givenLeadingSpaces_whenFullNameMatches_thenTrue() {
        final User user = mock(User.class);
        when(user.getDisplayName()).thenReturn(" nAmE");

        assertTrue(underTest.authorMatches(" NaMe", user));
    }

    @Test
    public void givenTrailingSpaces_whenFullNameMatches_thenTrue() {
        final User user = mock(User.class);
        when(user.getDisplayName()).thenReturn("nAmE    ");

        assertTrue(underTest.authorMatches("NaMe ", user));
    }

    @Test
    public void givenPartialFullName_whenNameMatches_thenTrue() {
        final User user = mock(User.class);
        when(user.getDisplayName()).thenReturn("full name");

        assertTrue(underTest.authorMatches("name", user));
    }
}
