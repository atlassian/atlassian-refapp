package com.atlassian.refapp.audit.spi;

import com.atlassian.audit.api.AuditConsumer;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import static org.junit.Assert.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

public class RefimplLegacyAuditEntityMigratorTest {
    @Rule
    public final MethodRule mockitoRule = MockitoJUnit.rule();

    @Mock
    AuditConsumer auditConsumer;

    @InjectMocks
    RefimplLegacyAuditEntityMigrator underTest;

    @Test
    public void testMigrate_shouldBlowUp_whenTheConsumerIsNull() {
        assertThrows(NullPointerException.class, () -> underTest.migrate(null));
    }

    @Test
    public void testMigrate_shouldDoNothing() {
        underTest.migrate(auditConsumer);

        verify(auditConsumer, never()).accept(any());
    }
}
