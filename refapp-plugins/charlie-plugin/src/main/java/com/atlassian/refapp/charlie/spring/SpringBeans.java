package com.atlassian.refapp.charlie.spring;

import com.atlassian.plugin.web.api.DynamicWebInterfaceManager;
import com.atlassian.refapp.charlie.CharlieStore;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.websudo.WebSudoManager;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.importOsgiService;

/**
 * Transformerless spring bean configuration.
 * See <a href="https://developer.atlassian.com/server/framework/atlassian-sdk/spring-java-config/">Spring Java Config</a>
 * @since 5.1.3
 */
@Configuration
public class SpringBeans {
    @Bean
    public ApplicationProperties applicationProperties() {
        return importOsgiService(ApplicationProperties.class);
    }

    @Bean
    public CharlieStore charlieStore() {
        return new CharlieStore(pluginSettingsFactory());
    }

    @Bean
    public DynamicWebInterfaceManager webInterfaceManager() {
        return importOsgiService(DynamicWebInterfaceManager.class);
    }

    @Bean
    public PageBuilderService pageBuilderService() {
        return importOsgiService(PageBuilderService.class);
    }

    @Bean
    public PluginSettingsFactory pluginSettingsFactory() {
        return importOsgiService(PluginSettingsFactory.class);
    }

    @Bean
    public TemplateRenderer templateRenderer() {
        return importOsgiService(TemplateRenderer.class);
    }

    @Bean
    public WebSudoManager salWebSudoManager() {
        return importOsgiService(WebSudoManager.class);
    }
}
