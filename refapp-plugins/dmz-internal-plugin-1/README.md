## dmz-internal-plugin-1 - Description

The purpose of this plugin is to ensure that internal plugins have access to
Atlassian internal packages via OSGi. Additionally, it will be used as showcase
how internal plugins will behave when accessing Atlassian internal packages via
OSGi.

Jackson 2.x components are exported by system bundle and added as Atlassian
internal packages (DMZ functionality).

This plugin is recognized by DMZ functionality as internal plugin, because the
plugin key starts with 'com.atlassian'. DMZ logic can be found in 
atlassian-plugins repository.
