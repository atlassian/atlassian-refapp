package com.internal.plugin;

import com.fasterxml.jackson.core.Version;

public class FooService {

    /**
     * <p>Internal plugin accessing Jackson 2.x.</p>
     * <p>Jackson 2.x is accessible only for internal plugins (DMZ functionality).</p>
     * <p>This plugin is recognized by DMZ functionality as internal plugin, because the
     * plugin key starts with 'com.atlassian'. Read more in README of this plugin</p>
     */
    public void tryToAccessJacksonCore() {
        Version version = Version.unknownVersion();
    }
}
