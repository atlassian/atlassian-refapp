package com.atlassian.refapp.trustedapps.spring;

import com.atlassian.security.auth.trustedapps.TrustedApplicationsConfigurationManager;
import com.atlassian.security.auth.trustedapps.TrustedApplicationsManager;
import com.atlassian.security.auth.trustedapps.UserResolver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.importOsgiService;

@Configuration
public class SpringBeans {
    @Bean
    public TrustedApplicationsConfigurationManager trustedApplicationsConfigurationManager() {
        return importOsgiService(TrustedApplicationsConfigurationManager.class);
    }

    @Bean
    public TrustedApplicationsManager trustedApplicationsManager() {
        return importOsgiService(TrustedApplicationsManager.class);
    }

    @Bean
    public UserResolver userResolver() {
        return importOsgiService(UserResolver.class);
    }
}
