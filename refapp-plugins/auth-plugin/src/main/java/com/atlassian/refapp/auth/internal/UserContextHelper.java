package com.atlassian.refapp.auth.internal;

import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.seraph.auth.AuthenticationContext;

import java.security.Principal;

public class UserContextHelper {
    private final AuthenticationContext authenticationContext;
    private final CrowdService crowdService;

    public UserContextHelper(AuthenticationContext authenticationContext,
                             CrowdService crowdService) {
        this.authenticationContext = authenticationContext;
        this.crowdService = crowdService;
    }

    public User getRemoteUser() {
        Principal principal = authenticationContext.getUser();
        if (principal == null) {
            return null;
        }
        return crowdService.getUser(principal.getName());
    }

    public boolean isRemoteUserAdministrator() {
        return isRemoteUserRole("administrators");
    }

    public boolean isRemoteUserSystemAdministrator() {
        return isRemoteUserRole("system_administrators");
    }

    private boolean isRemoteUserRole(String role) {
        User user = getRemoteUser();
        if (user == null) {
            return false;
        }
        return crowdService.isUserMemberOfGroup(user.getName(), role);
    }
}
