package com.atlassian.refapp.auth.internal.rest;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize
public class RestError {

    @JsonProperty
    String context;
    @JsonProperty
    String error;

    public RestError(String context, String error) {
        this.context = context;
        this.error = error;
    }
}
