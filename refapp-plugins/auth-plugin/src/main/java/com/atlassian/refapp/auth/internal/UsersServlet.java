package com.atlassian.refapp.auth.internal;

import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.crowd.exception.InvalidCredentialException;
import com.atlassian.crowd.exception.InvalidUserException;
import com.atlassian.crowd.exception.OperationNotPermittedException;
import com.atlassian.crowd.model.user.ImmutableUser;
import com.atlassian.crowd.model.user.ImmutableUserWithAttributes;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import com.google.common.collect.HashMultimap;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.log.JdkLogChute;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class UsersServlet extends HttpServlet {
    private final CrowdService crowdService;
    private final VelocityEngine velocity;
    private final PageBuilderService pageBuilderService;

    public UsersServlet(CrowdService crowdService, PageBuilderService pageBuilderService) {
        this.crowdService = crowdService;
        velocity = new VelocityEngine();
        velocity.addProperty(Velocity.RUNTIME_LOG_LOGSYSTEM_CLASS, JdkLogChute.class.getName());
        velocity.addProperty(Velocity.RESOURCE_LOADER, "classpath");
        velocity.addProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
        this.pageBuilderService = pageBuilderService;
    }

    private void sendRedirect(HttpServletResponse response, HttpServletRequest request, String path) throws IOException {
        response.sendRedirect(request.getRequestURL().append(path).toString());
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        String pathInfo = request.getPathInfo();
        if (pathInfo == null || pathInfo.length() <= 1) {
            final List<User> users = new ArrayList<>();
            EntityQuery<User> userEntityQuery = QueryBuilder.queryFor(User.class, EntityDescriptor.user()).returningAtMost(Integer.MAX_VALUE);
            Iterator<User> iter = crowdService.search(userEntityQuery).iterator();
            while (iter.hasNext()) {
                users.add(iter.next());
            }
            pageBuilderService.assembler().resources().requireWebResource("com.atlassian.auiplugin:aui-table");
            render("/users.vm", response, "users", users);
        } else {
            String username = pathInfo.substring(1);
            User user = crowdService.getUser(username);
            render("/profile.vm", response, "user", user);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            String pathInfo = request.getPathInfo();
            if (pathInfo == null || pathInfo.length() <= 1) {
                String name = request.getParameter("name");

                ImmutableUser immutableUser = ImmutableUser.builder(name)
                        .emailAddress(request.getParameter("email"))
                        .displayName(request.getParameter("fullName"))
                        .active(true)
                        .build();
                final ImmutableUserWithAttributes immutableUserWithAttributes = new ImmutableUserWithAttributes.Builder(immutableUser, HashMultimap.create()).build();
                crowdService.addUser(immutableUserWithAttributes, request.getParameter("password"));

                sendRedirect(response, request, "/" + name);
            } else {
                // Supporting PUT with POST because HTML doesn't support PUT
                String username = pathInfo.substring(1);
                updateUser(username, request);
                sendRedirect(response, request, "");
            }
        } catch (InvalidCredentialException | InvalidUserException | OperationNotPermittedException e) {
            throw new ServletException(e);
        }
    }

    private void updateUser(String username, HttpServletRequest request) throws InvalidCredentialException, OperationNotPermittedException, InvalidUserException {
        String fullName = request.getParameter("fullName");
        String password = request.getParameter("password");
        String email = request.getParameter("email");

        ImmutableUser immutableUser = ImmutableUser.builder(username)
                .emailAddress(email)
                .displayName(fullName)
                .active(true)
                .build();

        crowdService.updateUser(immutableUser);
        crowdService.updateUserCredential(immutableUser, password);
    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            String pathInfo = request.getPathInfo();
            String username = pathInfo.substring(1);
            updateUser(username, request);
            sendRedirect(response, request, "");
        } catch (InvalidCredentialException | InvalidUserException | OperationNotPermittedException e) {
            throw new ServletException(e);
        }
    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        try {
            String pathInfo = request.getPathInfo();
            String username = pathInfo.substring(1);
            User user = crowdService.getUser(username);
            crowdService.removeUser(user);
            sendRedirect(response, request, "");
        } catch (OperationNotPermittedException e) {
            throw new ServletException(e);
        }
    }

    private void render(String templateName, HttpServletResponse response, Object... context)
            throws ServletException {
        VelocityContext velocityContext = new VelocityContext();
        for (int i = 0; i < context.length - 1; i += 2) {
            velocityContext.put((String) context[i], context[i + 1]);
        }
        try {
            Template template = velocity.getTemplate(templateName);
            template.merge(velocityContext, response.getWriter());
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }
}
