package com.atlassian.refapp.auth.internal;

import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.seraph.auth.Authenticator;
import com.atlassian.seraph.auth.RoleMapper;
import com.atlassian.seraph.config.SecurityConfig;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.Map;

public class GroupRoleMapper implements RoleMapper {
    private final CrowdService crowdService;

    public GroupRoleMapper(Authenticator authenticator, CrowdService crowdService) {
        this.crowdService = crowdService;
        StaticDelegatingAuthenticator.setAuthenticator(authenticator);
        StaticDelegatingRoleMapper.setRoleMapper(this);
    }

    public void init(Map params, SecurityConfig config) {
    }

    /**
     * Assume that roles == groups.
     */
    public boolean hasRole(Principal user, HttpServletRequest request, String role) {
        return crowdService.isUserMemberOfGroup(user.getName(), role);
    }

    public boolean canLogin(Principal user, HttpServletRequest request) {
        return user != null;
    }
}
