package com.atlassian.refapp.auth.internal.rest;

import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.crowd.exception.InvalidCredentialException;
import com.atlassian.crowd.exception.InvalidUserException;
import com.atlassian.crowd.exception.OperationNotPermittedException;
import com.atlassian.crowd.model.user.ImmutableUser;
import com.atlassian.crowd.model.user.ImmutableUserWithAttributes;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.refapp.auth.internal.RefappPermissions;
import com.atlassian.refapp.auth.internal.UserContextHelper;
import com.atlassian.seraph.auth.AuthenticationContext;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Ordering;
import org.apache.commons.lang3.StringUtils;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Optional.ofNullable;
import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.FORBIDDEN;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;
import static org.apache.commons.lang3.StringUtils.capitalize;

/**
 * Simple REST resource to manage users in Refapp
 *
 * @since 3.0
 */
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Path("users")
public class UserManagementResource {

    private final UserContextHelper userContextHelper;
    private final CrowdService crowdService;

    public UserManagementResource(AuthenticationContext authenticationContext, CrowdService crowdService) {
        this.crowdService = crowdService;
        this.userContextHelper = new UserContextHelper(authenticationContext, crowdService);
    }

    @GET
    @SuppressWarnings("unchecked")
    public Response getAllUsers() {
        checkAdminPermission();

        EntityQuery<User> userEntityQuery = QueryBuilder
                .queryFor(User.class, EntityDescriptor.user())
                .returningAtMost(Integer.MAX_VALUE);

        final List<RestUser> restUsers = new ArrayList<>();

        for (User user : crowdService.search(userEntityQuery)) {
            restUsers.add(createRestUser(user, getPermissionLevel(user)));
        }

        return Response.ok(restUsers).build();
    }

    @GET
    @Path("{username}")
    public Response getUser(@PathParam("username") String username) {
        checkAdminPermission();
        checkUsername(username);

        User user = crowdService.getUser(username);
        if (user == null) {
            return Response.status(NOT_FOUND)
                    .cacheControl(noCache())
                    .entity(new RestError(null, String.format("User with username '%s' not found", username)))
                    .build();
        }

        return Response.ok(createRestUser(user, getPermissionLevel(user))).build();
    }

    @PUT
    @Path("{username}")
    public Response createOrUpdateUser(@PathParam("username") String username, RestUser userData) throws InvalidCredentialException, InvalidUserException, OperationNotPermittedException {
        checkAdminPermission();
        checkUsername(username);

        final String email = ofNullable(userData.email).orElse(username + "@example.com");
        final String fullName = ofNullable(userData.fullName).orElse(capitalize(username));
        final RefappPermissions permissionLevel = ofNullable(userData.permissionLevel).orElse(RefappPermissions.USER);

        ImmutableUser immutableUser = ImmutableUser.builder(username)
                .emailAddress(email)
                .displayName(fullName)
                .active(true)
                .build();
        final ImmutableUserWithAttributes immutableUserWithAttributes = new ImmutableUserWithAttributes.Builder(
                immutableUser, HashMultimap.create()).build();
        crowdService.addUser(immutableUserWithAttributes, username);

        for (RefappPermissions permission : RefappPermissions.values()) {
            if (permission.compareTo(permissionLevel) <= 0) {
                crowdService.addUserToGroup(immutableUserWithAttributes, crowdService.getGroup(permission.groupName()));
            }
        }

        // location will be set to the currently requested URI, which is correct URI for the created user
        return Response.created(URI.create(""))
                .entity(createRestUser(immutableUserWithAttributes, permissionLevel))
                .build();
    }

    @DELETE
    @Path("{username}")
    public Response removeUser(@PathParam("username") String username) throws OperationNotPermittedException {
        checkAdminPermission();
        checkUsername(username);

        final User user = crowdService.getUser(username);
        if (user != null) {
            crowdService.removeUser(user);
        }

        return Response.noContent()
                .build();
    }

    private void checkAdminPermission() {
        if (!userContextHelper.isRemoteUserSystemAdministrator() && !userContextHelper.isRemoteUserAdministrator()) {
            throw new WebApplicationException(Response.status(FORBIDDEN)
                    .entity(new RestError(null, "You need administrator permission to access this resource"))
                    .cacheControl(noCache())
                    .build());
        }
    }

    private void checkUsername(@PathParam("username") String username) {
        if (StringUtils.isEmpty(username)) {
            throw new WebApplicationException(Response.status(BAD_REQUEST)
                    .entity(new RestError("username", "Username not set"))
                    .cacheControl(noCache())
                    .build());
        }
    }

    private RestUser createRestUser(User user, RefappPermissions permissionLevel) {
        final RestUser restUser = new RestUser();
        restUser.name = user.getName();
        restUser.fullName = user.getDisplayName();
        restUser.email = user.getEmailAddress();
        restUser.permissionLevel = permissionLevel;

        return restUser;
    }

    private RefappPermissions getPermissionLevel(User user) {
        for (RefappPermissions permission : Ordering.natural()
                .reverse()
                .immutableSortedCopy(asList(RefappPermissions.values()))) {
            if (isMember(user, permission.groupName())) {
                return permission;
            }
        }
        return RefappPermissions.USER;
    }

    private boolean isMember(User user, String group) {
        return crowdService.isUserMemberOfGroup(user, crowdService.getGroup(group));
    }

    private static CacheControl noCache() {
        CacheControl cacheControl = new CacheControl();
        cacheControl.setNoCache(true);
        cacheControl.setNoStore(true);
        return cacheControl;
    }
}
