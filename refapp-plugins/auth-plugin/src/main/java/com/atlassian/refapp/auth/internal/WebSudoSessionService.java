package com.atlassian.refapp.auth.internal;

import com.atlassian.refapp.auth.external.WebSudoSessionManager;

import javax.servlet.http.HttpServletRequest;

public class WebSudoSessionService {

    private final WebSudoSessionManager webSudoSessionManager;

    public WebSudoSessionService(final WebSudoSessionManager webSudoSessionManager) {
        this.webSudoSessionManager = webSudoSessionManager;
    }

    void createWebSudoSession(final HttpServletRequest request, final boolean isAuthenticated) {
        if (isAuthenticated && request.getParameter("os_websudo") != null) {
            webSudoSessionManager.createWebSudoSession(request);
        }
    }
}
