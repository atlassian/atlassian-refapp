package com.atlassian.refapp.auth.internal;

import com.atlassian.refapp.api.EmbeddedCrowdAuthenticator;
import com.atlassian.seraph.auth.AuthenticatorException;
import com.atlassian.seraph.auth.DefaultAuthenticator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.Principal;

public class EmbeddedCrowdUserAuthenticator extends DefaultAuthenticator {
    private static final Logger LOG = LoggerFactory.getLogger(EmbeddedCrowdUserAuthenticator.class);

    private final WebSudoSessionService webSudoSessionService;
    private final EmbeddedCrowdAuthenticator embeddedCrowdAuthenticator;

    public EmbeddedCrowdUserAuthenticator(WebSudoSessionService webSudoSessionService,
                                          EmbeddedCrowdAuthenticator embeddedCrowdAuthenticator) {
        this.webSudoSessionService = webSudoSessionService;
        this.embeddedCrowdAuthenticator = embeddedCrowdAuthenticator;
    }

    @Override
    public boolean login(HttpServletRequest request, HttpServletResponse response,
                         String username, String password, boolean cookie) throws AuthenticatorException {
        try {
            boolean isAuthenticated = super.login(request, response, username, password, cookie);
            webSudoSessionService.createWebSudoSession(request, isAuthenticated);
            return isAuthenticated;
        } catch (AuthenticatorException e) {
            LOG.error("User repository communication failed: ", e);
            throw new AuthenticatorException("User repository communication failed");
        }
    }

    @Override
    protected boolean authenticate(final Principal user, final String password) {
        return embeddedCrowdAuthenticator.authenticateUser(user.getName(), password) != null;
    }

    @Override
    protected Principal getUser(final String username) {
        return embeddedCrowdAuthenticator.findUserByName(username);
    }
}
