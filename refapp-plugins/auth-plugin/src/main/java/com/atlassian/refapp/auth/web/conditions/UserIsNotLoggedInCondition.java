package com.atlassian.refapp.auth.web.conditions;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.Condition;
import com.atlassian.refapp.auth.internal.UserContextHelper;

import java.util.Map;
import java.util.Optional;

/**
 * A {@link Condition} that checks whether no user is logged in.
 */
public class UserIsNotLoggedInCondition implements Condition {

    @Override
    public void init(final Map<String, String> params) throws PluginParseException {
        // Nothing to do
    }

    @Override
    public boolean shouldDisplay(final Map<String, Object> context) {
        return Optional.ofNullable(context)
                .map(ctx -> ctx.get("userHelper"))
                .filter(UserContextHelper.class::isInstance)
                .map(UserContextHelper.class::cast)
                .filter(userContextHelper -> userContextHelper.getRemoteUser() == null)
                .isPresent();
    }
}
