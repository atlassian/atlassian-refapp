package com.atlassian.refapp.auth.spring;

import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.refapp.api.EmbeddedCrowdAuthenticator;
import com.atlassian.refapp.auth.external.WebSudoSessionManager;
import com.atlassian.refapp.auth.internal.DefaultWebSudoSessionManager;
import com.atlassian.refapp.auth.internal.EmbeddedCrowdUserAuthenticator;
import com.atlassian.refapp.auth.internal.GroupRoleMapper;
import com.atlassian.refapp.auth.internal.WebSudoSessionService;
import com.atlassian.seraph.auth.AuthenticationContext;
import com.atlassian.seraph.auth.AuthenticationContextImpl;
import com.atlassian.seraph.auth.RoleMapper;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import org.osgi.framework.ServiceRegistration;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.atlassian.plugins.osgi.javaconfig.ExportOptions.as;
import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.exportOsgiService;
import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.importOsgiService;

/**
 * Transformerless spring bean configuration.
 * See <a href="https://developer.atlassian.com/server/framework/atlassian-sdk/spring-java-config/">Spring Java Config</a>
 * @since 5.1.3
 */
@Configuration
public class SpringBeans {

    @Bean
    public CrowdService importCrowdService() {
        return importOsgiService(CrowdService.class);
    }

    @Bean
    public AuthenticationContext authenticationContext() {
        return new AuthenticationContextImpl();
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportAuthenticationContext(final AuthenticationContext authenticationContext) {
        return exportOsgiService(authenticationContext, as(AuthenticationContext.class));
    }


    @Bean
    public FactoryBean<ServiceRegistration> exportRoleMapper(final RoleMapper roleMapper) {
        return exportOsgiService(roleMapper, as(RoleMapper.class));
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportWebSudoSessionManager(final WebSudoSessionManager webSudoSessionManager) {
        return exportOsgiService(webSudoSessionManager, as(WebSudoSessionManager.class));
    }

    @Bean
    public PageBuilderService pageBuilderService() {
        return importOsgiService(PageBuilderService.class);
    }

    @Bean
    public RoleMapper roleMapper(com.atlassian.seraph.auth.Authenticator seraphAuthenticator, CrowdService crowdService) {
        return new GroupRoleMapper(seraphAuthenticator, crowdService);
    }

    @Bean
    public com.atlassian.seraph.auth.Authenticator seraphEmbeddedCrowdAuthenticator(
            WebSudoSessionService webSudoSessionService, EmbeddedCrowdAuthenticator embeddedCrowdAuthenticator) {
        return new EmbeddedCrowdUserAuthenticator(webSudoSessionService, embeddedCrowdAuthenticator);
    }

    @Bean
    public WebSudoSessionManager webSudoSessionManager() {
        return new DefaultWebSudoSessionManager();
    }

    @Bean
    public WebSudoSessionService webSudoSessionService(WebSudoSessionManager websudoManager) {
        return new WebSudoSessionService(websudoManager);
    }

    @Bean
    public EmbeddedCrowdAuthenticator importEmbeddedCrowdAuthenticator() {
        return importOsgiService(EmbeddedCrowdAuthenticator.class);
    }
}
