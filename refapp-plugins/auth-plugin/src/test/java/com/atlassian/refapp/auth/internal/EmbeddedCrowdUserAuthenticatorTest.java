package com.atlassian.refapp.auth.internal;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.refapp.api.EmbeddedCrowdAuthenticator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.security.Principal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class EmbeddedCrowdUserAuthenticatorTest {
    private static final String USERNAME = "admin";
    private static final String PASSWORD = "admin";
    private static final User DEFAULT_USER = new User() {
        @Override
        public long getDirectoryId() {
            return 0;
        }

        @Override
        public boolean isActive() {
            return true;
        }

        @Override
        public String getEmailAddress() {
            return USERNAME.concat("@email.com");
        }

        @Override
        public String getDisplayName() {
            return USERNAME;
        }

        @Override
        public int compareTo(User user) {
            return 0;
        }

        @Override
        public String getName() {
            return USERNAME;
        }
    };

    @Mock
    private EmbeddedCrowdAuthenticator embeddedCrowdAuthenticator;

    @InjectMocks
    private EmbeddedCrowdUserAuthenticator embeddedCrowdUserAuthenticator;

    @Test
    public void shouldSuccessfullyAuthenticate() {
        // given
        when(embeddedCrowdAuthenticator.authenticateUser(USERNAME, PASSWORD)).thenReturn(DEFAULT_USER);

        // when
        boolean authenticate = embeddedCrowdUserAuthenticator.authenticate(() -> USERNAME, PASSWORD);

        // then
        assertTrue(authenticate);
        verify(embeddedCrowdAuthenticator).authenticateUser(USERNAME, PASSWORD);
    }

    @Test
    public void shouldFailAuthentication() {
        // given
        when(embeddedCrowdAuthenticator.authenticateUser(USERNAME, PASSWORD)).thenReturn(null);

        // when
        boolean authenticate = embeddedCrowdUserAuthenticator.authenticate(() -> USERNAME, PASSWORD);

        // then
        assertFalse(authenticate);
        verify(embeddedCrowdAuthenticator).authenticateUser(USERNAME, PASSWORD);
    }

    @Test
    public void shouldGetUserByUsername() {
        // given
        when(embeddedCrowdAuthenticator.findUserByName(USERNAME)).thenReturn(DEFAULT_USER);

        // when
        Principal principal = embeddedCrowdUserAuthenticator.getUser(USERNAME);

        // then
        assertEquals(USERNAME, principal.getName());
        verify(embeddedCrowdAuthenticator).findUserByName(USERNAME);
    }
}
