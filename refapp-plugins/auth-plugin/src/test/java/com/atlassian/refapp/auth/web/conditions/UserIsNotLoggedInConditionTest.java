package com.atlassian.refapp.auth.web.conditions;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.refapp.auth.internal.UserContextHelper;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Map;

import static java.util.Collections.emptyMap;
import static java.util.Collections.singletonMap;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ParametersAreNonnullByDefault
public class UserIsNotLoggedInConditionTest {

    @Rule
    public final MethodRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private UserContextHelper userContextHelper;

    @InjectMocks
    private UserIsNotLoggedInCondition condition;

    @Test
    public void shouldDisplay_whenUserHelperNotFound_shouldReturnFalse() {
        // Act
        final boolean shouldDisplay = condition.shouldDisplay(emptyMap());

        // Assert
        assertThat(shouldDisplay, is(false));
    }

    @Test
    public void shouldDisplay_whenRemoteUserNotFound_shouldReturnTrue() {
        when(userContextHelper.getRemoteUser()).thenReturn(null);
        assertShouldDisplay(true);
    }

    @Test
    public void shouldDisplay_whenRemoteUserFound_shouldReturnFalse() {
        when(userContextHelper.getRemoteUser()).thenReturn(mock(User.class));
        assertShouldDisplay(false);
    }

    private void assertShouldDisplay(final boolean expectedResult) {
        final Map<String, Object> context = singletonMap("userHelper", userContextHelper);

        // Act
        final boolean shouldDisplay = condition.shouldDisplay(context);

        // Assert
        assertThat(shouldDisplay, is(expectedResult));
    }
}
