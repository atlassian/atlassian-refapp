package com.atlassian.refapp.auth.web.conditions;

import com.atlassian.refapp.auth.internal.UserContextHelper;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Map;

import static java.util.Collections.singletonMap;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;

@ParametersAreNonnullByDefault
public class UserIsSystemAdministratorConditionTest {

    @Rule
    public final MethodRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private UserContextHelper userContextHelper;

    @InjectMocks
    private UserIsSystemAdministratorCondition condition;

    @Test
    public void shouldDisplay_whenUserHelperNotFound_shouldReturnFalse() {
        assertShouldDisplay(null, true, false);
    }

    @Test
    public void shouldDisplay_whenRemoteUserIsNotSystemAdmin_shouldReturnFalse() {
        assertShouldDisplay(userContextHelper, false, false);
    }

    @Test
    public void shouldDisplay_whenRemoteUserIsSystemAdmin_shouldReturnTrue() {
        assertShouldDisplay(userContextHelper, true, true);
    }

    private void assertShouldDisplay(@Nullable final UserContextHelper userContextHelper, final boolean isSystemAdmin,
                                     final boolean expectedResult) {
        final Map<String, Object> context = singletonMap("userHelper", userContextHelper);
        if (userContextHelper != null) {
            when(userContextHelper.isRemoteUserSystemAdministrator()).thenReturn(isSystemAdmin);
        }

        // Act
        final boolean shouldDisplay = condition.shouldDisplay(context);

        // Assert
        assertThat(shouldDisplay, is(expectedResult));
    }
}
