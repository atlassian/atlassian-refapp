## dmz-external-plugin - Description

The purpose of this plugin is to ensure that external plugins can't access
Atlassian internal packages via OSGi. Additionally, it will be used as showcase
how external plugins will behave when accessing Atlassian internal packages via
OSGi.

Jackson 2.x components are exported by system bundle and added as Atlassian
internal packages (DMZ functionality).

This plugin is trying to use Jackson 2.x `com.fasterxml.jackson.core` package.
See: [FooService.java](src%2Fmain%2Fjava%2Fcom%2Fexternal%2Fplugin%2FFooService.java).

When external plugin is accessing Atlassian internal packages via OSGi there 
will be `org.osgi.framework.BundleException`.

The error in logs will be similar to this one:
`WARN - 08:42:55,685 -                     com.atlassian.plugin.impl.AbstractPlugin - [localhost-startStop-1] - Because of this exception
com.atlassian.plugin.PluginException: org.osgi.framework.BundleException: Unable to resolve com.atlassian.refapp.dmz-external-plugin [75](R 75.0): missing requirement [com.atlassian.refapp.dmz-external-plugin [75](R 75.0)] osgi.wiring.package; (osgi.wiring.package=com.fasterxml.jackson.core) Unresolved requirements: [[com.atlassian.refapp.dmz-external-plugin [75](R 75.0)] osgi.wiring.package; (osgi.wiring.package=com.fasterxml.jackson.core)]`
