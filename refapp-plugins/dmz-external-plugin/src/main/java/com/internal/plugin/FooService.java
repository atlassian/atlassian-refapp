package com.internal.plugin;

import com.fasterxml.jackson.core.Version;

public class FooService {

    /**
     * <p>External plugin accessing Jackson 2.x.</p>
     * <p>Jackson 2.x is accessible only for internal plugins (DMZ functionality).</p>
     * <p>This plugin will be disabled with org.osgi.framework.BundleException in RefApp logs.</p>
     *
     * <p>Expected result is:</p>
     * <pre>com.atlassian.plugin.PluginException: org.osgi.framework.BundleException:
     * Unable to resolve com.atlassian.refapp.dmz-external-plugin</pre>
     */
    public void tryToAccessJacksonCore() {
        Version version = Version.unknownVersion();
    }
}
