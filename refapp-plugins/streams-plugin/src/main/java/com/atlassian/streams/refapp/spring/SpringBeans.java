package com.atlassian.streams.refapp.spring;

import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.streams.api.renderer.StreamsEntryRendererFactory;
import com.atlassian.streams.refapp.RefappEntityResolver;
import com.atlassian.streams.refapp.RefappRenderer;
import com.atlassian.streams.refapp.RefappStreamsActivityManager;
import com.atlassian.streams.refapp.api.StreamsActivityManager;
import com.atlassian.streams.spi.DefaultFormatPreferenceProvider;
import com.atlassian.streams.spi.EntityResolver;
import com.atlassian.streams.spi.FormatPreferenceProvider;
import com.atlassian.streams.spi.StreamsI18nResolver;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import org.osgi.framework.ServiceRegistration;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.atlassian.plugins.osgi.javaconfig.ExportOptions.as;
import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.exportOsgiService;
import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.importOsgiService;

@Configuration
public class SpringBeans {
    @Bean
    public ApplicationProperties applicationProperties() {
        return importOsgiService(ApplicationProperties.class);
    }

    @Bean
    public EntityResolver entityResolver(StreamsActivityManager streamsActivityManager) {
        return new RefappEntityResolver(streamsActivityManager);
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportEntityResolver(final EntityResolver entityResolver) {
        return exportOsgiService(entityResolver, as(EntityResolver.class));
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportFormatPreferenceProvider(
            final FormatPreferenceProvider formatPreferenceProvider
    )
    {
        return exportOsgiService(formatPreferenceProvider, as(FormatPreferenceProvider.class));
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportStreamsActivityManager(
            final StreamsActivityManager streamsActivityManager
    ) {
        return exportOsgiService(streamsActivityManager, as(StreamsActivityManager.class));
    }

    @Bean
    public FormatPreferenceProvider formatPreferenceProvider() {
        return new DefaultFormatPreferenceProvider();
    }

    @Bean
    public PageBuilderService pageBuilderService() {
        return importOsgiService(PageBuilderService.class);
    }

    @Bean
    public RefappRenderer refappRenderer(StreamsEntryRendererFactory rendererFactory) {
        return new RefappRenderer(rendererFactory);
    }

    @Bean
    public StreamsActivityManager streamsActivityManager(
            ApplicationProperties applicationProperties,
            StreamsI18nResolver i18nResolver,
            RefappRenderer refappRenderer
    ) {
        return new RefappStreamsActivityManager(applicationProperties, i18nResolver, refappRenderer);
    }

    @Bean
    public StreamsEntryRendererFactory streamsEntryRendererFactory() {
        return importOsgiService(StreamsEntryRendererFactory.class);
    }

    @Bean
    public StreamsI18nResolver streamsI18nResolver() {
        return importOsgiService(StreamsI18nResolver.class);
    }

    @Bean
    public TemplateRenderer templateRenderer() {
        return importOsgiService(TemplateRenderer.class);
    }
}
