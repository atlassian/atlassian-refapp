package com.atlassian.refapp.decorator.theme;

import com.atlassian.annotations.nullability.ReturnValuesAreNonnullByDefault;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.usersettings.UserSettingsService;
import com.atlassian.theme.api.Theme;
import com.atlassian.theme.internal.api.ThemeService;
import com.atlassian.theme.internal.api.user.PreferredColorMode;
import com.atlassian.theme.internal.api.user.UserThemeService;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Optional;

import static com.atlassian.theme.api.ThemeColorMode.DARK;
import static com.atlassian.theme.api.ThemeColorMode.LIGHT;
import static java.util.Objects.requireNonNull;

@ParametersAreNonnullByDefault
@ReturnValuesAreNonnullByDefault
public class RefAppUserThemeService implements UserThemeService {
    private final ThemeService themeService;
    private final UserSettingsService userSettingsService;

    public RefAppUserThemeService(
            @Nonnull ThemeService themeService,
            @Nonnull UserSettingsService userSettingsService
    ) {
        this.themeService = requireNonNull(themeService, "themeService");
        this.userSettingsService = requireNonNull(userSettingsService, "userSettingsService");
    }

    private static final String PREFERRED_COLOR_MODE_KEY = "atl-theme-preferred-color-mode";
    private static final String PREFERRED_DARK_THEME = "atl-theme-preferred-dark-theme";
    private static final String PREFERRED_LIGHT_THEME = "atl-theme-preferred-light-theme";

    @Override
    public Optional<PreferredColorMode> getPreferredColorMode(UserKey userKey) {
        requireNonNull(userKey, "userKey");

        Optional<String> storedColorMode = userSettingsService
                .getUserSettings(userKey)
                .getString(PREFERRED_COLOR_MODE_KEY)
                .toOptional();

        return storedColorMode.flatMap(PreferredColorMode::optionalValueOf);
    }

    @Override
    public Optional<Theme> getPreferredDarkTheme(UserKey userKey) {
        requireNonNull(userKey, "userKey");

        return themeService.findMatchingTheme(DARK, userSettingsService
                .getUserSettings(userKey)
                .getString(PREFERRED_DARK_THEME)
                .getOrNull());
    }

    @Override
    public Optional<Theme> getPreferredLightTheme(UserKey userKey) {
        requireNonNull(userKey, "userKey");

        return themeService.findMatchingTheme(LIGHT, userSettingsService
                .getUserSettings(userKey)
                .getString(PREFERRED_LIGHT_THEME)
                .getOrNull());
    }

    @Override
    public void setPreferredColorMode(UserKey userKey, PreferredColorMode preferredColorMode) {
        requireNonNull(userKey, "userKey");
        requireNonNull(preferredColorMode, "preferredColorMode");

        userSettingsService.updateUserSettings(userKey, userSettingsBuilder -> {
            userSettingsBuilder.put(PREFERRED_COLOR_MODE_KEY, preferredColorMode.toString());
            return userSettingsBuilder.build();
        });
    }

    @Override
    public void setPreferredDarkTheme(UserKey userKey, Theme theme) {
        requireNonNull(userKey, "userKey");
        requireNonNull(theme, "theme");

        userSettingsService.updateUserSettings(userKey, userSettingsBuilder -> {
            userSettingsBuilder.put(PREFERRED_DARK_THEME, theme.getThemeKey());
            return userSettingsBuilder.build();
        });
    }

    @Override
    public void setPreferredLightTheme(UserKey userKey, Theme theme) {
        requireNonNull(userKey, "userKey");
        requireNonNull(theme, "theme");

        userSettingsService.updateUserSettings(userKey, userSettingsBuilder -> {
            userSettingsBuilder.put(PREFERRED_LIGHT_THEME, theme.getThemeKey());
            return userSettingsBuilder.build();
        });
    }
}
