package com.atlassian.refapp.decorator.spring;

import com.atlassian.refapp.decorator.theme.RefAppUserThemeService;
import com.atlassian.refapp.decorator.theme.XssSafeRequestScopeThemeService;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.usersettings.UserSettingsService;
import com.atlassian.theme.internal.DefaultThemeService;
import com.atlassian.theme.internal.api.ThemeService;
import com.atlassian.theme.internal.api.user.UserThemeService;
import com.atlassian.theme.internal.request.DefaultRequestScopeThemeService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ThemeBeans {
    @Bean("requestScopeThemeService")
    public DefaultRequestScopeThemeService defaultRequestScopeThemeService(
            UserManager userManager,
            ThemeService themeService,
            UserThemeService userThemeService
    ) {
        return new XssSafeRequestScopeThemeService(
                userManager,
                themeService,
                userThemeService,
                false, // new light theme is default - make it obvious when support needs to be added
                true, // enable original theme - make it easy for testing back-compatibility
                true // match UA - be nice to engineer's eyes and make it obvious when support needs to be added
        );
    }

    @Bean
    public ThemeService themeService() {
        return new DefaultThemeService();
    }

    @Bean
    public UserThemeService userThemeService(
            ThemeService themeService,
            UserSettingsService userSettingsService
    ) {
        return new RefAppUserThemeService(themeService, userSettingsService);
    }
}
