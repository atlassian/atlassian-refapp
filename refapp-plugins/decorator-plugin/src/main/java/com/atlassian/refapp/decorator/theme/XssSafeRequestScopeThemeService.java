package com.atlassian.refapp.decorator.theme;

import com.atlassian.sal.api.user.UserManager;
import com.atlassian.theme.internal.api.ThemeService;
import com.atlassian.theme.internal.api.user.UserThemeService;
import com.atlassian.theme.internal.request.DefaultRequestScopeThemeService;
import com.atlassian.velocity.htmlsafe.HtmlSafe;

import javax.annotation.Nonnull;
import javax.servlet.http.HttpServletRequest;

/**
 * Anything that ends up being sent to the browser should be XSS safe!
 */
public class XssSafeRequestScopeThemeService extends DefaultRequestScopeThemeService {
    public XssSafeRequestScopeThemeService(
            @Nonnull UserManager userManager,
            @Nonnull ThemeService themeService,
            @Nonnull UserThemeService userThemeService,
            boolean originalThemeIsDefaultLightModeTheme,
            boolean originalThemeEnabled,
            boolean shouldMatchUserAgentByDefault
    ) {
        super(
                userManager,
                themeService,
                userThemeService,
                originalThemeIsDefaultLightModeTheme,
                originalThemeEnabled,
                shouldMatchUserAgentByDefault
        );
    }

    /**
     * The default implementation is safe, mark it as such!
     * <p>
     * Should eventually move RefApp to using the templates from AUI and/or with some mix of Soy for dogfooding
     */
    @Nonnull
    @Override
    @HtmlSafe
    public String getHtmlAttributesForThisRequest(@Nonnull HttpServletRequest httpServletRequest) {
        return super.getHtmlAttributesForThisRequest(httpServletRequest);
    }
}
