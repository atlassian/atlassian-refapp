package com.atlassian.refapp.decorator.spring;

import com.atlassian.plugin.webresource.WebResourceUrlProvider;
import com.atlassian.refapp.auth.external.WebSudoSessionManager;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.usersettings.UserSettingsService;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.templaterenderer.velocity.one.six.VelocityTemplateRenderer;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.importOsgiService;

@Configuration
public class OsgiServiceImports {
    @Bean
    public PageBuilderService pageBuilderService() {
        return importOsgiService(PageBuilderService.class);
    }

    @Bean
    public SoyTemplateRenderer soyTemplateRenderer() {
        return importOsgiService(SoyTemplateRenderer.class);
    }

    @Bean
    public UserManager userManager() {
        return importOsgiService(UserManager.class);
    }

    @Bean
    public UserSettingsService userSettingsService() {
        return importOsgiService(UserSettingsService.class);
    }

    @Bean
    public VelocityTemplateRenderer templateRenderer() {
        return importOsgiService(VelocityTemplateRenderer.class);
    }

    @Bean
    public WebResourceUrlProvider webResourceUrlProvider() {
        return importOsgiService(WebResourceUrlProvider.class);
    }

    @Bean
    public WebSudoSessionManager webSudoSessionManager() {
        return importOsgiService(WebSudoSessionManager.class);
    }
}
