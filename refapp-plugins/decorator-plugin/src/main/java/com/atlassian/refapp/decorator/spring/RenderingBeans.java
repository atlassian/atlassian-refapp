package com.atlassian.refapp.decorator.spring;

import com.atlassian.plugin.web.api.DynamicWebInterfaceManager;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.importOsgiService;

@Configuration
public class RenderingBeans {
    @Bean
    public DynamicWebInterfaceManager webInterfaceManager() {
        return importOsgiService(DynamicWebInterfaceManager.class);
    }

    @Bean
    public ObjectWriter objectWriter() {
        return new ObjectMapper().writer().withDefaultPrettyPrinter();
    }
}
