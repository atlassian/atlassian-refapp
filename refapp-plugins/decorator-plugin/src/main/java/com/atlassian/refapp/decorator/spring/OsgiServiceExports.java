package com.atlassian.refapp.decorator.spring;

import com.atlassian.theme.api.request.RequestScopeThemeService;
import com.atlassian.theme.internal.api.ThemeService;
import com.atlassian.theme.internal.api.request.RequestScopeThemeOverrideService;
import com.atlassian.theme.internal.api.user.UserThemeService;
import org.osgi.framework.ServiceRegistration;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.atlassian.plugins.osgi.javaconfig.ExportOptions.as;
import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.exportOsgiService;

@Configuration
public class OsgiServiceExports {
    @Bean
    public FactoryBean<ServiceRegistration> exportRequestScopeThemeService(RequestScopeThemeService requestScopeThemeService) {
        return exportOsgiService(requestScopeThemeService, as(RequestScopeThemeService.class, RequestScopeThemeOverrideService.class));
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportThemService(ThemeService themeService) {
        return exportOsgiService(themeService, as(ThemeService.class));
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportUserThemeService(UserThemeService userThemeService) {
        return exportOsgiService(userThemeService, as(UserThemeService.class));
    }
}
