package com.atlassian.refapp.api;

import com.atlassian.crowd.embedded.api.User;

import java.security.Principal;

public interface EmbeddedCrowdAuthenticator {

    User authenticateUser(String username, String password);

    User findUserByName(String username);
}
