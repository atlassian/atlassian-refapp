package com.atlassian.plugin.refimpl.lifecycle;

import org.junit.Test;
import org.springframework.context.SmartLifecycle;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Unit test of the {@link AbstractSmartLifecycle} class.
 *
 * Not a base class for other tests.
 */
public class AbstractSmartLifecycleTest {

    @Test
    public void isRunning_whenStartHasNotBeenCalled_shouldReturnFalse() {
        // Act
        final SmartLifecycle smartLifecycle = new NoOpSmartLifecycle();

        // Assert
        assertFalse(smartLifecycle.isRunning());
    }

    @Test
    public void isRunning_whenStartHasBeenCalled_shouldReturnTrue() {
        // Arrange
        final SmartLifecycle smartLifecycle = new NoOpSmartLifecycle();

        // Act
        smartLifecycle.start();

        // Assert
        assertTrue(smartLifecycle.isRunning());
    }

    @Test
    public void isRunning_whenStopHasBeenCalled_shouldReturnFalse() {
        // Arrange
        final SmartLifecycle smartLifecycle = new NoOpSmartLifecycle();
        smartLifecycle.start();

        // Act
        smartLifecycle.stop();

        // Assert
        assertFalse(smartLifecycle.isRunning());
    }

    private static class NoOpSmartLifecycle extends AbstractSmartLifecycle {

        private NoOpSmartLifecycle() {
            super(LifecycleOrder.values()[0]);
        }

        @Override
        protected void doStart() {}
    }
}
