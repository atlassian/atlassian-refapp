package com.atlassian.plugin.refimpl.feature;

import com.atlassian.sal.api.features.DarkFeatureManager;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.annotation.Nonnull;
import java.util.Optional;

import static com.atlassian.sal.api.features.DarkFeatureManager.ATLASSIAN_DARKFEATURE_PREFIX;
import static com.atlassian.sal.api.features.DarkFeatureManager.DISABLE_ALL_DARKFEATURES_PROPERTY;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class LimitedSystemPropertyDarkFeatureManagerTest {
    private static final String TEST_FEATURE_KEY = ATLASSIAN_DARKFEATURE_PREFIX +
            LimitedSystemPropertyDarkFeatureManagerTest.class.getSimpleName();

    private static Boolean disableAllDarkFeaturesWasSet;

    private DarkFeatureManager underTest;

    @BeforeClass
    public static void beforeClass() {
        disableAllDarkFeaturesWasSet = Boolean.getBoolean(DISABLE_ALL_DARKFEATURES_PROPERTY);
        disableSystemProperty(DISABLE_ALL_DARKFEATURES_PROPERTY);
    }

    @Before
    public void before() {
        underTest = new LimitedSystemPropertyDarkFeatureManager();
    }

    @Test
    public void testIsValidFeatureKey() {
        assertTrue(LimitedSystemPropertyDarkFeatureManager.isValidFeatureKey(TEST_FEATURE_KEY));
        assertFalse(LimitedSystemPropertyDarkFeatureManager.isValidFeatureKey("notAValidKey"));
        assertFalse(LimitedSystemPropertyDarkFeatureManager.isValidFeatureKey("a.feature.key.without.the.prefix"));
    }

    @Test
    public void testIsEnabledForAllUsers() {
        enableSystemProperty(TEST_FEATURE_KEY);
        assertEquals(Optional.of(TRUE), underTest.isEnabledForAllUsers(TEST_FEATURE_KEY));

        disableSystemProperty(TEST_FEATURE_KEY);
        assertEquals(Optional.of(FALSE), underTest.isEnabledForAllUsers(TEST_FEATURE_KEY));
    }

    @Test
    public void testIsFeatureEnabledForAllUsers() {
        enableSystemProperty(TEST_FEATURE_KEY);
        assertTrue(underTest.isFeatureEnabledForAllUsers(TEST_FEATURE_KEY));

        disableSystemProperty(TEST_FEATURE_KEY);
        assertFalse(underTest.isFeatureEnabledForAllUsers(TEST_FEATURE_KEY));
    }

    @AfterClass
    public static void afterClass() {
        System.setProperty(DISABLE_ALL_DARKFEATURES_PROPERTY, String.valueOf(disableAllDarkFeaturesWasSet));
    }

    private static void disableSystemProperty(@Nonnull final String propertyKey) {
        System.setProperty(propertyKey, FALSE.toString());
    }

    private static void enableSystemProperty(@Nonnull final String propertyKey) {
        System.setProperty(propertyKey, TRUE.toString());
    }
}
