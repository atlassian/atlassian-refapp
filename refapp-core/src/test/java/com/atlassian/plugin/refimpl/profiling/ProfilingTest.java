package com.atlassian.plugin.refimpl.profiling;

import com.atlassian.util.profiling.Ticker;
import com.atlassian.util.profiling.Timers;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.lang.management.ManagementFactory;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

public class ProfilingTest {
    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    private ProfilingConfigProvider profilingConfigProvider;
    @InjectMocks
    private JmxExposingManager jmxExposingManager;
    @InjectMocks
    private ProfilingService profilingService;

    @Test
    public void givenRefappProfilingEnabled_whenAMetricIsCreated_thenItShouldBeExposedViaJmx() {
        // Enable profiling
        when(profilingConfigProvider.shouldMetricsBeExposedViaJmx()).thenReturn(true);
        when(profilingConfigProvider.shouldMicrometerBeEnabled()).thenReturn(true);
        when(profilingConfigProvider.shouldAtlassianProfilingBeEnabled()).thenReturn(true);
        // Start profiling
        jmxExposingManager.onStart();
        profilingService.onStart();

        // Create a metric
        final String metricName = "testingMetricName" + Math.random();
        final Integer numberOfBeansBefore = ManagementFactory.getPlatformMBeanServer().getMBeanCount();
        try (Ticker ignored = Timers.startWithMetric(metricName)) {
            // pretend there's a code block here
        }

        // Using an increased number of beans instead of tying this test to the internals of how the
        // MBeans would be created inside
        assertEquals(Integer.valueOf(numberOfBeansBefore + 1), ManagementFactory.getPlatformMBeanServer().getMBeanCount());
    }
}
