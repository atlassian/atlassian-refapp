package com.atlassian.plugin.refimpl.servlet;

import com.atlassian.plugin.servlet.DownloadStrategy;
import org.junit.Test;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import java.util.List;
import java.util.Vector;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.web.context.WebApplicationContext.ROOT_WEB_APPLICATION_CONTEXT_ATTRIBUTE;

public class FileServerServletTest {

    @Test
    public void getDownloadStrategies_whenCalled_shouldReturnAllRegisteredStrategyBeans() throws Exception {
        // Arrange
        final ServletConfig servletConfig = mock(ServletConfig.class);
        final ServletContext servletContext = mock(ServletContext.class);
        when(servletConfig.getServletContext()).thenReturn(servletContext);
        when(servletContext.getAttributeNames()).thenReturn(new Vector<String>().elements());
        when(servletContext.getInitParameterNames()).thenReturn(new Vector<String>().elements());
        final AnnotationConfigWebApplicationContext spring = createApplicationContext(servletContext);
        when(servletContext.getAttribute(ROOT_WEB_APPLICATION_CONTEXT_ATTRIBUTE)).thenReturn(spring);
        final FileServerServlet fileServerServlet = new FileServerServlet();
        fileServerServlet.init(servletConfig);

        // Act
        final List<DownloadStrategy> downloadStrategies = fileServerServlet.getDownloadStrategies();

        // Assert
        assertThat(downloadStrategies, hasSize(2));
    }

    private AnnotationConfigWebApplicationContext createApplicationContext(final ServletContext servletContext) {
        final AnnotationConfigWebApplicationContext spring = new AnnotationConfigWebApplicationContext();
        spring.register(FileServerServletBeans.class);
        spring.setServletContext(servletContext);
        spring.refresh();
        return spring;
    }

    @Configuration
    public static class FileServerServletBeans {

        @Bean
        public Object nonStrategyBean() {
            return new Object();
        }

        @Bean
        public DownloadStrategy strategyOne() {
            return mock(DownloadStrategy.class);
        }

        @Bean
        public DownloadStrategy strategyTwo() {
            return mock(DownloadStrategy.class);
        }
    }
}
