package com.atlassian.plugin.refimpl;

import com.atlassian.beehive.ClusterLockService;
import com.atlassian.cache.CacheFactory;
import com.atlassian.crowd.audit.AuditLogContext;
import com.atlassian.crowd.audit.AuditLogContextInternal;
import com.atlassian.crowd.core.event.listener.AutoGroupAdderListener;
import com.atlassian.crowd.core.event.listener.DirectoryDefaultGroupMembershipResolver;
import com.atlassian.crowd.crypto.DirectoryPasswordsEncryptor;
import com.atlassian.crowd.darkfeature.CrowdDarkFeatureManager;
import com.atlassian.crowd.directory.LdapDirectoryClearingClusterEventPublisher;
import com.atlassian.crowd.directory.ldap.cache.CacheRefresherFactory;
import com.atlassian.crowd.directory.ldap.cache.DirectoryCacheFactory;
import com.atlassian.crowd.directory.loader.AzureAdDirectoryInstanceLoader;
import com.atlassian.crowd.directory.loader.CustomDirectoryInstanceLoader;
import com.atlassian.crowd.directory.loader.DbCachingRemoteDirectoryInstanceLoader;
import com.atlassian.crowd.directory.loader.DelegatedAuthenticationDirectoryInstanceLoader;
import com.atlassian.crowd.directory.loader.DelegatingDirectoryInstanceLoader;
import com.atlassian.crowd.directory.loader.InternalDirectoryInstanceLoader;
import com.atlassian.crowd.directory.loader.LDAPDirectoryInstanceLoader;
import com.atlassian.crowd.directory.loader.RemoteCrowdDirectoryInstanceLoader;
import com.atlassian.crowd.embedded.api.CrowdDirectoryService;
import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.crowd.embedded.api.Encryptor;
import com.atlassian.crowd.embedded.core.CrowdEmbeddedApplicationFactory;
import com.atlassian.crowd.embedded.spi.DcLicenseChecker;
import com.atlassian.crowd.embedded.validator.DirectoryValidatorFactory;
import com.atlassian.crowd.event.EventStore;
import com.atlassian.crowd.manager.application.ApplicationManager;
import com.atlassian.crowd.manager.application.ApplicationService;
import com.atlassian.crowd.manager.application.AuthenticationOrderOptimizer;
import com.atlassian.crowd.manager.application.filtering.AccessFilterFactory;
import com.atlassian.crowd.manager.application.search.SearchStrategyFactory;
import com.atlassian.crowd.manager.audit.AuditLogChangesetPopulator;
import com.atlassian.crowd.manager.audit.AuditLogEnabledChecker;
import com.atlassian.crowd.manager.audit.AuditLogMetadataResolver;
import com.atlassian.crowd.manager.audit.AuditLogSystemProperties;
import com.atlassian.crowd.manager.audit.AuditService;
import com.atlassian.crowd.manager.audit.mapper.AuditLogDiffResultMapper;
import com.atlassian.crowd.manager.audit.mapper.AuditLogGroupMapper;
import com.atlassian.crowd.manager.audit.mapper.AuditLogUserMapper;
import com.atlassian.crowd.manager.avatar.AvatarProvider;
import com.atlassian.crowd.manager.directory.BeforeGroupRemoval;
import com.atlassian.crowd.manager.directory.DirectorySynchronisationInformationStore;
import com.atlassian.crowd.manager.directory.DirectorySynchronisationTokenStore;
import com.atlassian.crowd.manager.directory.DirectorySynchroniser;
import com.atlassian.crowd.manager.directory.InternalSynchronisationStatusManager;
import com.atlassian.crowd.manager.directory.TransactionalDirectoryDao;
import com.atlassian.crowd.manager.directory.monitor.poller.DirectoryPollerJobRunner;
import com.atlassian.crowd.manager.directory.monitor.poller.DirectoryPollerManager;
import com.atlassian.crowd.manager.directory.nestedgroups.NestedGroupsCacheProvider;
import com.atlassian.crowd.manager.permission.PermissionManager;
import com.atlassian.crowd.manager.property.PropertyManager;
import com.atlassian.crowd.manager.recovery.RecoveryModeAwareDirectoryManager;
import com.atlassian.crowd.manager.recovery.RecoveryModeDirectoryLoader;
import com.atlassian.crowd.manager.recovery.RecoveryModeService;
import com.atlassian.crowd.manager.threadlocal.CrowdThreadLocalStateAccessor;
import com.atlassian.crowd.manager.webhook.WebhookRegistry;
import com.atlassian.crowd.password.factory.PasswordEncoderFactory;
import com.atlassian.crowd.service.cluster.ClusterMessageService;
import com.atlassian.crowd.service.cluster.ClusterService;
import com.atlassian.crowd.service.license.LicenseService;
import com.atlassian.crowd.util.DirectorySynchronisationEventHelper;
import com.atlassian.hibernate.extras.ResetableHiLoGeneratorHelper;
import com.atlassian.plugin.refimpl.container.beans.RefappBeans;
import com.atlassian.plugin.refimpl.crowd.embedded.EmbeddedCrowdBootstrap;
import com.atlassian.plugin.refimpl.crowd.embedded.EmbeddedCrowdInitializer;
import com.atlassian.plugin.refimpl.crowd.noop.NoopAuditLogMapper;
import com.atlassian.plugin.refimpl.webresource.RequestCacheCleaner;
import com.atlassian.plugin.test.PluginJarBuilder;
import com.atlassian.refapp.api.ConnectionProvider;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.hibernate.SessionFactory;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.RestoreSystemProperties;
import org.junit.rules.MethodRule;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.vibur.dbcp.ViburDBCPDataSource;

import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.sql.DataSource;
import java.io.File;
import java.time.Clock;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Map;
import java.util.Vector;

import static org.apache.commons.io.FileUtils.deleteQuietly;
import static org.apache.commons.io.FileUtils.forceMkdir;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.sameInstance;
import static org.hamcrest.Matchers.startsWith;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.web.context.WebApplicationContext.ROOT_WEB_APPLICATION_CONTEXT_ATTRIBUTE;

/**
 * Integration tests for starting Refapp. Starts the Spring application context and the plugin system.
 * This test class covers the following distinct concerns of Refapp launch:
 * - test Plugin deployment;
 * - test Hibernate default configuration and initialisation;
 * - test Embedded Crowd beans initialisation.
 */
@RunWith(JUnitParamsRunner.class)
public class RefappLauncherIntegrationTest {

    @Rule
    public final MethodRule mockitoRule = MockitoJUnit.rule();

    @Rule
    public final RestoreSystemProperties restoreSystemProperties = new RestoreSystemProperties();

    private static final ServletContext servletContext = Mockito.mock(ServletContext.class);

    private static final File webappDirectory = new File("target/" + RefappLauncherIntegrationTest.class.getName());

    private static ConfigurableApplicationContext applicationContext;

    /**
     * Internal component of a test plugin created below, for inspecting the state of Refapp via OSGi.
     */
    public static class Helper {

        static final String PLUGIN_KEY = "test.helper";
    }

    @BeforeClass
    public static void setUp() throws Exception {
        final Enumeration<String> emptyEnumeration = new Vector<String>().elements();
        when(servletContext.getAttributeNames()).thenReturn(emptyEnumeration);
        when(servletContext.getInitParameterNames()).thenReturn(emptyEnumeration);
        System.setProperty("framework.bundles", "target/framework-bundles");
        forceMkdir(webappDirectory);
        when(servletContext.getRealPath(anyString())).thenAnswer(invocation -> {
            String path = invocation.getArgument(0);
            // Paths that aren't absolute won't work with Tomcat 8, so let's stop them now
            assertThat(path, startsWith("/"));
            return new File(webappDirectory, path).getPath();
        });
        final File pluginsDirectory = new File(webappDirectory, "/WEB-INF/plugins");
        forceMkdir(pluginsDirectory);
        new PluginJarBuilder("testHelper")
                .addFormattedResource("atlassian-plugin.xml",
                        "<atlassian-plugin name='TestHelper' key='" + Helper.PLUGIN_KEY + "' pluginsVersion='2'>",
                        "    <plugin-info>",
                        "        <version>1.0</version>",
                        "    </plugin-info>",
                        "    <component key='helper' class='" + Helper.class.getName() + "'/>",
                        "</atlassian-plugin>")
                .build(pluginsDirectory);
        applicationContext = createApplicationContext();
    }

    private static ConfigurableApplicationContext createApplicationContext() {
        final AnnotationConfigWebApplicationContext applicationContext = new AnnotationConfigWebApplicationContext();
        applicationContext.setServletContext(servletContext);
        applicationContext.register(RefappBeans.class);
        applicationContext.refresh();
        when(servletContext.getAttribute(ROOT_WEB_APPLICATION_CONTEXT_ATTRIBUTE)).thenReturn(applicationContext);
        return applicationContext;
    }

    @AfterClass
    public static void tearDown() {
        applicationContext.close();
        deleteQuietly(webappDirectory);
    }

    // The NPE in ComponentLocator.getComponent is because this test doesn't deploy the SAL plugin
    @Test
    public void requestCacheCleaner_whenInitialised_shouldObtainWebResourceIntegration() {
        // Arrange
        final RequestCacheCleaner requestCacheCleaner = spy(new RequestCacheCleaner());
        final FilterConfig filterConfig = mock(FilterConfig.class);
        when(filterConfig.getServletContext()).thenReturn(servletContext);
        when(servletContext.getAttribute(ROOT_WEB_APPLICATION_CONTEXT_ATTRIBUTE)).thenReturn(applicationContext);
        // Act
        requestCacheCleaner.init(filterConfig); // throws if bean lookup fails
        verify(requestCacheCleaner, times(1)).init(filterConfig);
    }

    @Test
    public void elementaryHibernateBeansAreInitialized() {
        final String errorMessage = "Bean not initialized";
        final ConnectionProvider connectionProvider = applicationContext.getBean(ConnectionProvider.class);
        final SessionFactory sessionFactory = applicationContext.getBean(SessionFactory.class);
        final HibernateTransactionManager hibernateTransactionManager =
                applicationContext.getBean(HibernateTransactionManager.class);
        final ResetableHiLoGeneratorHelper resetableHiLoGeneratorHelper =
                applicationContext.getBean(ResetableHiLoGeneratorHelper.class);
        assertThat(errorMessage, connectionProvider, notNullValue());
        assertThat(errorMessage, sessionFactory, notNullValue());
        assertThat(errorMessage, hibernateTransactionManager, notNullValue());
        assertThat(errorMessage, resetableHiLoGeneratorHelper, notNullValue());
    }

    @Test
    public void requiredHibernatePropertiesShouldBePresent() {
        final String errorMessage = "Property value different than expected";
        final SessionFactory sessionFactory = applicationContext.getBean(SessionFactory.class);
        final Map<String, Object> properties = sessionFactory.getProperties();
        assertThat(errorMessage, properties.get("hibernate.hbm2ddl.auto"), is("create-drop"));
        assertThat(errorMessage, properties.get("hibernate.connection.handling_mode"),
                is("DELAYED_ACQUISITION_AND_RELEASE_AFTER_TRANSACTION"));
    }

    @Test
    public void undesiredHibernatePropertiesShouldBeAbsent() {
        final String errorMessage = "Undesired property defined";
        final SessionFactory sessionFactory = applicationContext.getBean(SessionFactory.class);
        final Map<String, Object> properties = sessionFactory.getProperties();
        assertThat(errorMessage, properties.get("hibernate.connection.driver_class"), nullValue());
        assertThat(errorMessage, properties.get("hibernate.connection.url"), nullValue());
        assertThat(errorMessage, properties.get("hibernate.connection.username"), nullValue());
        assertThat(errorMessage, properties.get("hibernate.connection.password"), nullValue());
        assertThat(errorMessage, properties.get("hibernate.connection.autocommit"), nullValue());
        assertThat(errorMessage, properties.get("hibernate.connection.pool_size"), nullValue());
    }

    @Test
    public void refappDataSourceShouldBeUnique() {
        final String errorMessage = "Datasource different than expected";
        final SessionFactory sessionFactory = applicationContext.getBean(SessionFactory.class);
        final ConnectionProvider connectionProvider = applicationContext.getBean(ConnectionProvider.class);
        final DataSource dataSourceSessionFactory =
                (DataSource) sessionFactory.getProperties().get("hibernate.connection.datasource");
        final DataSource dataSourceConnectionProvider = connectionProvider.dataSource();
        assertThat(errorMessage, dataSourceSessionFactory.getClass(), equalTo(ViburDBCPDataSource.class));
        assertThat(errorMessage, dataSourceConnectionProvider.getClass(), equalTo(ViburDBCPDataSource.class));
        assertThat(errorMessage, dataSourceSessionFactory, sameInstance(dataSourceConnectionProvider));
    }

    @Test
    @Parameters(method = "getBeansForEmbeddedCrowd")
    public void embeddedCrowdBeansShouldBeInitialised(final Class<Object> clazz, final int instances) {
        Map<String, Object> beans = applicationContext.getBeansOfType(clazz);
        assertThat(String.format("Embedded Crowd bean missing: %s", beans), beans.size(), equalTo(instances));
    }

    private Iterable<Object[]> getBeansForEmbeddedCrowd() {
        return Arrays.asList(new Object[][]{
                //bean class, instances
                {EmbeddedCrowdInitializer.class, 1},
                {EmbeddedCrowdBootstrap.class, 1},
                {CrowdService.class, 1},
                {ApplicationService.class, 1},
                {CrowdDirectoryService.class, 1},
                {ClusterLockService.class, 1},
                {RecoveryModeService.class, 1},
                {AuditService.class, 1},
                {ClusterService.class, 1},
                {LicenseService.class, 1},
                {ApplicationManager.class, 1},
                {RecoveryModeAwareDirectoryManager.class, 1},
                {DirectoryPollerManager.class, 1},
                {PermissionManager.class, 1},
                {PropertyManager.class, 1},
                {CrowdDarkFeatureManager.class, 1},
                {InternalSynchronisationStatusManager.class, 1},
                {AuthenticationOrderOptimizer.class, 1},
                {InternalDirectoryInstanceLoader.class, 1},
                {RecoveryModeDirectoryLoader.class, 1},
                {AzureAdDirectoryInstanceLoader.class, 1},
                {CustomDirectoryInstanceLoader.class, 1},
                {LDAPDirectoryInstanceLoader.class, 1},
                {RemoteCrowdDirectoryInstanceLoader.class, 1},
                {DelegatingDirectoryInstanceLoader.class, 1},
                {DbCachingRemoteDirectoryInstanceLoader.class, 1},
                {DelegatedAuthenticationDirectoryInstanceLoader.class, 1},
                {CrowdEmbeddedApplicationFactory.class, 1},
                {CacheFactory.class, 1},
                {DirectoryCacheFactory.class, 1},
                {CacheRefresherFactory.class, 1},
                {DirectoryValidatorFactory.class, 1},
                {PasswordEncoderFactory.class, 1},
                {SearchStrategyFactory.class, 1},
                {AccessFilterFactory.class, 1},
                {DirectorySynchronisationTokenStore.class, 1},
                {DirectorySynchronisationInformationStore.class, 1},
                {TransactionalDirectoryDao.class, 1},
                {DirectorySynchroniser.class, 1},
                {DirectorySynchronisationEventHelper.class, 1},
                {ClusterMessageService.class, 1},
                {LdapDirectoryClearingClusterEventPublisher.class, 1},
                {NestedGroupsCacheProvider.class, 1},
                {BeforeGroupRemoval.class, 1},
                {DirectoryPollerJobRunner.class, 1},
                {AvatarProvider.class, 1},
                {WebhookRegistry.class, 1},
                {EventStore.class, 1},
                {AutoGroupAdderListener.class, 1},
                {DirectoryDefaultGroupMembershipResolver.class, 1},
                {AuditLogChangesetPopulator.class, 1},
                {AuditLogContext.class, 2},
                {NoopAuditLogMapper.class, 2},
                {AuditLogGroupMapper.class, 2},
                {AuditLogMetadataResolver.class, 1},
                {CrowdThreadLocalStateAccessor.class, 1},
                {AuditLogContextInternal.class, 1},
                {AuditLogEnabledChecker.class, 1},
                {AuditLogSystemProperties.class, 1},
                {AuditLogUserMapper.class, 3},
                {AuditLogDiffResultMapper.class, 1},
                {Encryptor.class, 1},
                {DirectoryPasswordsEncryptor.class, 1},
                {DcLicenseChecker.class, 1},
                {Clock.class, 1}
        });
    }
}
