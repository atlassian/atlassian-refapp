package com.atlassian.plugin.refimpl.plugins;

import com.atlassian.plugin.osgi.hostcomponents.ComponentRegistrar;
import com.atlassian.plugin.osgi.hostcomponents.InstanceBuilder;
import com.atlassian.plugin.osgi.hostcomponents.PropertyBuilder;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.springframework.context.ApplicationContext;

import java.util.Map;

import static java.util.Collections.singletonList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasEntry;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class RefappHostComponentProviderTest {

    private static final MyService MY_SERVICE = new MyService() {};

    @Rule
    public final MethodRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private ApplicationContext applicationContext;

    private interface MyService {}

    private RefappHostComponentProvider hostComponentProvider;

    @Before
    public void setUp() {
        hostComponentProvider = new RefappHostComponentProvider(singletonList(MyService.class));
        hostComponentProvider.setApplicationContext(applicationContext);
        when(applicationContext.getBean(MyService.class)).thenReturn(MY_SERVICE);
    }

    @Test
    public void provide_shouldRegisterMapAndMyServiceWithCorrectNames() {
        // Arrange
        final ComponentRegistrar registrar = mock(ComponentRegistrar.class);
        final InstanceBuilder instanceBuilder = mock(InstanceBuilder.class);
        final PropertyBuilder propertyBuilder = mock(PropertyBuilder.class);
        when(registrar.register(any())).thenReturn(instanceBuilder);
        when(instanceBuilder.forInstance(any())).thenReturn(propertyBuilder);

        // Act
        hostComponentProvider.provide(registrar);

        // Assert
        verify(registrar).register(Map.class);
        verify(registrar).register(MyService.class);
        verify(instanceBuilder).forInstance(any(Map.class));
        verify(instanceBuilder).forInstance(MY_SERVICE);
        verify(propertyBuilder).withName("map");
        verify(propertyBuilder).withName("myService");
    }
}
