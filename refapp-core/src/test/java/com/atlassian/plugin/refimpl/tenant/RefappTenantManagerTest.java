package com.atlassian.plugin.refimpl.tenant;


import com.atlassian.plugin.refimpl.tenant.RefappTenantManager.RefappTenant;
import com.atlassian.tenancy.api.Tenant;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.List;
import java.util.concurrent.Callable;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SuppressWarnings("deprecation")
public class RefappTenantManagerTest {

    private static final String TENANT_ID = "theTenantId";
    private static final String TENANT_NAME = "theTenantName";

    @Rule
    public final MethodRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private Callable<Object> callable;

    @Mock
    private RefappTenant tenant;

    @InjectMocks
    private RefappTenantManager tenantManager;

    @Before
    public void setUp() {
        when(tenant.getTenantId()).thenReturn(TENANT_ID);
        when(tenant.name()).thenReturn(TENANT_NAME);
    }

    // ------------------------------- TenantAccessor SPI -------------------------------

    @Test
    public void getAvailableTenants_shouldReturnJustTheFixedTenant() {
        // Act
        final Iterable<Tenant> tenants = tenantManager.getAvailableTenants();

        // Assert
        assertThat(tenants, contains(tenant));
    }

    @Test
    public void asTenant_shouldSimplyInvokeTheCallable() throws Exception {
        // Arrange
        final Tenant ignoredTenant = mock(Tenant.class);
        final Object result = mock(Object.class);
        when(callable.call()).thenReturn(result);

        // Act
        final Object actualResult = tenantManager.asTenant(ignoredTenant, callable);

        // Assert
        assertThat(actualResult, is(result));
    }

    // ------------------------------- TenantContext SPI -------------------------------

    @Test
    public void getCurrentTenant_shouldReturnJustTheFixedTenant() {
        // Act
        final Tenant currentTenant = tenantManager.getCurrentTenant();

        // Assert
        assertThat(currentTenant, is(tenant));
    }
}
