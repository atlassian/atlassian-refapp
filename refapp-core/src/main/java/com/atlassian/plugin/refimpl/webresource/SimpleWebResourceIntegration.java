package com.atlassian.plugin.refimpl.webresource;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.PluginEventListener;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.event.events.PluginDisabledEvent;
import com.atlassian.plugin.event.events.PluginEnabledEvent;
import com.atlassian.plugin.event.events.PluginModuleDisabledEvent;
import com.atlassian.plugin.event.events.PluginModuleEnabledEvent;
import com.atlassian.plugin.refimpl.ParameterUtils;
import com.atlassian.plugin.refimpl.feature.LimitedSystemPropertyDarkFeatureManager;
import com.atlassian.plugin.refimpl.version.HostVersion;
import com.atlassian.plugin.webresource.ResourceBatchingConfiguration;
import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.plugin.webresource.cdn.CDNStrategy;
import com.atlassian.sal.api.component.ComponentLocator;
import com.atlassian.sal.api.features.DarkFeatureManager;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.message.LocaleResolver;
import com.atlassian.webresource.api.assembler.resource.CompleteWebResourceKey;
import io.atlassian.util.concurrent.ResettableLazyReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import static java.util.Collections.emptyList;
import static java.util.Objects.requireNonNull;

/**
 * The {@link WebResourceIntegration} implementation.
 */
public class SimpleWebResourceIntegration implements WebResourceIntegration {

    private static final Logger log = LoggerFactory.getLogger(SimpleWebResourceIntegration.class);
    private final PluginAccessor pluginAccessor;

    /**
     * Calculates hash of all the plugins with i18n resources.
     */
    public static class I18nHasher {

        private final ResettableLazyReference<String> hashLazyReference;

        public I18nHasher(final PluginEventManager eventManager, final PluginAccessor pluginAccessor) {
            hashLazyReference = new ResettableLazyReference<String>() {
                @Override
                protected String create() {
                    // Tree Set produces consistent ordering and hashing.
                    final Set<String> versions = new TreeSet<>();
                    for (Plugin plugin : pluginAccessor.getEnabledPlugins()) {
                        // It would be better to count only plugins that actually contain i18n resources, but
                        // for simplicity counting all the plugins.
                        versions.add(plugin.getKey() + " v. " + plugin.getPluginInformation().getVersion());
                    }
                    return "" + versions.hashCode();
                }
            };
            eventManager.register(this);
        }

        public String get() {
            return hashLazyReference.get();
        }

        @PluginEventListener
        public void onPluginDisabled(final PluginDisabledEvent event) {
            hashLazyReference.reset();
        }

        @PluginEventListener
        public void onPluginEnabled(final PluginEnabledEvent event) {
            hashLazyReference.reset();
        }

        @PluginEventListener
        public void onPluginModuleEnabled(final PluginModuleEnabledEvent event) {
            hashLazyReference.reset();
        }

        @PluginEventListener
        public void onPluginModuleDisabled(final PluginModuleDisabledEvent event) {
            hashLazyReference.reset();
        }
    }

    private final DarkFeatureManager fallbackDarkFeatureManager;
    private final String systemBuildNumber;
    private final LocaleResolver localeResolver;
    private final HostVersion refappVersion;
    private final I18nResolver i18nResolver;
    private final PluginEventManager eventManager;
    // If it's null, we just create a new one. Though this means results from one request will affect the next request
    // on this same thread because we don't ever clean it up from a filter or anything - definitely not for use in
    // production!
    private final ThreadLocal<Map<String, Object>> requestCache = ThreadLocal.withInitial(HashMap::new);
    private final I18nHasher i18nHasher;
    private final RefAppResourceBatchingConfiguration batchingConfig;
    private final EventPublisher eventPublisher;

    public SimpleWebResourceIntegration(final LocaleResolver localeResolver, final I18nResolver i18nResolver,
                                        final PluginEventManager eventManager, final HostVersion refappVersion,
                                        final RefAppResourceBatchingConfiguration batchingConfig,
                                        final EventPublisher eventPublisher, final PluginAccessor pluginAccessor) {
        // we fake the build number by using the startup time which will force anything cached by clients to be
        // reloaded after a restart
        systemBuildNumber = String.valueOf(System.currentTimeMillis());
        fallbackDarkFeatureManager = new LimitedSystemPropertyDarkFeatureManager();
        this.localeResolver = localeResolver;
        this.i18nResolver = i18nResolver;
        this.eventManager = eventManager;
        this.refappVersion = requireNonNull(refappVersion);
        this.i18nHasher = new I18nHasher(eventManager, pluginAccessor);
        this.batchingConfig = batchingConfig;
        this.eventPublisher = eventPublisher;
        this.pluginAccessor = requireNonNull(pluginAccessor);
    }

    @Override
    public Locale getLocale() {
        return localeResolver.getLocale();
    }

    @Override
    public String getI18nText(final Locale locale, final String s) {
        return i18nResolver.getText(locale, s);
    }

    @Override
    public String getI18nRawText(final Locale locale, final String s) {
        return i18nResolver.getRawText(locale, s);
    }

    @Override
    public CDNStrategy getCDNStrategy() {
        return null;  // Null implies that a CDN is not used
    }

    public String getBaseUrl() {
        return getBaseUrl(UrlMode.AUTO);
    }

    public String getBaseUrl(UrlMode urlMode) {
        // The context path is read from a sys property and might be "/" context path-less instances.
        // for web resources this causes relative paths to be created as //blah so it needs to be replaced by ""
        String baseUrl = ParameterUtils.getBaseUrl(urlMode);
        return "/".equals(baseUrl) ? "" : baseUrl;
    }

    @Override
    public PluginAccessor getPluginAccessor() {
        return pluginAccessor;
    }

    @Override
    public Map<String, Object> getRequestCache() {
        return requestCache.get();
    }

    @Override
    public String getSystemBuildNumber() {
        return systemBuildNumber;
    }

    @Override
    public String getSystemCounter() {
        return "1";
    }

    @Override
    public String getSuperBatchVersion() {
        return "1";
    }

    @Override
    public String getStaticResourceLocale() {
        log.warn("`getStaticResourceLocale` is deprecated and not used anymore!");
        return getLocale().toString();
    }

    @Override
    public String getI18nStateHash() {
        return i18nHasher.get();
    }

    @Override
    public File getTemporaryDirectory() {
        final String tempDir = System.getProperty("java.io.tmpdir");
        return new File(tempDir);
    }

    /**
     * Implementation Note: the WRM should implement this method on the {@link ResourceBatchingConfiguration} interface instead of
     * {@link WebResourceIntegration}, That's why I've implemented it on {@link RefAppResourceBatchingConfiguration}
     * and I'm pulling it in here; to predict that future where all the batching stuff is on the batching interface ;)
     */
    @Override
    public List<CompleteWebResourceKey> getSyncWebResourceKeys() {
        return batchingConfig != null && batchingConfig.isSyncBatchingEnabled() ?
                batchingConfig.getSyncBatchModuleCompleteKeys() :
                emptyList();
    }

    @Override
    @Nonnull
    public DarkFeatureManager getDarkFeatureManager() {
        try {
            return ComponentLocator.isInitialized() ?
                    ComponentLocator.getComponent(DarkFeatureManager.class) :
                    fallbackDarkFeatureManager;
        } catch (Exception e) {
            log.info("Falling back to an alternative DarkFeatureManager. Failed to import a DarkFeatureManager component", e);
            return fallbackDarkFeatureManager;
        }
    }

    @Override
    @Nonnull
    public EventPublisher getEventPublisher() {
        return eventPublisher;
    }

    @Override
    public PluginEventManager getPluginEventManager() {
        return this.eventManager;
    }

    @Override
    public Set<String> allowedCondition1Keys() {
        return null;
    }

    @Override
    public Set<String> allowedTransform1Keys() {
        return null;
    }

    @Override
    public boolean forbidCondition1AndTransformer1() {
        return false;
    }

    @Override
    public String getHostApplicationVersion() {
        return refappVersion.getValue();
    }

    @Override
    public Iterable<Locale> getSupportedLocales() {
        return localeResolver.getSupportedLocales();
    }
}
