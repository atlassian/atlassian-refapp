package com.atlassian.plugin.refimpl.servlet;

import com.atlassian.plugin.refimpl.file.FileUtils;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.servlet.ServletContext;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Utility methods relating to the {@link javax.servlet.ServletContext}.
 *
 * @since 5.4 moved from ContainerManager (now RefappLauncher)
 */
@ParametersAreNonnullByDefault
public final class ServletContextUtils {

    /**
     * The directory containing all bundled plugins.
     */
    private static final String BUNDLED_PLUGINS_DIR = "/WEB-INF/atlassian-bundled-plugins";

    private ServletContextUtils() {}

    /**
     * Ensures the existence of a directory, as follows:
     * <ol>
     *     <li>if the given system property has a value, that's the directory</li>
     *     <li>otherwise, the directory is the given default, relative to the given servlet context</li>
     * </ol>
     * @param servletContext the servlet context
     * @param sysPropName the name of the system property
     * @param defaultPath the default path
     * @return the directory
     * @see ServletContext#getRealPath(String)
     */
    @Nonnull
    public static File findAndCreateDirectory(
            final ServletContext servletContext, final String sysPropName, final String defaultPath) {
        final File dir;
        if (System.getProperty(sysPropName) != null) {
            dir = FileUtils.makeSureDirectoryExists(System.getProperty(sysPropName));
        } else {
            dir = makeSureDirectoryExists(servletContext, defaultPath);
        }
        return dir;
    }

    /**
     * Ensures the existence of the given directory, relative to the given servlet context.
     *
     * @param servletContext the servlet context
     * @param relativePath the relative path
     * @return the directory
     */
    @Nonnull
    public static File makeSureDirectoryExists(final ServletContext servletContext, final String relativePath) {
        return FileUtils.makeSureDirectoryExists(servletContext.getRealPath(relativePath));
    }

    /**
     * Returns the URL of the directory in which to install bundled plugins.
     *
     * @param servletContext the servlet context
     * @return see description
     */
    @Nonnull
    public static URL getBundledPluginDir(final ServletContext servletContext) {
        final File bundledPluginDir = makeSureDirectoryExists(servletContext, BUNDLED_PLUGINS_DIR);
        final URL bundledPluginUrl;
        try {
            bundledPluginUrl = bundledPluginDir.toURI().toURL();
        } catch (MalformedURLException e) {
            // This is unexpected, as we know the path to be valid
            throw new IllegalStateException("Can't form url to bundled plugins directory at: " + BUNDLED_PLUGINS_DIR, e);
        }
        return bundledPluginUrl;
    }
}
