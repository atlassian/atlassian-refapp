package com.atlassian.plugin.refimpl.filter;

import com.atlassian.webresource.api.assembler.PageBuilderService;
import com.opensymphony.module.sitemesh.HTMLPage;
import com.opensymphony.module.sitemesh.Page;
import org.slf4j.Logger;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;

import javax.annotation.Nonnull;
import javax.servlet.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Optional;

import static com.atlassian.plugin.refimpl.container.BeanLocator.getBean;
import static com.atlassian.webresource.api.UrlMode.RELATIVE;
import static com.opensymphony.module.sitemesh.RequestConstants.PAGE;
import static java.util.Objects.requireNonNull;
import static java.util.Optional.empty;
import static org.slf4j.LoggerFactory.getLogger;

/**
 * This is applied in a recovery setting, so we can figure out what went wrong, don't depend on what we don't need.
 * <p>
 * Probably can't get away with less than
 * - Spring, WRM, Seraph (because all the useful UIs use these)
 * - Sitemesh, Servlets, Tomcat, Seraph (because we're rendering a webpage)
 * All of that should be in core anyway without OSGi service dependencies on plugins, right?
 */
public class DecoratorFallback implements Filter {
    private static final Logger log = getLogger(DecoratorFallback.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        chain.doFilter(request, response);

        if (request.getAttribute("__atl_decorated") == null) {
            response.setContentType("text/html");

            Optional<Page> optionalPage = Optional.ofNullable(request.getAttribute(PAGE))
                    .filter(object -> object instanceof Page)
                    .map(object -> (Page) object);
            Optional<HTMLPage> optionalHTMLPage = optionalPage
                    .filter(page -> page instanceof HTMLPage)
                    .map(page -> (HTMLPage) page);

            Optional<PageBuilderService> optionalPageBuilderService = empty();
            try {
                optionalPageBuilderService = Optional.of(getBean(PageBuilderService.class, request.getServletContext()));
            } catch (NoSuchBeanDefinitionException exception) {
                log.error("Couldn't get a PageBuilderService", exception);
            }

            // With the Sitemesh 3 upgrade this could be simplified to using the sitemesh tags
            final PrintWriter writer = response.getWriter();
            writer.println("<html>");
            writer.println("  <head>");
            optionalPage.ifPresent(page -> {
                writer.print("<title>");
                writer.print(page.getTitle());
                writer.println("</title>");
            });
            optionalHTMLPage.map(HTMLPage::getHead).ifPresent(writer::println);
            writer.println("  </head>");
            writer.println("  <body>");
            writer.println("    <p style=\"color:red;font-size:200%\">Something broke the decorators!</p>");
            optionalPage.map(Page::getBody).ifPresent(writer::println);
            writer.println("  </body>");
            writer.println("  <footer>");
            includeWebResources(optionalPageBuilderService, writer);
            writer.println("  </footer>");
            writer.println("</html>");
        }
    }

    @Override
    public void destroy() {
    }

    private static void includeWebResources(@Nonnull Optional<PageBuilderService> optionalPageBuilderService, @Nonnull PrintWriter responseWriter) {
        requireNonNull(optionalPageBuilderService, "optionalPageBuilderService");
        requireNonNull(responseWriter, "responseWriter");

        optionalPageBuilderService.ifPresent(pageBuilderService -> pageBuilderService
                .assembler()
                .assembled()
                .drainIncludedResources()
                .writeHtmlTags(responseWriter, RELATIVE));
    }
}
