package com.atlassian.plugin.refimpl.lifecycle;

import com.atlassian.scheduler.SchedulerServiceException;
import com.atlassian.scheduler.core.SchedulerServiceController;

import static java.util.Objects.requireNonNull;

/**
 * Controls the lifecycle of the Atlassian scheduler.
 *
 * @since 6.1
 */
public class SchedulerLifecycle extends AbstractSmartLifecycle {

    private final SchedulerServiceController schedulerServiceController;

    public SchedulerLifecycle(final SchedulerServiceController schedulerServiceController) {
        super(LifecycleOrder.THIRD);
        this.schedulerServiceController = requireNonNull(schedulerServiceController);
    }

    @Override
    protected void doStart() {
        try {
            schedulerServiceController.start();
        } catch (final SchedulerServiceException e) {
            throw new IllegalStateException("Failed to start scheduler", e);
        }
    }

    @Override
    protected void doStop() {
        schedulerServiceController.shutdown();
    }
}
