package com.atlassian.plugin.refimpl.plugins;

import com.atlassian.plugin.osgi.container.PackageScannerConfiguration;
import com.atlassian.plugin.osgi.container.impl.DefaultPackageScannerConfiguration;
import com.atlassian.plugin.refimpl.ConfigParser;
import com.atlassian.plugin.refimpl.SystemExportVersionUtils;
import com.atlassian.plugin.refimpl.version.HostVersion;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.servlet.ServletContext;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The {@link com.atlassian.plugin.osgi.container.PackageScannerConfiguration} for Refapp.
 *
 * @since 5.4 code moved from ContainerManager (now RefappLauncher)
 */
@ParametersAreNonnullByDefault
public final class PackageScannerConfigurationFactory {

    /**
     * The system property key for overriding package export versions.
     */
    private static final String PACKAGE_VERSION_EXPORT_OVERRIDES = "refapp.packageExport.overrides";

    /**
     * Factory method.
     *
     * @param servletContext the servlet context
     * @param hostVersion the host version
     * @return a new instance
     */
    @Nonnull
    public static PackageScannerConfiguration getPackageScannerConfiguration(
            final ServletContext servletContext, final HostVersion hostVersion) {
        final DefaultPackageScannerConfiguration scannerConfig =
                new DefaultPackageScannerConfiguration(hostVersion.getValue());
        scannerConfig.setServletContext(servletContext);

        final List<String> packageExcludes = new ArrayList<>(scannerConfig.getPackageExcludes());
        packageExcludes.add("com.atlassian.healthcheck.spi.impl");
        scannerConfig.setPackageExcludes(packageExcludes);

        ArrayList<String> packageIncludes = new ArrayList<>(scannerConfig.getPackageIncludes());
        packageIncludes.add("org.json");
        packageIncludes.add("com.rometools.rome.*");
        scannerConfig.setPackageIncludes(packageIncludes);

        final Map<String, String> packageVersions = new HashMap<>();
        packageVersions.put("com.google.common.*", SystemExportVersionUtils.getGoogleGuavaVersion());
        // org.w3c.dom.html comes from both xercesImpl and xml-apis. To avoid package scanner non-deterministically guessing a
        // version, we force the version here. Following JIRA's lead, we'll version the whole package to nothing.
        packageVersions.put("org.w3c.*", "");
        // javax.annotation is present both in WEB-INF/lib/jsr305-1.3.9.jar and tomcat's lib/annotations-api.jar, and there's also
        // a bit in ${JAVA_HOME}/jre/lib/rt.jar. As above, we specify a blank version. In fact this version is changed to 1.0.0 by
        // the plugin system *after* scanning, but we still wish to avoid the warning. We do just javax.annotation, and no
        // subpackages, because there's no clash amongst the subpackages.
        packageVersions.put("javax.annotation", "");
        final String packageVersionExportOverrides = System.getProperty(PACKAGE_VERSION_EXPORT_OVERRIDES);
        if (packageVersionExportOverrides != null) {
            packageVersions.putAll(ConfigParser.parseMap(packageVersionExportOverrides));
        }
        scannerConfig.setPackageVersions(packageVersions);

        // DMZ. Allow this plugin to access all internal packages e.g. Jackson 2.x components.
        scannerConfig.getApplicationBundledInternalPlugins().add("com.internal.plugin.dmz-internal-plugin-2");

        return scannerConfig;
    }

    private PackageScannerConfigurationFactory() {}
}
