package com.atlassian.plugin.refimpl.profiling;

import com.atlassian.util.profiling.ProfilerConfiguration;
import com.atlassian.util.profiling.Timers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;

import static java.util.Objects.requireNonNull;
import static java.util.concurrent.TimeUnit.MILLISECONDS;

/**
 * Globally configures Atlassian Profiling
 *
 * @since 5.4.0
 */
public class ProfilingService {
    private static final Logger log = LoggerFactory.getLogger(ProfilingService.class);

    private static final long MIN_TIME_FRAME = 0L;
    private static final long MIN_TRACE_TIME = 0L;

    private final ProfilingConfigProvider profilingConfigProvider;

    public ProfilingService(final ProfilingConfigProvider profilingConfigProvider) {
        this.profilingConfigProvider = requireNonNull(profilingConfigProvider);
    }

    @PostConstruct
    public void onStart() {
        if (profilingConfigProvider.shouldAtlassianProfilingBeEnabled()) {
            ProfilerConfiguration configuration = Timers.getConfiguration();
            configuration.setEnabled(true);
            configuration.setMinFrameTime(MIN_TIME_FRAME, MILLISECONDS);
            configuration.setMinTraceTime(MIN_TRACE_TIME, MILLISECONDS);
            log.info("Atlassian Profiling started");
        }
    }
}
