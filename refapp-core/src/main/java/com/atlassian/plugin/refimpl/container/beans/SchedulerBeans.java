package com.atlassian.plugin.refimpl.container.beans;

import com.atlassian.plugin.refimpl.scheduler.CaesiumConfig;
import com.atlassian.scheduler.SchedulerHistoryService;
import com.atlassian.scheduler.caesium.impl.CaesiumSchedulerService;
import com.atlassian.scheduler.caesium.impl.MemoryClusteredJobDao;
import com.atlassian.scheduler.caesium.spi.CaesiumSchedulerConfiguration;
import com.atlassian.scheduler.caesium.spi.ClusteredJobDao;
import com.atlassian.scheduler.core.DefaultSchedulerHistoryService;
import com.atlassian.scheduler.core.LifecycleAwareSchedulerService;
import com.atlassian.scheduler.core.impl.MemoryRunDetailsDao;
import com.atlassian.scheduler.core.spi.RunDetailsDao;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Spring beans relating to job scheduling.
 *
 * @since 5.4
 */
@Configuration
public class SchedulerBeans {

    @Bean
    public ClusteredJobDao clusteredJobDao() {
        return new MemoryClusteredJobDao();
    }

    @Bean
    public RunDetailsDao runDetailsDao() {
        return new MemoryRunDetailsDao();
    }

    @Bean
    public CaesiumSchedulerConfiguration schedulerConfiguration() {
        return new CaesiumConfig();
    }

    @Bean
    public SchedulerHistoryService schedulerHistoryService(final RunDetailsDao runDetailsDao) {
        return new DefaultSchedulerHistoryService(runDetailsDao);
    }

    @Bean(destroyMethod = "")  // RefappLauncher calls shutdown programmatically to ensure correct order
    public LifecycleAwareSchedulerService schedulerService(
            final CaesiumSchedulerConfiguration config,
            final RunDetailsDao runDetailsDao,
            final ClusteredJobDao clusteredJobDao) {
        return new CaesiumSchedulerService(config, runDetailsDao, clusteredJobDao);
    }
}
