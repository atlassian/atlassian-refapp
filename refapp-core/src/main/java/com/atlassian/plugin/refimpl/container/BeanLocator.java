package com.atlassian.plugin.refimpl.container;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.ServletContext;
import java.util.Collection;

import static org.springframework.web.context.support.WebApplicationContextUtils.getRequiredWebApplicationContext;

/**
 * Provides lookups of Refapp components for callers that don't support dependency injection, such as JSPs, servlet
 * filters, etc.
 *
 * @since 5.4
 */
public final class BeanLocator {

    private BeanLocator() {}

    /**
     * Returns all beans of the given type, from the Spring container associated with the given servlet context.
     *
     * @param beanClass the bean class
     * @param servletContext the servlet context
     * @param <T> the bean type
     * @return see description
     * @see WebApplicationContext#getBeansOfType(Class)
     */
    public static <T> Collection<T> getBeans(final Class<T> beanClass, final ServletContext servletContext) {
        final WebApplicationContext applicationContext = getRequiredWebApplicationContext(servletContext);
        return applicationContext.getBeansOfType(beanClass).values();
    }

    /**
     * Returns any bean of the given type, from the Spring container associated with the given servlet context.
     * Only use this when you don't care which of multiple beans are returned, for example when you know that any of
     * them will suit your purpose or are actually the same object under the covers.
     *
     * @param beanClass the bean class
     * @param servletContext the servlet context
     * @param <T> the bean type
     * @return the requested bean
     */
    public static <T> T getAnyBean(final Class<T> beanClass, final ServletContext servletContext) {
        final ApplicationContext applicationContext = getRequiredWebApplicationContext(servletContext);
        final Collection<T> beans = applicationContext.getBeansOfType(beanClass).values();
        if (beans.isEmpty()) {
            throw new NoSuchBeanDefinitionException(beanClass);
        }
        return beans.iterator().next();
    }

    /**
     * Returns the one bean of the given type, from the Spring container associated with the given servlet context.
     *
     * @param beanClass the bean class
     * @param servletContext the servlet context
     * @param <T> the bean type
     * @return the requested bean
     * @throws org.springframework.beans.factory.NoSuchBeanDefinitionException if there's no such bean
     * @throws org.springframework.beans.factory.NoUniqueBeanDefinitionException if there's multiple
     * @see org.springframework.context.ApplicationContext#getBean(Class)
     */
    public static <T> T getBean(final Class<T> beanClass, final ServletContext servletContext) {
        final BeanFactory beanFactory = getRequiredWebApplicationContext(servletContext);
        return beanFactory.getBean(beanClass);
    }
}
