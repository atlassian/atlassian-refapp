package com.atlassian.plugin.refimpl.crowd.license;

import com.atlassian.crowd.service.license.LicenseService;
import com.atlassian.extras.api.crowd.CrowdLicense;

/**
 * A mock implementation of the {@link LicenseService} interface for Crowd.
 */
public class LicenseServiceImpl implements LicenseService {
    @Override
    public CrowdLicense getLicense() {
        throw new UnsupportedOperationException();
    }
}
