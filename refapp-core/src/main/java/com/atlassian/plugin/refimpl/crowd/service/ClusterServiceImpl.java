package com.atlassian.plugin.refimpl.crowd.service;

import com.atlassian.crowd.service.cluster.ClusterInformation;
import com.atlassian.crowd.service.cluster.ClusterNode;
import com.atlassian.crowd.service.cluster.ClusterService;

import javax.annotation.Nonnull;
import java.util.Optional;

/**
 * A mock implementation of the {@link ClusterService} interface for Crowd.
 */
public class ClusterServiceImpl implements ClusterService {
    @Override
    public boolean isAvailable() {
        return false;
    }

    @Nonnull
    @Override
    public String getNodeId() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Optional<ClusterNode> getClusterNode() {
        return Optional.empty();
    }

    @Nonnull
    @Override
    public ClusterInformation getInformation() {
        throw new UnsupportedOperationException();
    }
}
