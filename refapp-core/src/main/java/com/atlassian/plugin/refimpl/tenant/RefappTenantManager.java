package com.atlassian.plugin.refimpl.tenant;

import com.atlassian.annotations.VisibleForTesting;
import com.atlassian.tenancy.api.Tenant;
import com.atlassian.tenancy.api.TenantAccessor;
import com.atlassian.tenancy.api.TenantContext;

import javax.annotation.Nonnull;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import static com.atlassian.plugin.refimpl.ParameterUtils.getBaseUrl;
import static com.atlassian.plugin.webresource.UrlMode.ABSOLUTE;
import static java.util.Collections.singletonList;
import static java.util.Objects.requireNonNull;

/**
 * An all-in-one class for Refapp's tenancy concerns.
 *
 * Refapp is not multi-tenanted, but it exports various tenancy-related services to OSGi. It's unknown whether these
 * are used anywhere. It's possible they are required by one or more platform APIs that still have the concept of
 * tenancy.
 *
 * @since 5.4 replaced several classes
 */
public class RefappTenantManager implements TenantAccessor, TenantContext {

    /**
     * Factory method.
     *
     * @return a new instance
     */
    public static RefappTenantManager getInstance() {
        return new RefappTenantManager(new RefappTenant(getTenantIdFromBaseUrl()));
    }

    private static String getTenantIdFromBaseUrl() {
        final String baseUrl = getBaseUrl(ABSOLUTE);
        try {
            final URL url = new URL(baseUrl);
            return url.getAuthority() + url.getPath(); // e.g. "localhost:5990/refapp"
        } catch (final MalformedURLException e) {
            return baseUrl;
        }
    }

    private final RefappTenant tenant;

    /**
     * Constructor.
     *
     * @param tenant the tenant
     */
    @VisibleForTesting
    RefappTenantManager(final RefappTenant tenant) {
        this.tenant = requireNonNull(tenant);
    }

    // ------------------------------- TenantAccessor SPI -------------------------------

    @Override
    public Iterable<Tenant> getAvailableTenants() {
        return singletonList(tenant);
    }

    @Override
    public <T> T asTenant(final Tenant ignored, final Callable<T> callable) throws InvocationTargetException {
        try {
            return callable.call();
        } catch (Exception e) {
            throw new InvocationTargetException(e);
        }
    }

    // ------------------------------- TenantContext SPI -------------------------------

    @Nonnull
    @Override
    public Tenant getCurrentTenant() {
        return tenant;
    }

    // ------------------------------- Tenant implementation -------------------------------

    @VisibleForTesting
    static class RefappTenant implements Tenant {

        private final String tenantId;

        private RefappTenant(final String tenantId) {
            this.tenantId = requireNonNull(tenantId);
        }

        String getTenantId() {
            return tenantId;
        }

        @Override
        public String name() {
            return "RefApp tenant";
        }
    }
}
