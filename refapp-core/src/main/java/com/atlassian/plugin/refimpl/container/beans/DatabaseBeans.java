package com.atlassian.plugin.refimpl.container.beans;

import com.atlassian.plugin.refimpl.db.ConnectionProviderImpl;
import com.atlassian.refapp.api.ConnectionProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.sql.SQLException;

/**
 * Spring beans relating to the Refapp database.
 *
 * @since 5.4
 */
@Configuration
public class DatabaseBeans {

    @Bean
    public ConnectionProvider connectionProvider() throws SQLException {
        // Spring will call close() automatically
        return new ConnectionProviderImpl();
    }
}
