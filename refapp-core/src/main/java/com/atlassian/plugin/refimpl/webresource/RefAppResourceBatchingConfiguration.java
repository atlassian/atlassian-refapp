package com.atlassian.plugin.refimpl.webresource;

import com.atlassian.plugin.webresource.DefaultResourceBatchingConfiguration;
import com.atlassian.plugin.webresource.ResourceBatchingConfiguration;
import com.atlassian.webresource.api.assembler.resource.CompleteWebResourceKey;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import static java.util.stream.Collectors.toList;

/**
 * Config class for batching that detects if the refapp should use specific batching strategies or not.
 * Resources that need to be super-batched are hardcoded within the class.
 *
 * Note that some batching configurations may not apply until a plugin lifecycle event occurs, as a consequence
 * of how the WRM implements its caching for web-resource configuration and discovery.
 */
public class RefAppResourceBatchingConfiguration extends DefaultResourceBatchingConfiguration
        implements ResourceBatchingConfiguration {

    private static final Logger log = LoggerFactory.getLogger(RefAppResourceBatchingConfiguration.class);

    /** set by quickreload */
    private static final String WEBRESOURCE_BATCHING_OFF_KEY = "plugin.webresource.batching.off";
    /** typically set by AMPS when running in debug mode */
    private static final String ATL_DEV_MODE_KEY = "atlassian.dev.mode";
    /** allow forcefully enabling sync batch. */
    private static final String REFAPP_SYNC_BATCHING_ON_KEY = "refapp.sync.batching.on";
    /** allow forcefully enabling superbatch. */
    private static final String REFAPP_SUPER_BATCHING_ON_KEY = "refapp.super.batching.on";
    /** allow forcefully enabling context batching. */
    private static final String REFAPP_CONTEXT_BATCHING_ON_KEY = "refapp.context.batching.on";
    /** allow forcefully enabling webresource batching. */
    private static final String REFAPP_WEBRESOURCE_BATCHING_ON_KEY = "refapp.webresource.batching.on";

    /**
     * Keep a reference to the superbatch resource defined in refapp's atlassian-plugin.xml, just in case
     * a developer chooses to empty the {@link #refappSuperbatchResources} at some point during runtime
     * and wants to bring it back.
     */
    public static final String REFAPP_DEFAULT_SUPERBATCH_RESOURCE = "com.atlassian.refapp.decorator:superbatch-resources";

    /**
     * Allow developers to alter the refapp's superbatch resources at runtime.
     */
    private final List<String> refappSuperbatchResources = new CopyOnWriteArrayList<>();

    /**
     * Allow developers to alter the refapp's syncbatch resources at runtime.
     */
    private final List<CompleteWebResourceKey> refappSyncbatchResources = new CopyOnWriteArrayList<>();

    public RefAppResourceBatchingConfiguration() {
        this.refappSuperbatchResources.add(REFAPP_DEFAULT_SUPERBATCH_RESOURCE);
    }

    @Override
    public List<String> getSuperBatchModuleCompleteKeys() {
        return ImmutableList.copyOf(refappSuperbatchResources);
    }

    public List<CompleteWebResourceKey> getSyncBatchModuleCompleteKeys() {
        return ImmutableList.copyOf(refappSyncbatchResources);
    }

    /**
     * Add one or more web-resource keys to the superbatch.
     *
     * Visible to allow developers to test WRM's batching behaviours, either manually via a debug session
     * or via automated tests.
     *
     * Implementation Note:  You may need to trigger a plugin lifecycle event in order for the change to take effect.
     * @param keys complete module keys of the web-resource(s) to add to the superbatch.
     */
    @VisibleForTesting
    public void addToSuperBatch(final String... keys) {
        refappSuperbatchResources.addAll(Arrays.asList(keys));
    }

    /**
     * Remove one or more web-resource keys from the superbatch.
     *
     * Visible to allow developers to test WRM's batching behaviours, either manually via a debug session
     * or via automated tests.
     *
     * Implementation Note:  You may need to trigger a plugin lifecycle event in order for the change to take effect.
     * @param keys complete module keys of the web-resource(s) to remove from the superbatch.
     */
    @VisibleForTesting
    public void removeFromSuperBatch(final String... keys) {
        refappSuperbatchResources.removeAll(Arrays.asList(keys));
    }

    /**
     * Replace all resources in the superbatch with the ones provided.
     *
     * Visible to allow developers to test WRM's batching behaviours, either manually via a debug session
     * or via automated tests.
     *
     * Implementation Note:  You may need to trigger a plugin lifecycle event in order for the change to take effect.
     * @param keys complete module keys of the web-resource(s) to add to the superbatch.
     */
    @VisibleForTesting
    public void setSuperBatchResources(final String... keys) {
        refappSuperbatchResources.clear();
        addToSuperBatch(keys);
    }

    /**
     * Add one or more web-resource keys to the sync batch. These resources will be inlined in the page's head.
     *
     * Visible to allow developers to test WRM's batching behaviours, either manually via a debug session
     * or via automated tests.
     *
     * Implementation Note:  You may need to trigger a plugin lifecycle event in order for the change to take effect.
     * @param keys complete module keys of the web-resource(s) to add to the sync batch.
     */
    @VisibleForTesting
    public void addToSyncBatch(final String... keys) {
        refappSyncbatchResources.addAll(Arrays.stream(keys).map(this::toWrKey).collect(toList()));
    }

    /**
     * Remove one or more web-resource keys from the sync batch.
     *
     * Visible to allow developers to test WRM's batching behaviours, either manually via a debug session
     * or via automated tests.
     *
     * Implementation Note:  You may need to trigger a plugin lifecycle event in order for the change to take effect.
     * @param keys complete module keys of the web-resource(s) to remove from the sync batch.
     */
    @VisibleForTesting
    public void removeFromSyncBatch(final String... keys) {
        refappSyncbatchResources.removeAll(Arrays.stream(keys).map(this::toWrKey).collect(toList()));
    }

    /**
     * Replace all web-resources in the sync batch with the ones provided.
     *
     * Visible to allow developers to test WRM's batching behaviours, either manually via a debug session
     * or via automated tests.
     *
     * Implementation Note:  You may need to trigger a plugin lifecycle event in order for the change to take effect.
     * @param keys complete module keys of the web-resource(s) to add to the sync batch.
     */
    @VisibleForTesting
    public void setSyncBatchResources(final String... keys) {
        refappSyncbatchResources.clear();
        addToSyncBatch(keys);
    }

    /**
     * @return if {@link #REFAPP_SUPER_BATCHING_ON_KEY} is explicitly set, will return true or false based
     *         on its value. Otherwise, will check whether batching should be disabled and return true if nothing
     *         is explicitly stopping batching from happening.
     */
    @Override
    public boolean isSuperBatchingEnabled() {
        boolean result = isExplicitlyEnabledWithGenericBatchingFallback(REFAPP_SUPER_BATCHING_ON_KEY);

        log.debug("isSuperBatchingEnabled: {}", result);

        return result;
    }

    /**
     * @return if {@link #REFAPP_SYNC_BATCHING_ON_KEY} is explicitly set, will return true or false based
     *         on its value. Otherwise, will check whether batching should be disabled and return true if nothing
     *         is explicitly stopping batching from happening.
     */
    public boolean isSyncBatchingEnabled() {
        boolean result = isExplicitlyEnabledWithGenericBatchingFallback(REFAPP_SYNC_BATCHING_ON_KEY);

        log.debug("isSyncBatchingEnabled: {}", result);

        return result;
    }

    /**
     * @return if {@link #REFAPP_CONTEXT_BATCHING_ON_KEY} is explicitly set, will return true or false based
     *         on its value. Otherwise, will check whether batching should be disabled and return true if nothing
     *         is explicitly stopping batching from happening.
     */
    @Override
    public boolean isContextBatchingEnabled() {
        boolean result = isExplicitlyEnabledWithGenericBatchingFallback(REFAPP_CONTEXT_BATCHING_ON_KEY);

        log.debug("isContextBatchingEnabled: {}", result);

        return result;
    }

    /**
     * @return if {@link #REFAPP_WEBRESOURCE_BATCHING_ON_KEY} is explicitly set, will return true or false based
     *         on its value. Otherwise, will check whether batching should be disabled and return true if nothing
     *         is explicitly stopping batching from happening.
     */
    @Override
    public boolean isPluginWebResourceBatchingEnabled() {
        boolean result = isExplicitlyEnabledWithGenericBatchingFallback(REFAPP_WEBRESOURCE_BATCHING_ON_KEY);

        log.debug("isPluginWebResourceBatchingEnabled: {}", result);

        return result;
    }

    /**
     * @param key the system property to check. Will be treated as a boolean value.
     * @return if the key exists in system properties, returns result of {@link #isOn(String)} for the key.
     *         otherwise returns true if {@link #shouldBatchingBeStopped()} is false.
     */
    public boolean isExplicitlyEnabledWithGenericBatchingFallback(final String key) {
        if (System.getProperties().containsKey(key)) {
            return isOn(key);
        }
        return !shouldBatchingBeStopped();
    }

    /**
     * A kill-switch for batching.
     * @return true if {@link #WEBRESOURCE_BATCHING_OFF_KEY} is set to "true" or if {@link #isDevMode()} is true.
     */
    public boolean shouldBatchingBeStopped() {
        return isOn(WEBRESOURCE_BATCHING_OFF_KEY) || isDevMode();
    }

    /**
     * @return `true` if the {@link #ATL_DEV_MODE_KEY} system property is set to true, otherwise `false`.
     */
    public boolean isDevMode() {
        return isOn(ATL_DEV_MODE_KEY);
    }

    /**
     * @return `true` if the system property value for the provided key is the string "true" ignoring case,
     *         otherwise `false`.
     */
    public boolean isOn(final String key) {
        return Boolean.getBoolean(key);
    }

    private CompleteWebResourceKey toWrKey(String key) {
        String[] parts = key.split(":");
        return new CompleteWebResourceKey(parts[0], parts[1]);
    }
}
