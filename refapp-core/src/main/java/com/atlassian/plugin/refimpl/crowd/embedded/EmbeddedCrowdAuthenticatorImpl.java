package com.atlassian.plugin.refimpl.crowd.embedded;

import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.crowd.exception.CrowdException;
import com.atlassian.refapp.api.EmbeddedCrowdAuthenticator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.security.Principal;

public class EmbeddedCrowdAuthenticatorImpl implements EmbeddedCrowdAuthenticator {
    private static final Logger LOG = LoggerFactory.getLogger(EmbeddedCrowdAuthenticatorImpl.class);

    @Inject
    private CrowdService crowdService;

    @Override
    public User authenticateUser(final String username, final String password) {
        LOG.info("About to authenticate user: {}", username);
        try {
            return this.crowdService.authenticate(username, password);
        } catch (final CrowdException e) {
            LOG.error("Problems during authentication.", e);
            return null;
        }
    }

    @Override
    public User findUserByName(final String username) {
        LOG.info("About to find user by name: {}", username);
        return this.crowdService.getUser(username);
    }
}
