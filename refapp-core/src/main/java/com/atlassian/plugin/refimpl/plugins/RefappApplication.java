package com.atlassian.plugin.refimpl.plugins;

import com.atlassian.plugin.Application;

import javax.annotation.Nonnull;

/**
 * The {@link Application} for Refapp.
 *
 * @since 5.4 moved from ContainerManager (now RefappLauncher)
 */
public class RefappApplication implements Application {

    private static final Application INSTANCE = new RefappApplication();

    /**
     * Returns the {@link Application} for Refapp.
     *
     * @return the instance
     */
    @Nonnull
    public static Application refapp() {
        return INSTANCE;
    }

    private RefappApplication() {}

    @Override
    public String getKey() {
        return "refapp";
    }

    @Override
    public String getVersion() {
        return "1.0";
    }

    @Override
    public String getBuildNumber() {
        return "123";
    }
}
