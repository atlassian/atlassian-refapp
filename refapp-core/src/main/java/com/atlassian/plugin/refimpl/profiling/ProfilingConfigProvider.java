package com.atlassian.plugin.refimpl.profiling;

/**
 * Describes the desired profiling configuration
 *
 * @since 5.4.0
 */
public class ProfilingConfigProvider {
    private static final String PROFILING_ENABLED_SYS_PROP = "refapp.profiling.enabled";

    // Methods that will be used by package services, each method should represent the effect on each service

    protected boolean shouldAtlassianProfilingBeEnabled() {
        return refappProfilingEnabledSysProp();
    }

    protected boolean shouldMetricsBeExposedViaJmx() {
        return refappProfilingEnabledSysProp();
    }

    protected boolean shouldMicrometerBeEnabled() {
        return refappProfilingEnabledSysProp();
    }

    // Private methods for accessing system properties

    private boolean refappProfilingEnabledSysProp() {
        return Boolean.getBoolean(PROFILING_ENABLED_SYS_PROP);
    }
}
