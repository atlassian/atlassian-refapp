package com.atlassian.plugin.refimpl.db;

import com.atlassian.plugin.refimpl.lifecycle.AbstractSmartLifecycle;
import com.atlassian.plugin.refimpl.lifecycle.LifecycleOrder;
import com.atlassian.refapp.api.ConnectionProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.SmartLifecycle;

import javax.annotation.ParametersAreNonnullByDefault;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import static java.lang.String.format;
import static java.util.Objects.requireNonNull;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * Creates the database schema, for databases that have that concept.
 *
 * @since 5.2
 */
@ParametersAreNonnullByDefault
public class SchemaCreator extends AbstractSmartLifecycle {

    private static final Logger LOGGER = LoggerFactory.getLogger(SchemaCreator.class);

    private final ConnectionProvider connectionProvider;

    public SchemaCreator(final ConnectionProvider connectionProvider) {
        super(LifecycleOrder.SECOND);
        this.connectionProvider = requireNonNull(connectionProvider);
    }

    @Override
    protected void doStart() {
        createSchema();
    }

    /**
     * Creates an empty Refapp schema if no such schema already exists.
     */
    private void createSchema() {
        connectionProvider.schema()
                .filter(SchemaCreator::isValid)
                .ifPresent(this::doCreateSchema);
    }

    private static boolean isValid(final String schemaName) {
        return isNotBlank(schemaName)
                && !schemaName.contains(";")    // prevent SQL injection
                && !schemaName.contains("\"");  // would break our quoting
    }

    private void doCreateSchema(final String schemaName) {
        final String createSchemaSql = format("create schema if not exists \"%s\"", schemaName);
        try (final Connection connection = connectionProvider.connection();
             final Statement statement = connection.createStatement()) {
            statement.execute(createSchemaSql);
            LOGGER.debug("Created schema '{}' if it didn't already exist", schemaName);
        } catch (final SQLException e) {
            LOGGER.warn("Couldn't create schema '{}'", schemaName, e);
        }
    }
}
