package com.atlassian.plugin.refimpl.servlet;

import com.atlassian.plugin.servlet.AbstractFileServerServlet;
import com.atlassian.plugin.servlet.DownloadStrategy;

import java.util.ArrayList;
import java.util.List;

import static com.atlassian.plugin.refimpl.container.BeanLocator.getBeans;

public class FileServerServlet extends AbstractFileServerServlet {

    protected List<DownloadStrategy> getDownloadStrategies() {
        return new ArrayList<>(getBeans(DownloadStrategy.class, getServletContext()));
    }
}
