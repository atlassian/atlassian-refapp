package com.atlassian.plugin.refimpl.container.beans;

import com.atlassian.beehive.ClusterLockService;
import com.atlassian.beehive.simple.SimpleClusterLockService;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.healthcheck.spi.HealthCheckWhitelist;
import com.atlassian.healthcheck.spi.impl.ClasspathFileHealthCheckWhitelist;
import com.atlassian.plugin.main.AtlassianPlugins;
import com.atlassian.plugin.refimpl.crowd.embedded.EmbeddedCrowdAuthenticatorImpl;
import com.atlassian.plugin.refimpl.profiling.JmxExposingManager;
import com.atlassian.plugin.refimpl.profiling.ProfilingConfigProvider;
import com.atlassian.plugin.refimpl.profiling.ProfilingService;
import com.atlassian.plugin.refimpl.tenant.RefappTenantManager;
import com.atlassian.plugin.refimpl.version.HostVersion;
import com.atlassian.refapp.api.EmbeddedCrowdAuthenticator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * The Spring beans that make up Refapp. See {@link SmartLifecycleBeans} for how Refapp starts.
 *
 * @since 5.4 object creation was in ContainerManager (now RefappLauncher)
 */
@Configuration
@Import({
        DatabaseBeans.class,
        PluginSystemBeans.class,
        SchedulerBeans.class,
        SmartLifecycleBeans.class,
        WebResourceBeans.class,
        HibernateBeans.class,
        EmbeddedCrowdBeans.class
})
public class RefappBeans {

    // ------------------- Miscellaneous beans not worth their own bean config file -------------------

    @Bean
    public ClusterLockService clusterLockService() {
        return new SimpleClusterLockService();
    }

    @Bean
    public EventPublisher eventPublisher(final AtlassianPlugins plugins) {
        return plugins.getEventPublisher();
    }

    @Bean
    public HealthCheckWhitelist healthCheckWhitelist() {
        return new ClasspathFileHealthCheckWhitelist();
    }

    @Bean
    public HostVersion hostVersion() {
        return HostVersion.getInstance();
    }

    @Bean
    public JmxExposingManager micrometerMetrics(final ProfilingConfigProvider profilingConfigProvider) {
        return new JmxExposingManager(profilingConfigProvider);
    }

    @Bean
    public ProfilingConfigProvider profilingConfigProvider() {
        return new ProfilingConfigProvider();
    }

    @Bean
    public ProfilingService profilingService(final ProfilingConfigProvider profilingConfigProvider) {
        return new ProfilingService(profilingConfigProvider);
    }

    @Bean
    public RefappTenantManager tenantManager() {
        return RefappTenantManager.getInstance();
    }

    @Bean
    public EmbeddedCrowdAuthenticator embeddedCrowdAuthenticator() {
        return new EmbeddedCrowdAuthenticatorImpl();
    }
}
