package com.atlassian.plugin.refimpl.webresource;

import com.atlassian.plugin.webresource.WebResourceIntegration;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;

import static com.atlassian.plugin.refimpl.container.BeanLocator.getBean;

public class RequestCacheCleaner implements Filter {

    private WebResourceIntegration webResourceIntegration;

    @Override
    public void init(final FilterConfig filterConfig) {
        this.webResourceIntegration = getBean(WebResourceIntegration.class, filterConfig.getServletContext());
    }

    @Override
    public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain)
            throws IOException, ServletException {
        try {
            chain.doFilter(request, response);
        } finally {
            webResourceIntegration.getRequestCache().clear();
        }
    }

    @Override
    public void destroy() {
        // Nothing to do
    }
}
