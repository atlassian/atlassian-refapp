package com.atlassian.plugin.refimpl.crowd.noop;

import com.atlassian.crowd.audit.AuditLogEntry;
import com.atlassian.crowd.audit.AuditLogEventType;
import com.atlassian.crowd.manager.audit.mapper.AuditLogGroupMapper;
import com.atlassian.crowd.manager.audit.mapper.AuditLogUserMapper;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.user.User;
import org.apache.commons.lang3.builder.DiffResult;

import java.util.Collections;
import java.util.List;

/**
 * A mock implementation of the {@link AuditLogUserMapper} and {@link AuditLogGroupMapper} interface for Crowd.
 */
public class NoopAuditLogMapper implements AuditLogUserMapper, AuditLogGroupMapper {
    @Override
    public List<AuditLogEntry> calculateDifference(final Group group, final Group group1) {
        return Collections.emptyList();
    }

    @Override
    public AuditLogEntry calculatePasswordDiff() {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<AuditLogEntry> calculateDifference(final AuditLogEventType auditLogEventType, final User user, final User user1) {
        return Collections.emptyList();
    }

    @Override
    public List<AuditLogEntry> mapDiffsToAuditLogEntries(AuditLogEventType auditLogEventType, String s, DiffResult diffResult) {
        return Collections.emptyList();
    }
}
