package com.atlassian.plugin.refimpl.lifecycle;

import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.SplitStartupPluginSystemLifecycle;
import com.atlassian.plugin.main.AtlassianPlugins;

import static java.util.Objects.requireNonNull;

/**
 * Controls the startup and shutdown of the plugin system.
 *
 * @since 6.1
 */
public class PluginSystemLifecycle extends AbstractSmartLifecycle {

    private final AtlassianPlugins pluginSystem;
    private final PluginAccessor pluginAccessor;
    private final SplitStartupPluginSystemLifecycle splitStartupPluginSystemLifecycle;

    public PluginSystemLifecycle(final AtlassianPlugins pluginSystem,
                                 final PluginAccessor pluginAccessor,
                                 final SplitStartupPluginSystemLifecycle splitStartupPluginSystemLifecycle) {
        super(LifecycleOrder.FOURTH);
        this.pluginAccessor = requireNonNull(pluginAccessor);
        this.pluginSystem = requireNonNull(pluginSystem);
        this.splitStartupPluginSystemLifecycle = requireNonNull(splitStartupPluginSystemLifecycle);
    }

    @Override
    protected void doStart() {
        splitStartupPluginSystemLifecycle.init();
        pluginAccessor.getPlugins(); // presumably called for its side effect(s)
    }

    @Override
    protected void doStop() {
        splitStartupPluginSystemLifecycle.shutdown();
        pluginSystem.destroy();
    }
}
