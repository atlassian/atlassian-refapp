package com.atlassian.plugin.refimpl.file;

import javax.annotation.Nonnull;
import java.io.File;

/**
 * File-related utility methods.
 *
 * @since 5.4 moved from ContainerManager (now RefappLauncher)
 */
public final class FileUtils {

    private FileUtils() {}

    /**
     * Ensures that the given directory exists.
     *
     * @param path the directory path
     * @return the directory
     */
    @Nonnull
    public static File makeSureDirectoryExists(final String path) {
        final File dir = new File(path);
        if (!dir.exists() && !dir.mkdirs()) {
            throw new IllegalStateException("Could not create directory <" + dir + ">");
        }
        return dir;
    }
}
