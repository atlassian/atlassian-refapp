package com.atlassian.plugin.refimpl.lifecycle;

/**
 * The places in the startup sequence where a component can be initialised.
 *
 * @since 6.1
 */
public enum LifecycleOrder {

    // Keep these in sequential order, as we use their `ordinal()` value

    FIRST,
    SECOND,
    THIRD,
    FOURTH,
    FIFTH
}
