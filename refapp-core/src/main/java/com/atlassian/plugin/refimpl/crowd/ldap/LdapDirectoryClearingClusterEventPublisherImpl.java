package com.atlassian.plugin.refimpl.crowd.ldap;

import com.atlassian.crowd.directory.LdapDirectoryClearingClusterEventPublisher;

/**
 * A mock implementation of the {@link LdapDirectoryClearingClusterEventPublisher} interface for Crowd.
 */
public class LdapDirectoryClearingClusterEventPublisherImpl implements LdapDirectoryClearingClusterEventPublisher {
    @Override
    public void publishEvent(final Long directoryId) {
        throw new UnsupportedOperationException();
    }
}
