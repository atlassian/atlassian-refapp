package com.atlassian.plugin.refimpl.container.beans;

import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.SplitStartupPluginSystemLifecycle;
import com.atlassian.plugin.main.AtlassianPlugins;
import com.atlassian.plugin.refimpl.db.SchemaCreator;
import com.atlassian.plugin.refimpl.lifecycle.FactoryFinderLoader;
import com.atlassian.plugin.refimpl.lifecycle.PluginSystemLifecycle;
import com.atlassian.plugin.refimpl.lifecycle.SchedulerLifecycle;
import com.atlassian.plugin.refimpl.lifecycle.StartupMessageLogger;
import com.atlassian.refapp.api.ConnectionProvider;
import com.atlassian.scheduler.core.SchedulerServiceController;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;

/**
 * Components that implement the Refapp startup and shutdown sequences, using the Spring
 * {@link org.springframework.context.SmartLifecycle} API to control the sequence.
 *
 * @since 6.1
 */
public class SmartLifecycleBeans {

    @Bean
    public FactoryFinderLoader factoryFinderLoader() {
        return new FactoryFinderLoader();
    }

    @Bean
    public SchemaCreator schemaCreator(final ConnectionProvider connectionProvider) {
        return new SchemaCreator(connectionProvider);
    }

    @Bean
    public SchedulerLifecycle schedulerLifecycle(final SchedulerServiceController schedulerServiceController) {
        return new SchedulerLifecycle(schedulerServiceController);
    }

    @Bean
    public PluginSystemLifecycle pluginSystemLifecycle(
            final AtlassianPlugins atlassianPlugins,
            @Qualifier("pluginAccessor") final PluginAccessor pluginAccessor,
            @Qualifier("splitStartupPluginSystemLifecycle") final SplitStartupPluginSystemLifecycle pluginSystemLifecycle) {
        return new PluginSystemLifecycle(atlassianPlugins, pluginAccessor, pluginSystemLifecycle);
    }

    @Bean
    public StartupMessageLogger startupMessageLogger() {
        return new StartupMessageLogger();
    }
}
