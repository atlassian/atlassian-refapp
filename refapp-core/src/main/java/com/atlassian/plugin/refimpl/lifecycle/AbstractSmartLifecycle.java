package com.atlassian.plugin.refimpl.lifecycle;

import org.springframework.context.SmartLifecycle;

import javax.annotation.ParametersAreNonnullByDefault;

import static java.util.Objects.requireNonNull;

/**
 * Convenient superclass for implementing a {@link SmartLifecycle} component.
 *
 * @since 6.1
 */
@ParametersAreNonnullByDefault
public abstract class AbstractSmartLifecycle implements SmartLifecycle {

    private final LifecycleOrder lifecycleOrder;

    private volatile boolean running;

    /**
     * Constructor.
     *
     * @param lifecycleOrder the order in which this component should start up (also the reverse order of shutdown)
     */
    protected AbstractSmartLifecycle(final LifecycleOrder lifecycleOrder) {
        this.lifecycleOrder = requireNonNull(lifecycleOrder);
    }

    @Override
    public final int getPhase() {
        return lifecycleOrder.ordinal();
    }

    @Override
    public final boolean isRunning() {
        return running;
    }

    @Override
    public final void start() {
        running = true;
        doStart();
    }

    /**
     * Hook for concrete subclasses to perform their necessary startup logic.
     */
    protected abstract void doStart();

    @Override
    public final void stop() {
        running = false;
        doStop();
    }

    /**
     * Hook for concrete subclasses to perform any necessary shutdown logic.
     *
     * This implementation does nothing.
     */
    protected void doStop() {}
}
