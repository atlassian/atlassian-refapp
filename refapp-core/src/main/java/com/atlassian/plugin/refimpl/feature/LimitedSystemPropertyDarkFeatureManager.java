package com.atlassian.plugin.refimpl.feature;

import com.atlassian.sal.api.features.DarkFeatureManager;
import com.atlassian.sal.api.features.EnabledDarkFeatures;
import com.atlassian.sal.api.features.ValidFeatureKeyPredicate;
import com.atlassian.sal.api.user.UserKey;
import com.google.common.annotations.VisibleForTesting;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Optional;

import static com.atlassian.sal.api.features.EnabledDarkFeatures.NONE;
import static java.util.Objects.requireNonNull;

/**
 * Implements a limited number of methods, and checks system properties to determine if a feature is enabled.
 * @since 5.4.2
 */
public class LimitedSystemPropertyDarkFeatureManager implements DarkFeatureManager {

    // --- HELPERS -------

    @VisibleForTesting
    static boolean isValidFeatureKey(@Nonnull final String featureKey) {
        requireNonNull(featureKey);

        return ValidFeatureKeyPredicate.isValidFeatureKey(featureKey) &&
                featureKey.startsWith(ATLASSIAN_DARKFEATURE_PREFIX);
    }

    private static boolean checkSystemProperty(@Nonnull final String featureKey) {
        return !Boolean.getBoolean(DISABLE_ALL_DARKFEATURES_PROPERTY) &&
                Boolean.getBoolean(featureKey);
    }

    // --- OVERRIDES -------

    @Nonnull
    @Override
    public Optional<Boolean> isEnabledForAllUsers(@Nonnull String featureKey) {
        if (!isValidFeatureKey(featureKey)) {
            return Optional.empty();
        }

        return Optional.of(checkSystemProperty(featureKey));
    }

    @Nonnull
    @Override
    public Optional<Boolean> isEnabledForCurrentUser(@Nonnull String featureKey) {
        return Optional.empty();
    }

    @Nonnull
    @Override
    public Optional<Boolean> isEnabledForUser(@Nullable UserKey userKey, @Nonnull String featureKey) {
        return Optional.empty();
    }

    @Override
    public boolean isFeatureEnabledForAllUsers(String featureKey) {
        return isValidFeatureKey(featureKey) && checkSystemProperty(featureKey);
    }

    @Override
    public boolean isFeatureEnabledForCurrentUser(String featureKey) {
        return false;
    }

    @Override
    public boolean isFeatureEnabledForUser(@Nullable UserKey userKey, String featureKey) {
        return false;
    }

    @Override
    public boolean canManageFeaturesForAllUsers() {
        return false;
    }

    @Override
    public void enableFeatureForAllUsers(String featureKey) {
    }

    @Override
    public void disableFeatureForAllUsers(String featureKey) {
    }

    @Override
    public void enableFeatureForCurrentUser(String featureKey) {
    }

    @Override
    public void enableFeatureForUser(UserKey userKey, String featureKey) {
    }

    @Override
    public void disableFeatureForCurrentUser(String featureKey) {
    }

    @Override
    public void disableFeatureForUser(UserKey userKey, String featureKey) {
    }

    @Override
    public EnabledDarkFeatures getFeaturesEnabledForAllUsers() {
        return NONE;
    }

    @Override
    public EnabledDarkFeatures getFeaturesEnabledForCurrentUser() {
        return NONE;
    }

    @Override
    public EnabledDarkFeatures getFeaturesEnabledForUser(@Nullable UserKey userKey) {
        return NONE;
    }
}
