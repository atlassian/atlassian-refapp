package com.atlassian.plugin.refimpl.crowd.embedded;

import com.atlassian.crowd.directory.InternalDirectory;
import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.embedded.api.OperationType;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.embedded.impl.ImmutableDirectory;
import com.atlassian.crowd.embedded.impl.ImmutableGroup;
import com.atlassian.crowd.exception.ApplicationAlreadyExistsException;
import com.atlassian.crowd.exception.ApplicationNotFoundException;
import com.atlassian.crowd.exception.DirectoryInstantiationException;
import com.atlassian.crowd.exception.DirectoryNotFoundException;
import com.atlassian.crowd.exception.InvalidCredentialException;
import com.atlassian.crowd.exception.InvalidUserException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.exception.OperationNotPermittedException;
import com.atlassian.crowd.exception.embedded.InvalidGroupException;
import com.atlassian.crowd.manager.application.ApplicationManager;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.application.ApplicationAttributeConstants;
import com.atlassian.crowd.model.application.ApplicationType;
import com.atlassian.crowd.model.application.ImmutableApplication;
import com.atlassian.crowd.model.user.ImmutableUser;
import com.atlassian.crowd.model.user.ImmutableUserWithAttributes;
import com.atlassian.crowd.password.encoder.AtlassianSecurityPasswordEncoder;
import com.atlassian.crowd.password.encoder.PasswordEncoder;
import com.atlassian.crowd.password.factory.PasswordEncoderFactory;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Sets;

import javax.inject.Inject;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.atlassian.crowd.embedded.api.DirectoryType.INTERNAL;

/**
 * The {@code EmbeddedCrowdBootstrap} class is responsible for initializing the Embedded Crowd application and
 * creating default entities such as Application, Directory, User, and Group.
 * It contains methods to retrieve an existing Embedded Crowd application or create a new one, create an internal
 * directory, create a default administrator user, and a system administrator group.
 */
public class EmbeddedCrowdBootstrap {
    public static final String INTERNAL_DIRECTORY_NAME = "RefApp Internal Directory";
    private static final String EMBEDDED_CROWD_APPLICATION_NAME = "crowd-embedded";
    private final PasswordEncoder passwordEncoder = new AtlassianSecurityPasswordEncoder();
    @Inject
    private CrowdService crowdService;
    @Inject
    private ApplicationManager applicationManager;
    @Inject
    private DirectoryManager directoryManager;

    public void initialiseEmbeddedCrowd() throws OperationFailedException, ApplicationAlreadyExistsException, InvalidCredentialException, ApplicationNotFoundException, DirectoryNotFoundException, InvalidGroupException, OperationNotPermittedException, InvalidUserException {
        final Application embeddedCrowdApplication = getOrCreateApplication(EMBEDDED_CROWD_APPLICATION_NAME);

        final Directory internalDirectory = createInternalDirectory();
        this.applicationManager.addDirectoryMapping(
                embeddedCrowdApplication, internalDirectory, true, OperationType.values());

        final Group systemAdministratorGroup = new ImmutableGroup("system_administrators");
        this.crowdService.addGroup(systemAdministratorGroup);

        final Group administratorGroup = new ImmutableGroup("administrators");
        this.crowdService.addGroup(administratorGroup);

        final Group usersGroup = new ImmutableGroup("users");
        this.crowdService.addGroup(usersGroup);

        final Group unlicensedGroup = new ImmutableGroup("unlicensed");
        this.crowdService.addGroup(unlicensedGroup);

        final Map<ImmutableUser, Group> users = ImmutableMap.of(
                new ImmutableUser(1L, "admin", "A. D. Ministrator Sysadmin",
                        "admin@example.com", true, "A. D. Ministrator", "Sysadmin", "1"), systemAdministratorGroup,
                new ImmutableUser(2L, "fred", "Fred Sysadmin",
                        "fred@example.org", true, "Fred Sysadmin", "Sysadmin", "2"), systemAdministratorGroup,
                new ImmutableUser(3L, "betty", "Betty Admin",
                        "betty@example.com", true, "Betty", "Admin", "3"), administratorGroup,
                new ImmutableUser(4L, "barney", "Barney User",
                        "barney@example.com", true, "Barney", "User", "4"), usersGroup,
                new ImmutableUser(5L, "michell", "Michell Unlicensed",
                        "michell@example.org", true, "Michell Unlicensed", "Unlicensed", "5"), unlicensedGroup);

        for (final Map.Entry<ImmutableUser, Group> userEntry : users.entrySet()) {
            final ImmutableUser user = userEntry.getKey();
            final Group group = userEntry.getValue();

            final ImmutableUserWithAttributes immutableUserWithAttributes = new ImmutableUserWithAttributes.Builder(user, HashMultimap.create()).build();
            this.crowdService.addUser(immutableUserWithAttributes, user.getName());
            this.crowdService.addUserToGroup(user, group);
        }
    }

    public Application getOrCreateApplication(final String applicationName) throws ApplicationAlreadyExistsException, InvalidCredentialException {
        try {
            return this.applicationManager.findByName(applicationName);
        } catch (final ApplicationNotFoundException e) {
            final ImmutableApplication application = ImmutableApplication
                    .builder(applicationName, ApplicationType.CROWD)
                    .setPasswordCredential(PasswordCredential.encrypted(encodePassword(applicationName)))
                    .setDescription(applicationName)
                    .setCreatedDate(new Date())
                    .setUpdatedDate(new Date())
                    .setActive(true)
                    .setMembershipAggregationEnabled(true)
                    .setAttributes(ImmutableMap.of(ApplicationAttributeConstants.ATTRIBUTE_KEY_ATLASSIAN_SHA1_APPLIED,
                            Boolean.TRUE.toString(), ApplicationAttributeConstants.ATTRIBUTE_KEY_AGGREGATE_MEMBERSHIPS,
                            Boolean.TRUE.toString()))
                    .build();
            return this.applicationManager.add(application);
        }
    }

    public Directory createInternalDirectory() throws DirectoryInstantiationException {
        final ImmutableDirectory.Builder immutableDirectoryBuilder = ImmutableDirectory.newBuilder();
        immutableDirectoryBuilder.setName(INTERNAL_DIRECTORY_NAME);
        immutableDirectoryBuilder.setActive(true);
        immutableDirectoryBuilder.setType(INTERNAL);
        immutableDirectoryBuilder.setDescription(INTERNAL_DIRECTORY_NAME);
        immutableDirectoryBuilder.setImplementationClass(InternalDirectory.class.getCanonicalName());
        immutableDirectoryBuilder.setCreatedDate(new Date());
        immutableDirectoryBuilder.setUpdatedDate(new Date());
        immutableDirectoryBuilder.setAllowedOperations(Sets.newHashSet(OperationType.values()));
        final Map<String, String> directoryAttributes = new HashMap<>();
        directoryAttributes.put("user_encryption_method", PasswordEncoderFactory.ATLASSIAN_SECURITY_ENCODER);
        immutableDirectoryBuilder.setAttributes(directoryAttributes);
        final Directory directory = immutableDirectoryBuilder.toDirectory();
        return this.directoryManager.addDirectory(directory);
    }

    private String encodePassword(final String password) {
        return this.passwordEncoder.encodePassword(password, null);
    }
}
