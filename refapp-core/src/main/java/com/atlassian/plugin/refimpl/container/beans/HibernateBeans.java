package com.atlassian.plugin.refimpl.container.beans;

import com.atlassian.hibernate.extras.ResetableHiLoGeneratorHelper;
import com.atlassian.refapp.api.ConnectionProvider;
import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBuilder;
import org.vibur.dbcp.ViburDBCPDataSource;

/**
 * Defines beans related to Hibernate for embedded-crowd, instead of importing the resource
 * applicationContext-CrowdPersistence.xml via applicationContextEmbeddedCrowd.xml.
 * Refapp has its own database connection pool (ViburDBCP) and provides it to the
 * SessionFactory via datasource, so that Hibernate can skip creating a second pool (c3p0).
 */
@Configuration
@Import(DatabaseBeans.class)
public class HibernateBeans {

    @Bean
    public SessionFactory sessionFactory(final ConnectionProvider connectionProvider) {
        final ViburDBCPDataSource datasource = (ViburDBCPDataSource) connectionProvider.dataSource();
        final LocalSessionFactoryBuilder builder = new LocalSessionFactoryBuilder(datasource);
        builder.configure("hibernate.cfg.xml"); // This file is in crowd-persistence-hibernate5
        builder.setProperty("hibernate.hbm2ddl.auto", "create-drop");
        builder.setProperty("hibernate.connection.handling_mode", "DELAYED_ACQUISITION_AND_RELEASE_AFTER_TRANSACTION");
        return builder.buildSessionFactory();
    }

    @Bean
    public HibernateTransactionManager transactionManager(final SessionFactory sessionFactory) {
        final HibernateTransactionManager hibernateTransactionManager = new HibernateTransactionManager();
        hibernateTransactionManager.setSessionFactory(sessionFactory);
        return hibernateTransactionManager;
    }

    @Bean
    public ResetableHiLoGeneratorHelper resetableHiLoGeneratorHelper(final SessionFactory sessionFactory) {
        final ResetableHiLoGeneratorHelper resetableHiLoGeneratorHelper = new ResetableHiLoGeneratorHelper();
        resetableHiLoGeneratorHelper.setSessionFactory(sessionFactory);
        return resetableHiLoGeneratorHelper;
    }

}
