package com.atlassian.plugin.refimpl.crowd.embedded;

import com.atlassian.crowd.password.encoder.AtlassianSecurityPasswordEncoder;
import com.atlassian.crowd.password.factory.PasswordEncoderFactory;
import com.atlassian.crowd.plugin.descriptors.PasswordEncoderModuleDescriptor;
import com.atlassian.plugin.module.ModuleFactory;

public class EncoderModuleDescriptor extends PasswordEncoderModuleDescriptor<AtlassianSecurityPasswordEncoder> {

    public EncoderModuleDescriptor(PasswordEncoderFactory passwordEncoderFactory, ModuleFactory moduleFactory) {
        super(passwordEncoderFactory, moduleFactory);
    }
}
