package com.atlassian.plugin.refimpl.lifecycle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

/**
 * Logs a message to the console when Refapp starts.
 *
 * @since 6.1
 */
public class StartupMessageLogger extends AbstractSmartLifecycle {

    private static final Logger LOGGER = LoggerFactory.getLogger(StartupMessageLogger.class);

    public StartupMessageLogger() {
        super(LifecycleOrder.FIFTH);
    }

    @Override
    protected void doStart() {
        Optional.ofNullable(System.getProperty("baseurl.display"))
                .ifPresent(baseUrl -> LOGGER.info("\n\n*** Refapp started on {}\n\n", baseUrl));
    }
}
