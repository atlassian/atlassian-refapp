package com.atlassian.plugin.refimpl.lifecycle;

/**
 * Pre-emptively loads the {@code javax.xml.transform.FactoryFinder} class,
 * before any plugins do so.
 *
 * See https://ecosystem.atlassian.net/browse/REFAPP-353.
 *
 * @since 6.1
 */
public class FactoryFinderLoader extends AbstractSmartLifecycle {

    public FactoryFinderLoader() {
        super(LifecycleOrder.FIRST);
    }

    @Override
    protected void doStart() {
        try {
            Class.forName("javax.xml.transform.FactoryFinder");
        } catch (final ClassNotFoundException e) {
            throw new IllegalStateException(e);
        }
    }
}
