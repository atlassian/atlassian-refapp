package com.atlassian.plugin.refimpl.version;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * The host version. A value wrapper to improve type-safety.
 *
 * @since 5.4
 */
public class HostVersion {

    private static final Logger LOGGER = LoggerFactory.getLogger(HostVersion.class);

    /**
     * Returns the host version.
     *
     * @return see description
     */
    @Nonnull
    public static HostVersion getInstance() {
        return new HostVersion(loadVersion());
    }

    // Moved from ContainerManager (now RefappLauncher)
    @Nullable
    private static String loadVersion() {
        final Properties props = new Properties();
        try (InputStream in = HostVersion.class.getClassLoader().getResourceAsStream(
                "META-INF/maven/com.atlassian.plugins/atlassian-plugins-core/pom.properties")) {
            if (in != null) {
                props.load(in);
                return props.getProperty("version");
            }
        } catch (final IOException e) {
            LOGGER.error("Could not load version", e);
        }
        return null;
    }

    private final String version;

    private HostVersion(@Nullable final String version) {
        this.version = version;
    }

    /**
     * Returns the version.
     *
     * @return see description
     */
    @Nullable
    public String getValue() {
        return version;
    }
}
