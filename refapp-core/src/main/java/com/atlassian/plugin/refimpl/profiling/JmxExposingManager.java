package com.atlassian.plugin.refimpl.profiling;

import com.atlassian.util.profiling.Metrics;
import com.atlassian.util.profiling.StrategiesRegistry;
import com.atlassian.util.profiling.Timers;
import com.atlassian.util.profiling.micrometer.MicrometerStrategy;
import com.atlassian.util.profiling.micrometer.util.QualifiedCompatibleHierarchicalNameMapper;
import com.atlassian.util.profiling.micrometer.util.UnescapedObjectNameFactory;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.jmx.JmxReporter;
import io.micrometer.core.instrument.Clock;
import io.micrometer.core.instrument.composite.CompositeMeterRegistry;
import io.micrometer.jmx.JmxConfig;
import io.micrometer.jmx.JmxMeterRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;

import static com.atlassian.util.profiling.MetricTag.SEND_ANALYTICS;
import static io.micrometer.core.instrument.config.MeterFilter.ignoreTags;
import static java.util.Objects.requireNonNull;

/**
 * Manages exposing metrics from Atlassian Profiling through JMX
 *
 * @since 5.4.0
 */
public class JmxExposingManager {
    private static final String JMX_DOMAIN = "com.atlassian.refapp";
    private static final Logger log = LoggerFactory.getLogger(JmxExposingManager.class);

    private final ProfilingConfigProvider profilingConfigProvider;
    private CompositeMeterRegistry compositeMeterRegistry;
    private JmxMeterRegistry jmxMeterRegistry;
    private JmxReporter jmxReporter;
    private MetricRegistry metricRegistry;
    private MicrometerStrategy micrometerStrategy;

    public JmxExposingManager(final ProfilingConfigProvider profilingConfigProvider) {
        this.profilingConfigProvider = requireNonNull(profilingConfigProvider);
    }

    @PostConstruct
    public void onStart() {
        if (profilingConfigProvider.shouldMicrometerBeEnabled() && profilingConfigProvider.shouldMetricsBeExposedViaJmx()) {
            this.compositeMeterRegistry = new CompositeMeterRegistry();
            this.metricRegistry = new MetricRegistry();
            this.jmxReporter = JmxReporter.forRegistry(metricRegistry)
                    .inDomain(JMX_DOMAIN)
                    .createsObjectNamesWith(new UnescapedObjectNameFactory())
                    .build();
            this.jmxMeterRegistry = new JmxMeterRegistry(
                    JmxConfig.DEFAULT,
                    Clock.SYSTEM,
                    new QualifiedCompatibleHierarchicalNameMapper(),
                    metricRegistry,
                    jmxReporter);
            this.micrometerStrategy = new MicrometerStrategy(compositeMeterRegistry);

            jmxMeterRegistry.config().meterFilter(ignoreTags(SEND_ANALYTICS.getKey()));
            compositeMeterRegistry.add(jmxMeterRegistry);
            StrategiesRegistry.addMetricStrategy(micrometerStrategy);
            jmxMeterRegistry.start();
            Metrics.getConfiguration().setEnabled(true);
            Timers.getConfiguration().setEnabled(true);

            log.info("Started atlassian-profiling with JMX");
        }
    }
}
