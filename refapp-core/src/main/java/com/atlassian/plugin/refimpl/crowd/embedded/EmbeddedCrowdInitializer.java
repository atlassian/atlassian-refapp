package com.atlassian.plugin.refimpl.crowd.embedded;

import com.atlassian.crowd.exception.ApplicationAlreadyExistsException;
import com.atlassian.crowd.exception.ApplicationNotFoundException;
import com.atlassian.crowd.exception.DirectoryNotFoundException;
import com.atlassian.crowd.exception.InvalidCredentialException;
import com.atlassian.crowd.exception.InvalidUserException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.exception.OperationNotPermittedException;
import com.atlassian.crowd.exception.embedded.InvalidGroupException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

/**
 * The {@code EmbeddedCrowdInitializer} listens for the {@link ContextRefreshedEvent} and initializes the Embedded Crowd.
 * <p>This class uses the {@link EmbeddedCrowdBootstrap} bean to initialize Embedded Crowd.
 *
 * @see EmbeddedCrowdBootstrap
 * @see ContextRefreshedEvent
 * @see EventListener
 */
@Component
public class EmbeddedCrowdInitializer {
    private static final Logger LOGGER = LoggerFactory.getLogger(EmbeddedCrowdInitializer.class);

    private final EmbeddedCrowdBootstrap embeddedCrowdBootstrap;

    @Autowired
    public EmbeddedCrowdInitializer(final EmbeddedCrowdBootstrap embeddedCrowdBootstrap) {
        this.embeddedCrowdBootstrap = embeddedCrowdBootstrap;
    }

    @EventListener(ContextRefreshedEvent.class)
    public void initializeEmbeddedCrowd() {
        try {
            this.embeddedCrowdBootstrap.initialiseEmbeddedCrowd();
            LOGGER.info("Embedded Crowd has been initialized in RefApp!");
        } catch (final InvalidGroupException | ApplicationAlreadyExistsException | ApplicationNotFoundException |
                       OperationFailedException | DirectoryNotFoundException | InvalidCredentialException |
                       InvalidUserException | OperationNotPermittedException e) {
            throw new IllegalStateException("Failed initializing Embedded Crowd in RefApp!", e);
        }
    }
}

