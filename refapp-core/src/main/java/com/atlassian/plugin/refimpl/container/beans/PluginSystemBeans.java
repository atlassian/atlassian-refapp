package com.atlassian.plugin.refimpl.container.beans;

import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginController;
import com.atlassian.plugin.SplitStartupPluginSystemLifecycle;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.hostcontainer.HostContainer;
import com.atlassian.plugin.main.AtlassianPlugins;
import com.atlassian.plugin.main.PluginsConfiguration;
import com.atlassian.plugin.main.PluginsConfigurationBuilder;
import com.atlassian.plugin.manager.PluginPersistentStateStore;
import com.atlassian.plugin.metadata.DefaultPluginMetadataManager;
import com.atlassian.plugin.metadata.PluginMetadataManager;
import com.atlassian.plugin.module.ClassPrefixModuleFactory;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.module.PrefixDelegatingModuleFactory;
import com.atlassian.plugin.module.PrefixModuleFactory;
import com.atlassian.plugin.osgi.container.OsgiContainerManager;
import com.atlassian.plugin.osgi.container.PackageScannerConfiguration;
import com.atlassian.plugin.osgi.hostcomponents.HostComponentProvider;
import com.atlassian.plugin.osgi.module.BeanPrefixModuleFactory;
import com.atlassian.plugin.refimpl.DefaultPluginPersistentStateStore;
import com.atlassian.plugin.refimpl.container.SpringHostContainer;
import com.atlassian.plugin.refimpl.plugins.RefappHostComponentProvider;
import com.atlassian.plugin.refimpl.plugins.RefappModuleDescriptorFactoryFactory;
import com.atlassian.plugin.refimpl.saldeps.CookieBasedScopeManager;
import com.atlassian.plugin.refimpl.version.HostVersion;
import com.atlassian.plugin.scope.ScopeManager;
import com.atlassian.plugin.servlet.DefaultServletModuleManager;
import com.atlassian.plugin.servlet.ServletModuleManager;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.web.context.ServletContextAware;

import javax.annotation.Nonnull;
import javax.servlet.ServletContext;
import java.io.File;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;

import static com.atlassian.plugin.refimpl.plugins.PackageScannerConfigurationFactory.getPackageScannerConfiguration;
import static com.atlassian.plugin.refimpl.plugins.RefappApplication.refapp;
import static com.atlassian.plugin.refimpl.servlet.ServletContextUtils.findAndCreateDirectory;
import static com.atlassian.plugin.refimpl.servlet.ServletContextUtils.getBundledPluginDir;
import static com.atlassian.plugin.refimpl.servlet.ServletContextUtils.makeSureDirectoryExists;
import static java.util.Arrays.asList;
import static java.util.Objects.requireNonNull;

/**
 * Spring beans relating to the OSGi plugin system.
 *
 * @since 5.4
 */
@Configuration
public class PluginSystemBeans implements ServletContextAware {

    private ServletContext servletContext;

    @Override
    public void setServletContext(@Nonnull final ServletContext servletContext) {
        this.servletContext = requireNonNull(servletContext);
    }

    @Bean
    public AtlassianPlugins atlassianPlugins(final PluginsConfiguration pluginsConfiguration) {
        return new AtlassianPlugins(pluginsConfiguration);
    }

    @Bean
    public URL bundledPluginDir() {
        return getBundledPluginDir(this.servletContext);
    }

    @Bean
    public File frameworkBundlesDir() {
        return findAndCreateDirectory(this.servletContext, "framework.bundles", "/WEB-INF/framework-bundles");
    }

    @Bean("refappHostComponentProvider")
    public RefappHostComponentProvider hostComponentProvider() {
        return RefappHostComponentProvider.getInstance();
    }

    @Bean
    public HostContainer hostContainer() {
        return new SpringHostContainer();
    }

    @Bean
    public ModuleDescriptorFactory moduleDescriptorFactory(final HostContainer hostContainer) {
        return RefappModuleDescriptorFactoryFactory.getInstance(hostContainer);
    }

    @Bean
    public ModuleFactory moduleFactory(final HostContainer hostContainer) {
        final Set<PrefixModuleFactory> prefixModuleFactories = new HashSet<>(asList(
                new BeanPrefixModuleFactory(),
                new ClassPrefixModuleFactory(hostContainer)
        ));
        return new PrefixDelegatingModuleFactory(prefixModuleFactories);
    }

    @Bean
    public File osgiCache() {
        return findAndCreateDirectory(this.servletContext, "osgi.cache", "/WEB-INF/osgi-cache");
    }

    @Bean
    public OsgiContainerManager osgiContainerManager(final AtlassianPlugins plugins) {
        return plugins.getOsgiContainerManager();
    }

    @Bean
    public PackageScannerConfiguration packageScannerConfiguration(final HostVersion hostVersion) {
        return getPackageScannerConfiguration(this.servletContext, hostVersion);
    }

    @Bean
    public PluginAccessor pluginAccessor(final AtlassianPlugins plugins) {
        return plugins.getPluginAccessor();
    }

    @Bean(destroyMethod = "")
    public PluginController pluginController(final AtlassianPlugins plugins) {
        return plugins.getPluginController();
    }

    @Bean
    public File pluginDirectory() {
        return makeSureDirectoryExists(this.servletContext, "/WEB-INF/plugins");
    }

    @Bean
    public PluginEventManager pluginEventManager(final AtlassianPlugins plugins) {
        return plugins.getPluginEventManager();
    }

    @Bean
    public PluginMetadataManager pluginMetadataManager() {
        return new DefaultPluginMetadataManager();
    }

    @Bean
    public PluginPersistentStateStore pluginPersistentStateStore(@Qualifier("osgiCache") final File osgiCache) {
        return new DefaultPluginPersistentStateStore(osgiCache);
    }

    @Bean
    @SuppressWarnings("java:S107")
    public PluginsConfiguration pluginsConfiguration(
            @Qualifier("bundledPluginDir") final URL bundledPluginDir,
            @Qualifier("frameworkBundlesDir") final File frameworkBundlesDir,
            @Qualifier("pluginDirectory") final File pluginDirectory,
            final ModuleDescriptorFactory moduleDescriptorFactory,
            final PackageScannerConfiguration scannerConfig,
            @Qualifier("refappHostComponentProvider") final HostComponentProvider hostComponentProvider,
            @Qualifier("osgiCache") final File osgiCache,
            final PluginPersistentStateStore pluginStateStore,
            final ScopeManager scopeManager) {
        // When bundledPluginUrl is a directory, bundledPluginCacheDirectory is not used at all.
        // But we have to keep it to make the validation happy.
        final File legacyBundledPluginCacheDirectory = FileUtils.toFile(bundledPluginDir);
        return new PluginsConfigurationBuilder()
                .useLegacyDynamicPluginDeployer(true)
                .frameworkBundleDirectory(frameworkBundlesDir)
                .bundledPluginUrl(bundledPluginDir)
                .bundledPluginCacheDirectory(legacyBundledPluginCacheDirectory)
                .pluginDirectory(pluginDirectory)
                .moduleDescriptorFactory(moduleDescriptorFactory)
                .packageScannerConfiguration(scannerConfig)
                .hostComponentProvider(hostComponentProvider)
                .osgiPersistentCache(osgiCache)
                .pluginStateStore(pluginStateStore)
                .application(refapp())
                .scopeManager(scopeManager)
                .build();
    }

    @Bean
    public ScopeManager scopeManager() {
        return new CookieBasedScopeManager();
    }

    @Bean
    public ServletModuleManager servletModuleManager(
            final PluginEventManager pluginEventManager, final ScopeManager scopeManager) {
        return new DefaultServletModuleManager(this.servletContext, pluginEventManager, scopeManager);
    }

    @Bean(destroyMethod = "")  // RefappLauncher calls its shutdown() method at the appropriate time
    @Primary
    public SplitStartupPluginSystemLifecycle splitStartupPluginSystemLifecycle(final AtlassianPlugins plugins) {
        /*
            The returned object is actually a DefaultPluginManager, which apart from implementing
            SplitStartupPluginSystemLifecycle also implements PluginAccessor and PluginController. However, there are
            separate bean definitions for those latter two types, so if you get an error about too many beans of those
            types, qualify your bean lookup with the relevant bean name, i.e. "pluginAccessor" or "pluginController".
         */
        return plugins.getPluginSystemLifecycle();
    }
}
