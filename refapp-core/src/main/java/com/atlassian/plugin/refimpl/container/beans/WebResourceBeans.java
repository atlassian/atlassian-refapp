package com.atlassian.plugin.refimpl.container.beans;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.refimpl.saldeps.CoreRefimplI18nResolver;
import com.atlassian.plugin.refimpl.saldeps.CoreRefimplLocaleResolver;
import com.atlassian.plugin.refimpl.servlet.SimpleContentTypeResolver;
import com.atlassian.plugin.refimpl.version.HostVersion;
import com.atlassian.plugin.refimpl.webresource.RefAppResourceBatchingConfiguration;
import com.atlassian.plugin.refimpl.webresource.SimpleWebResourceIntegration;
import com.atlassian.plugin.servlet.DownloadStrategy;
import com.atlassian.plugin.servlet.ServletContextFactory;
import com.atlassian.plugin.webresource.PluginResourceLocator;
import com.atlassian.plugin.webresource.PluginResourceLocatorImpl;
import com.atlassian.plugin.webresource.ResourceBatchingConfiguration;
import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.plugin.webresource.WebResourceManagerImpl;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;
import com.atlassian.plugin.webresource.WebResourceUrlProviderImpl;
import com.atlassian.plugin.webresource.assembler.DefaultPageBuilderService;
import com.atlassian.plugin.webresource.assembler.DefaultWebResourceAssemblerFactory;
import com.atlassian.plugin.webresource.prebake.PrebakeWebResourceAssemblerFactory;
import com.atlassian.plugin.webresource.servlet.PluginResourceDownload;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.ServletContextAware;

import javax.annotation.Nonnull;
import javax.servlet.ServletContext;

import static java.util.Objects.requireNonNull;

/**
 * Spring beans relating to web resources.
 *
 * @since 5.4
 */
@Configuration
public class WebResourceBeans implements ServletContextAware {

    private ServletContext servletContext;

    @Override
    public void setServletContext(@Nonnull final ServletContext servletContext) {
        this.servletContext = requireNonNull(servletContext);
    }

    @Bean
    public PageBuilderService pageBuilderService(
            final WebResourceIntegration webResourceIntegration,
            final PrebakeWebResourceAssemblerFactory prebakeWebResourceAssemblerFactory) {
        return new DefaultPageBuilderService(webResourceIntegration, prebakeWebResourceAssemblerFactory);
    }

    @Bean
    public DownloadStrategy pluginDownloadStrategy(final PluginResourceLocator pluginResourceLocator) {
        return new PluginResourceDownload(
                pluginResourceLocator, new SimpleContentTypeResolver(), "UTF-8");
    }

    @Bean
    public PluginResourceLocator pluginResourceLocator(final WebResourceIntegration webResourceIntegration,
                                                       final ServletContextFactory servletContextFactory,
                                                       final WebResourceUrlProvider webResourceUrlProvider,
                                                       final ResourceBatchingConfiguration refappBatchConfig,
                                                       final PluginEventManager pluginEventManager) {
        return new PluginResourceLocatorImpl(webResourceIntegration,
                servletContextFactory, webResourceUrlProvider, refappBatchConfig, pluginEventManager);
    }

    @Bean
    public RefAppResourceBatchingConfiguration resourceBatchingConfiguration() {
        return new RefAppResourceBatchingConfiguration();
    }

    @Bean
    public PrebakeWebResourceAssemblerFactory webResourceAssemblerFactory(
            final PluginResourceLocator pluginResourceLocator) {
        return new DefaultWebResourceAssemblerFactory(pluginResourceLocator);
    }

    @Bean
    public ServletContextFactory servletContextFactory() {
        return () -> servletContext;
    }

    @Bean
    public I18nResolver i18nResolver(PluginAccessor pluginAccessor, PluginEventManager pluginEventManager) {
        return new CoreRefimplI18nResolver(pluginAccessor, pluginEventManager);
    }


    @Bean
    public SimpleWebResourceIntegration webResourceIntegration(
            final PluginAccessor pluginAccessor,
            final PluginEventManager pluginEventManager,
            final HostVersion hostVersion,
            final I18nResolver i18nResolver,
            final RefAppResourceBatchingConfiguration refappBatchConfig,
            final EventPublisher eventPublisher
    ) {
        return new SimpleWebResourceIntegration(
                new CoreRefimplLocaleResolver(),
                i18nResolver,
                pluginEventManager,
                hostVersion,
                refappBatchConfig,
                eventPublisher,
                pluginAccessor
        );
    }

    @Bean
    public WebResourceManager webResourceManager(
            final PluginResourceLocator pluginResourceLocator,
            final WebResourceIntegration webResourceIntegration,
            final WebResourceUrlProvider webResourceUrlProvider,
            final ResourceBatchingConfiguration refappBatchConfig) {
        return new WebResourceManagerImpl(
                pluginResourceLocator, webResourceIntegration, webResourceUrlProvider, refappBatchConfig);
    }

    @Bean
    public WebResourceUrlProvider webResourceUrlProvider(final WebResourceIntegration webResourceIntegration) {
        return new WebResourceUrlProviderImpl(webResourceIntegration);
    }
}
