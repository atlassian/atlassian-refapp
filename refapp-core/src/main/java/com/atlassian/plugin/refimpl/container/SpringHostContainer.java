package com.atlassian.plugin.refimpl.container;

import com.atlassian.plugin.hostcontainer.HostContainer;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import javax.annotation.Nonnull;

//TODO: It is a copy of `SpringHostContainer` from `atlassian-plugins-osgi-bridge` module
public class SpringHostContainer implements HostContainer, ApplicationContextAware {
    private ApplicationContext applicationContext;

    public <T> T create(Class<T> moduleClass) {
        if (applicationContext == null) {
            throw new IllegalStateException("Application context missing");
        }
        //noinspection unchecked
        return (T) applicationContext.getAutowireCapableBeanFactory().createBean(moduleClass, AutowireCapableBeanFactory.AUTOWIRE_CONSTRUCTOR, false);
    }

    public void setApplicationContext(@Nonnull ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }
}
