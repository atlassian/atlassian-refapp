package com.atlassian.plugin.refimpl.container;

import com.atlassian.plugin.refimpl.container.beans.RefappBeans;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.AbstractContextLoaderInitializer;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;

/**
 * The {@link WebApplicationInitializer} for Refapp.
 *
 * See <a href="https://www.baeldung.com/spring-web-contexts">Spring Web Contexts</a> for how this works.
 *
 * @since 5.4
 */
public class RefappApplicationInitializer extends AbstractContextLoaderInitializer {

    @Override
    protected WebApplicationContext createRootApplicationContext() {
        final AnnotationConfigWebApplicationContext applicationContext = new AnnotationConfigWebApplicationContext();
        applicationContext.register(RefappBeans.class);
        return applicationContext;
    }
}
