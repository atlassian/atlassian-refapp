package com.atlassian.plugin.refimpl.plugins;

import com.atlassian.plugin.DefaultModuleDescriptorFactory;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.eventlistener.descriptors.EventListenerModuleDescriptor;
import com.atlassian.plugin.hostcontainer.HostContainer;
import com.atlassian.plugin.refimpl.crowd.embedded.EncoderModuleDescriptor;
import com.atlassian.plugin.schema.impl.DefaultDescribedModuleDescriptorFactory;
import com.atlassian.plugin.servlet.descriptors.ServletContextListenerModuleDescriptor;
import com.atlassian.plugin.servlet.descriptors.ServletContextParamModuleDescriptor;
import com.atlassian.plugin.servlet.descriptors.ServletFilterModuleDescriptor;
import com.atlassian.plugin.servlet.descriptors.ServletModuleDescriptor;
import com.atlassian.plugin.webresource.WebResourceModuleDescriptor;
import com.atlassian.plugin.webresource.transformer.UrlReadingWebResourceTransformerModuleDescriptor;
import com.atlassian.plugin.webresource.transformer.WebResourceTransformerModuleDescriptor;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * The {@link com.atlassian.plugin.ModuleDescriptorFactory} for Refapp.
 *
 * @since 5.4 code moved from ContainerManager (now RefappLauncher)
 */
public final class RefappModuleDescriptorFactoryFactory {

    private static final Map<String, Class<? extends ModuleDescriptor<?>>> MODULE_DESCRIPTORS = new LinkedHashMap<>();

    static {
        MODULE_DESCRIPTORS.put("listener", EventListenerModuleDescriptor.class);
        MODULE_DESCRIPTORS.put("servlet", ServletModuleDescriptor.class);
        MODULE_DESCRIPTORS.put("servlet-context-listener", ServletContextListenerModuleDescriptor.class);
        MODULE_DESCRIPTORS.put("servlet-context-param", ServletContextParamModuleDescriptor.class);
        MODULE_DESCRIPTORS.put("servlet-filter", ServletFilterModuleDescriptor.class);
        MODULE_DESCRIPTORS.put("url-reading-web-resource-transformer", UrlReadingWebResourceTransformerModuleDescriptor.class);
        MODULE_DESCRIPTORS.put("web-resource", WebResourceModuleDescriptor.class);
        MODULE_DESCRIPTORS.put("web-resource-transformer", WebResourceTransformerModuleDescriptor.class);
        MODULE_DESCRIPTORS.put("encoder", EncoderModuleDescriptor.class);
    }

    /**
     * Factory method.
     *
     * @param hostContainer the host container
     * @return a new instance
     */
    public static ModuleDescriptorFactory getInstance(final HostContainer hostContainer) {
        final DefaultModuleDescriptorFactory moduleDescriptorFactory =
                new DefaultDescribedModuleDescriptorFactory(hostContainer);
        MODULE_DESCRIPTORS.forEach(moduleDescriptorFactory::addModuleDescriptor);
        return moduleDescriptorFactory;
    }
}
