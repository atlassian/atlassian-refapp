package com.atlassian.plugin.refimpl.container.beans;

import com.atlassian.beehive.ClusterLockService;
import com.atlassian.cache.CacheFactory;
import com.atlassian.cache.memory.MemoryCacheManager;
import com.atlassian.crowd.audit.AuditLogContext;
import com.atlassian.crowd.audit.AuditLogContextInternal;
import com.atlassian.crowd.audit.AuditLogContextInternalImpl;
import com.atlassian.crowd.audit.NoOpAuditLogContext;
import com.atlassian.crowd.core.event.DelegatingMultiEventPublisher;
import com.atlassian.crowd.core.event.MultiEventPublisher;
import com.atlassian.crowd.core.event.listener.AutoGroupAdderListener;
import com.atlassian.crowd.core.event.listener.DefaultGroupMembershipResolver;
import com.atlassian.crowd.core.event.listener.DirectoryDefaultGroupMembershipResolver;
import com.atlassian.crowd.crypto.Base64Encryptor;
import com.atlassian.crowd.crypto.DirectoryPasswordsEncryptor;
import com.atlassian.crowd.dao.application.ApplicationDAO;
import com.atlassian.crowd.dao.group.GroupDAOHibernate;
import com.atlassian.crowd.dao.property.PropertyDAO;
import com.atlassian.crowd.dao.user.UserDAOHibernate;
import com.atlassian.crowd.dao.webhook.WebhookDAO;
import com.atlassian.crowd.darkfeature.CrowdDarkFeatureManager;
import com.atlassian.crowd.darkfeature.CrowdDarkFeatureManagerImpl;
import com.atlassian.crowd.directory.LdapDirectoryClearingClusterEventPublisher;
import com.atlassian.crowd.directory.TransactionalDirectoryCacheFactory;
import com.atlassian.crowd.directory.ldap.cache.CacheRefresherFactory;
import com.atlassian.crowd.directory.ldap.cache.DirectoryCacheFactory;
import com.atlassian.crowd.directory.ldap.util.LDAPPropertiesHelper;
import com.atlassian.crowd.directory.ldap.util.LdapConnectionPropertiesDiffResultMapper;
import com.atlassian.crowd.directory.loader.AzureAdDirectoryInstanceLoader;
import com.atlassian.crowd.directory.loader.AzureAdDirectoryInstanceLoaderImpl;
import com.atlassian.crowd.directory.loader.CustomDirectoryInstanceLoader;
import com.atlassian.crowd.directory.loader.DbCachingRemoteDirectoryInstanceLoader;
import com.atlassian.crowd.directory.loader.DelegatedAuthenticationDirectoryInstanceLoader;
import com.atlassian.crowd.directory.loader.DelegatedAuthenticationDirectoryInstanceLoaderImpl;
import com.atlassian.crowd.directory.loader.DelegatingDirectoryInstanceLoader;
import com.atlassian.crowd.directory.loader.DelegatingDirectoryInstanceLoaderImpl;
import com.atlassian.crowd.directory.loader.DirectoryInstanceLoader;
import com.atlassian.crowd.directory.loader.InternalDirectoryInstanceLoader;
import com.atlassian.crowd.directory.loader.InternalDirectoryInstanceLoaderImpl;
import com.atlassian.crowd.directory.loader.LDAPDirectoryInstanceLoader;
import com.atlassian.crowd.directory.loader.LDAPDirectoryInstanceLoaderImpl;
import com.atlassian.crowd.directory.loader.RemoteCrowdDirectoryInstanceLoader;
import com.atlassian.crowd.directory.loader.RemoteCrowdDirectoryInstanceLoaderImpl;
import com.atlassian.crowd.directory.synchronisation.cache.CacheRefresherFactoryImpl;
import com.atlassian.crowd.embedded.api.ApplicationFactory;
import com.atlassian.crowd.embedded.api.CrowdDirectoryService;
import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.crowd.embedded.api.Encryptor;
import com.atlassian.crowd.embedded.core.CrowdDirectoryServiceImpl;
import com.atlassian.crowd.embedded.core.CrowdEmbeddedApplicationFactory;
import com.atlassian.crowd.embedded.core.CrowdServiceImpl;
import com.atlassian.crowd.embedded.spi.DcLicenseChecker;
import com.atlassian.crowd.embedded.spi.DirectoryDao;
import com.atlassian.crowd.embedded.spi.DirectorySynchronisationStatusDao;
import com.atlassian.crowd.embedded.spi.DirectorySynchronisationTokenDao;
import com.atlassian.crowd.embedded.validator.DirectoryValidatorFactory;
import com.atlassian.crowd.embedded.validator.impl.DirectoryValidatorFactoryImpl;
import com.atlassian.crowd.event.EventStore;
import com.atlassian.crowd.event.EventStoreGeneric;
import com.atlassian.crowd.event.listener.mapper.AuditLogDiffResultMapperImpl;
import com.atlassian.crowd.license.DcLicenseCheckerImpl;
import com.atlassian.crowd.manager.application.ApplicationManager;
import com.atlassian.crowd.manager.application.ApplicationManagerGeneric;
import com.atlassian.crowd.manager.application.ApplicationService;
import com.atlassian.crowd.manager.application.AuthenticationOrderOptimizer;
import com.atlassian.crowd.manager.application.filtering.AccessFilterFactory;
import com.atlassian.crowd.manager.application.filtering.AccessFilterFactoryImpl;
import com.atlassian.crowd.manager.application.search.DefaultSearchStrategyFactory;
import com.atlassian.crowd.manager.application.search.SearchStrategyFactory;
import com.atlassian.crowd.manager.audit.AuditLogChangesetPopulator;
import com.atlassian.crowd.manager.audit.AuditLogChangesetPopulatorImpl;
import com.atlassian.crowd.manager.audit.AuditLogEnabledChecker;
import com.atlassian.crowd.manager.audit.AuditLogEnabledCheckerImpl;
import com.atlassian.crowd.manager.audit.AuditLogMetadataResolver;
import com.atlassian.crowd.manager.audit.AuditLogMetadataResolverImpl;
import com.atlassian.crowd.manager.audit.AuditLogProperties;
import com.atlassian.crowd.manager.audit.AuditLogSystemProperties;
import com.atlassian.crowd.manager.audit.AuditService;
import com.atlassian.crowd.manager.audit.NoOpAuditService;
import com.atlassian.crowd.manager.audit.mapper.AuditLogDiffResultMapper;
import com.atlassian.crowd.manager.audit.mapper.AuditLogGroupMapper;
import com.atlassian.crowd.manager.audit.mapper.AuditLogUserMapper;
import com.atlassian.crowd.manager.audit.mapper.AuditLogUserMapperImpl;
import com.atlassian.crowd.manager.avatar.AvatarProvider;
import com.atlassian.crowd.manager.avatar.WebServiceAvatarProvider;
import com.atlassian.crowd.manager.cluster.message.NoOpClusterMessageService;
import com.atlassian.crowd.manager.directory.BeforeGroupRemoval;
import com.atlassian.crowd.manager.directory.DirectoryDaoTransactionalDecorator;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.manager.directory.DirectorySynchronisationInformationStore;
import com.atlassian.crowd.manager.directory.DirectorySynchronisationTokenStore;
import com.atlassian.crowd.manager.directory.DirectorySynchroniser;
import com.atlassian.crowd.manager.directory.DirectorySynchroniserImpl;
import com.atlassian.crowd.manager.directory.InDatabaseDirectorySynchronisationInformationStore;
import com.atlassian.crowd.manager.directory.InDatabaseDirectorySynchronisationTokenStore;
import com.atlassian.crowd.manager.directory.InternalSynchronisationStatusManager;
import com.atlassian.crowd.manager.directory.NoopBeforeGroupRemoval;
import com.atlassian.crowd.manager.directory.SynchronisationStatusManager;
import com.atlassian.crowd.manager.directory.SynchronisationStatusManagerImpl;
import com.atlassian.crowd.manager.directory.TransactionalDirectoryDao;
import com.atlassian.crowd.manager.directory.monitor.poller.AtlassianSchedulerDirectoryPollerManager;
import com.atlassian.crowd.manager.directory.monitor.poller.DirectoryPollerJobRunner;
import com.atlassian.crowd.manager.directory.monitor.poller.DirectoryPollerManager;
import com.atlassian.crowd.manager.directory.nestedgroups.NestedGroupsCacheProvider;
import com.atlassian.crowd.manager.permission.PermissionManager;
import com.atlassian.crowd.manager.permission.PermissionManagerImpl;
import com.atlassian.crowd.manager.property.PropertyManager;
import com.atlassian.crowd.manager.property.PropertyManagerGeneric;
import com.atlassian.crowd.manager.recovery.RecoveryModeAwareApplicationService;
import com.atlassian.crowd.manager.recovery.RecoveryModeAwareDirectoryManager;
import com.atlassian.crowd.manager.recovery.RecoveryModeDirectoryLoader;
import com.atlassian.crowd.manager.recovery.RecoveryModeService;
import com.atlassian.crowd.manager.recovery.SystemPropertyRecoveryModeService;
import com.atlassian.crowd.manager.threadlocal.CrowdThreadLocalStateAccessor;
import com.atlassian.crowd.manager.threadlocal.ThreadLocalStateAccessor;
import com.atlassian.crowd.manager.webhook.WebhookRegistry;
import com.atlassian.crowd.manager.webhook.WebhookRegistryImpl;
import com.atlassian.crowd.password.factory.PasswordEncoderFactory;
import com.atlassian.crowd.service.cluster.ClusterMessageService;
import com.atlassian.crowd.service.cluster.ClusterService;
import com.atlassian.crowd.service.license.LicenseService;
import com.atlassian.crowd.util.DirectorySynchronisationEventHelper;
import com.atlassian.crowd.util.I18nHelper;
import com.atlassian.crowd.util.InstanceFactory;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.refimpl.ParameterUtils;
import com.atlassian.plugin.refimpl.crowd.embedded.EmbeddedCrowdBootstrap;
import com.atlassian.plugin.refimpl.crowd.embedded.EmbeddedCrowdInitializer;
import com.atlassian.plugin.refimpl.crowd.ldap.LdapDirectoryClearingClusterEventPublisherImpl;
import com.atlassian.plugin.refimpl.crowd.license.LicenseServiceImpl;
import com.atlassian.plugin.refimpl.crowd.noop.NoopAuditLogMapper;
import com.atlassian.plugin.refimpl.crowd.service.ClusterServiceImpl;
import com.atlassian.plugin.refimpl.feature.LimitedSystemPropertyDarkFeatureManager;
import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.scheduler.SchedulerService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.interceptor.TransactionInterceptor;

import java.net.URI;
import java.time.Clock;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Spring beans relating to Crowd or Embedded-Crowd.
 * <p>
 * Read the Crowd/Embedded-Crowd relating bean definitions from libraries in applicationContextEmbeddedCrowd.xml
 *
 * @since 6.2
 */
@Configuration
@Import(HibernateBeans.class)
@ImportResource({"classpath*:applicationContextEmbeddedCrowd.xml"})
@EnableTransactionManagement(proxyTargetClass = true)
public class EmbeddedCrowdBeans {
    @Bean
    public EmbeddedCrowdInitializer embeddedCrowdInitializer(final EmbeddedCrowdBootstrap embeddedCrowdBootstrap) {
        return new EmbeddedCrowdInitializer(embeddedCrowdBootstrap);
    }

    @Bean
    public EmbeddedCrowdBootstrap embeddedCrowdBootstrap() {
        return new EmbeddedCrowdBootstrap();
    }

    // Service
    @Bean
    public CrowdService crowdService(final ApplicationFactory applicationFactory, final ApplicationService applicationService, final DelegatingDirectoryInstanceLoader delegatingDirectoryInstanceLoader) {
        return new CrowdServiceImpl(applicationFactory, applicationService, delegatingDirectoryInstanceLoader);
    }

    @Bean
    public ApplicationService applicationService(final DirectoryManager directoryManager, final SearchStrategyFactory searchStrategyFactory, final PermissionManager permissionManager, @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection") final EventPublisher eventPublisher, final EventStore eventStore, final WebhookRegistry webhookRegistry, final AvatarProvider avatarProvider, final ApplicationFactory applicationFactory, final RecoveryModeService recoveryModeService, final AuthenticationOrderOptimizer authenticationOrderOptimizer, final AccessFilterFactory accessFilterFactory, final CrowdDarkFeatureManager crowdDarkFeatureManager) {
        return new RecoveryModeAwareApplicationService(directoryManager, searchStrategyFactory, permissionManager, eventPublisher, eventStore, webhookRegistry, avatarProvider, applicationFactory, recoveryModeService, authenticationOrderOptimizer, accessFilterFactory, crowdDarkFeatureManager);
    }

    @Bean
    public CrowdDirectoryService crowdDirectoryService(final CrowdEmbeddedApplicationFactory crowdEmbeddedApplicationFactory, final DelegatingDirectoryInstanceLoader delegatingDirectoryInstanceLoader, final RecoveryModeAwareDirectoryManager recoveryModeAwareDirectoryManager, final ApplicationManager applicationManager, final DirectoryValidatorFactory directoryValidatorFactory) {
        return new CrowdDirectoryServiceImpl(crowdEmbeddedApplicationFactory, delegatingDirectoryInstanceLoader, recoveryModeAwareDirectoryManager, applicationManager, directoryValidatorFactory);
    }

    @Bean
    public RecoveryModeService recoveryModeService(final DelegatingDirectoryInstanceLoader delegatingDirectoryInstanceLoader, @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection") final EventPublisher eventPublisher) {
        return new SystemPropertyRecoveryModeService(delegatingDirectoryInstanceLoader, eventPublisher);
    }

    @Bean
    public ClusterService clusterService() {
        return new ClusterServiceImpl();
    }

    @Bean
    public LicenseService licenseService() {
        return new LicenseServiceImpl();
    }

    // Manager
    @Bean
    public ApplicationManager applicationManager(final ApplicationDAO applicationDao, final PasswordEncoderFactory passwordEncoderFactory, @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection") final EventPublisher eventPublisher) {
        return new ApplicationManagerGeneric(applicationDao, passwordEncoderFactory, eventPublisher);
    }

    @Bean
    public RecoveryModeAwareDirectoryManager recoveryModeAwareDirectoryManager(final DirectoryDao directoryDao, final ApplicationDAO applicationDAO, @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection") final EventPublisher eventPublisher, final PermissionManager permissionManager, final DelegatingDirectoryInstanceLoader delegatingDirectoryInstanceLoader, final DirectorySynchroniser directorySynchroniser, final DirectoryPollerManager directoryPollerManager, @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection") final ClusterLockService clusterLockService, final SynchronisationStatusManager synchronisationStatusManager, final BeforeGroupRemoval beforeGroupRemoval, final RecoveryModeService recoveryModeService, final Optional<NestedGroupsCacheProvider> nestedGroupsCacheProvider, final LDAPPropertiesHelper ldapPropertiesHelper, final LdapConnectionPropertiesDiffResultMapper ldapConnectionPropertiesDiffResultMapper) {
        final MultiEventPublisher multiEventPublisher = new DelegatingMultiEventPublisher(eventPublisher);
        return new RecoveryModeAwareDirectoryManager(directoryDao, applicationDAO, multiEventPublisher, permissionManager, delegatingDirectoryInstanceLoader, directorySynchroniser, directoryPollerManager, clusterLockService, synchronisationStatusManager, beforeGroupRemoval, recoveryModeService, nestedGroupsCacheProvider, ldapPropertiesHelper, ldapConnectionPropertiesDiffResultMapper);
    }

    @Bean
    public DirectoryPollerManager directoryPollerManager(@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection") final SchedulerService schedulerService) {
        return new AtlassianSchedulerDirectoryPollerManager(schedulerService);
    }

    @Bean
    public PermissionManager permissionManager(final ApplicationDAO applicationDao, final DirectoryDao directoryDao, @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection") final EventPublisher eventPublisher) {
        return new PermissionManagerImpl(applicationDao, directoryDao, eventPublisher);
    }

    @Bean
    public PropertyManager propertyManager(final PropertyDAO propertyDAO, @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection") final EventPublisher eventPublisher) {
        return new PropertyManagerGeneric(propertyDAO, eventPublisher);
    }

    @Bean
    public CrowdDarkFeatureManager crowdDarkFeatureManager() {
        return new CrowdDarkFeatureManagerImpl(new LimitedSystemPropertyDarkFeatureManager());
    }

    @Bean
    public InternalSynchronisationStatusManager internalSynchronisationStatusManager(final DirectorySynchronisationInformationStore directorySynchronisationInformationStore, @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection") final EventPublisher eventPublisher, final DirectoryDao directoryDao, final Clock clock, final DirectorySynchronisationTokenStore directorySynchronisationTokenStore, final ClusterService clusterService) {
        return new SynchronisationStatusManagerImpl(directorySynchronisationInformationStore, eventPublisher, directoryDao, clock, directorySynchronisationTokenStore, clusterService);
    }

    // Authentication
    @Bean
    public AuthenticationOrderOptimizer authenticationOrderOptimizer(final UserDAOHibernate userDao, final RecoveryModeService recoveryModeService) {
        return new AuthenticationOrderOptimizer(userDao, recoveryModeService);
    }

    // Loader
    @Bean
    public InternalDirectoryInstanceLoader internalDirectoryInstanceLoader(final InstanceFactory instanceFactory) {
        return new InternalDirectoryInstanceLoaderImpl(instanceFactory);
    }

    @Bean
    public RecoveryModeDirectoryLoader recoveryModeDirectoryLoader() {
        return new RecoveryModeDirectoryLoader();
    }

    @Bean
    public AzureAdDirectoryInstanceLoader azureAdDirectoryInstanceLoader(final InstanceFactory instanceFactory) {
        return new AzureAdDirectoryInstanceLoaderImpl(instanceFactory);
    }

    @Bean
    public CustomDirectoryInstanceLoader customDirectoryInstanceLoader(final InstanceFactory instanceFactory) {
        return new CustomDirectoryInstanceLoader(instanceFactory);
    }

    @Bean
    LDAPDirectoryInstanceLoader ldapDirectoryInstanceLoader(final InstanceFactory instanceFactory) {
        return new LDAPDirectoryInstanceLoaderImpl(instanceFactory);
    }

    @Bean
    public RemoteCrowdDirectoryInstanceLoader remoteCrowdDirectoryInstanceLoader(final InstanceFactory instanceFactory) {
        return new RemoteCrowdDirectoryInstanceLoaderImpl(instanceFactory);
    }

    @Bean
    public DelegatingDirectoryInstanceLoader delegatingDirectoryInstanceLoader(final InternalDirectoryInstanceLoader internalDirectoryInstanceLoader, final LDAPDirectoryInstanceLoader ldapDirectoryInstanceLoader, final RemoteCrowdDirectoryInstanceLoader remoteCrowdDirectoryInstanceLoader, final AzureAdDirectoryInstanceLoader azureAdDirectoryInstanceLoader, final CustomDirectoryInstanceLoader customDirectoryInstanceLoader) {
        final List<DirectoryInstanceLoader> directoryInstanceLoaders = new ArrayList<>();
        directoryInstanceLoaders.add(internalDirectoryInstanceLoader);
        directoryInstanceLoaders.add(ldapDirectoryInstanceLoader);
        directoryInstanceLoaders.add(remoteCrowdDirectoryInstanceLoader);
        directoryInstanceLoaders.add(azureAdDirectoryInstanceLoader);
        directoryInstanceLoaders.add(customDirectoryInstanceLoader);
        return new DelegatingDirectoryInstanceLoaderImpl(directoryInstanceLoaders);
    }

    @Bean
    public DbCachingRemoteDirectoryInstanceLoader dbCachingRemoteDirectoryInstanceLoader(final DelegatingDirectoryInstanceLoader delegatingDirectoryInstanceLoader, final InternalDirectoryInstanceLoader internalDirectoryInstanceLoader, final DirectoryCacheFactory directoryCacheFactory, final CacheRefresherFactory cacheRefresherFactory, final AuditService auditService, final NoopAuditLogMapper noopAuditLogMapper, @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection") final EventPublisher eventPublisher, final DirectoryDao directoryDao) {
        return new DbCachingRemoteDirectoryInstanceLoader(delegatingDirectoryInstanceLoader, internalDirectoryInstanceLoader, directoryCacheFactory, cacheRefresherFactory, auditService, noopAuditLogMapper, noopAuditLogMapper, eventPublisher, directoryDao);
    }

    @Bean
    public DelegatedAuthenticationDirectoryInstanceLoader delegatedAuthenticationDirectoryInstanceLoader(final LDAPDirectoryInstanceLoader ldapDirectoryInstanceLoader, final InternalDirectoryInstanceLoader internalDirectoryInstanceLoader, @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection") final EventPublisher eventPublisher, final DirectoryDao directoryDao) {
        return new DelegatedAuthenticationDirectoryInstanceLoaderImpl(ldapDirectoryInstanceLoader, internalDirectoryInstanceLoader, eventPublisher, directoryDao);
    }

    // Factory
    @Bean
    CrowdEmbeddedApplicationFactory crowdEmbeddedApplicationFactory(final ApplicationDAO applicationDAO) {
        return new CrowdEmbeddedApplicationFactory(applicationDAO);
    }

    @Bean
    public CacheFactory cacheFactory() {
        return new MemoryCacheManager();
    }

    @Bean
    public DirectoryCacheFactory directoryCacheFactory(final DirectoryDao directoryDao, final SynchronisationStatusManager synchronisationStatusManager, @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection") final EventPublisher eventPublisher, final UserDAOHibernate userDao, final GroupDAOHibernate groupDao, final TransactionInterceptor transactionInterceptor, final CrowdDarkFeatureManager crowdDarkFeatureManager) {
        final MultiEventPublisher multiEventPublisher = new DelegatingMultiEventPublisher(eventPublisher);
        return new TransactionalDirectoryCacheFactory(directoryDao, synchronisationStatusManager, multiEventPublisher, userDao, groupDao, transactionInterceptor, crowdDarkFeatureManager);
    }

    @Bean
    public CacheRefresherFactory cacheRefresherFactory() {
        return new CacheRefresherFactoryImpl();
    }

    @Bean
    public DirectoryValidatorFactory directoryValidatorFactory(final I18nHelper i18nHelper) {
        return new DirectoryValidatorFactoryImpl(i18nHelper);
    }

    @Bean
    public SearchStrategyFactory searchStrategyFactory(final DirectoryManager directoryManager) {
        return new DefaultSearchStrategyFactory(directoryManager);
    }

    @Bean
    AccessFilterFactory accessFilterFactory(final DirectoryManager directoryManager, final DcLicenseChecker dcLicenseChecker) {
        return new AccessFilterFactoryImpl(directoryManager, dcLicenseChecker);
    }

    @Bean
    public DirectorySynchronisationTokenStore directorySynchronisationTokenStore(final DirectorySynchronisationTokenDao directorySynchronisationTokenDao) {
        return new InDatabaseDirectorySynchronisationTokenStore(directorySynchronisationTokenDao);
    }

    @Bean
    public DirectorySynchronisationInformationStore directorySynchronisationInformationStore(final DirectorySynchronisationStatusDao directorySynchronisationStatusDao, final DirectoryDao directoryDao, final ClusterService clusterService) {
        return new InDatabaseDirectorySynchronisationInformationStore(directorySynchronisationStatusDao, directoryDao, clusterService);
    }

    // DAO
    @Bean
    public TransactionalDirectoryDao transactionalDirectoryDao(final DirectoryDao directoryDao) {
        return new DirectoryDaoTransactionalDecorator(directoryDao);
    }

    // Synchroniser
    @Bean
    public DirectorySynchroniser directorySynchroniser(@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection") final ClusterLockService lockService, final TransactionalDirectoryDao transactionalDirectoryDao, final InternalSynchronisationStatusManager internalSynchronisationStatusManager, @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection") final EventPublisher eventPublisher, final AuditLogContext auditLogContext, final DirectorySynchronisationEventHelper syncEventHelper) {
        return new DirectorySynchroniserImpl(lockService, transactionalDirectoryDao, internalSynchronisationStatusManager, eventPublisher, auditLogContext, syncEventHelper);
    }

    @Bean
    public DirectorySynchronisationEventHelper directorySynchronisationEventHelper(final InternalSynchronisationStatusManager internalSynchronisationStatusManager, final AuditLogContext auditLogContext, @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection") final EventPublisher eventPublisher, final DelegatingDirectoryInstanceLoader delegatingDirectoryInstanceLoader) {
        return new DirectorySynchronisationEventHelper(internalSynchronisationStatusManager, auditLogContext, eventPublisher, delegatingDirectoryInstanceLoader);
    }

    // LDAP
    @Bean
    public ClusterMessageService clusterMessageService() {
        return new NoOpClusterMessageService();
    }

    @Bean
    public LdapDirectoryClearingClusterEventPublisher ldapDirectoryClearingClusterEventPublisher() {
        return new LdapDirectoryClearingClusterEventPublisherImpl();
    }

    @Bean
    public NestedGroupsCacheProvider nestedGroupsCacheProvider() {
        return new NestedGroupsCacheProvider(2000L, 100);
    }

    @Bean
    public BeforeGroupRemoval beforeGroupRemoval() {
        return new NoopBeforeGroupRemoval();
    }

    @Bean
    public DirectoryPollerJobRunner directoryPollerJobRunner(final RecoveryModeAwareDirectoryManager recoveryModeAwareDirectoryManager, final DirectorySynchroniser directorySynchroniser, final DelegatingDirectoryInstanceLoader delegatingDirectoryInstanceLoader, @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection") final SchedulerService schedulerService) {
        return new DirectoryPollerJobRunner(recoveryModeAwareDirectoryManager, directorySynchroniser, delegatingDirectoryInstanceLoader, schedulerService);
    }

    @Bean
    public AvatarProvider avatarProvider(final PropertyManager propertyManager) {
        return new WebServiceAvatarProvider(propertyManager, URI.create(ParameterUtils.getBaseUrl(UrlMode.ABSOLUTE)));
    }

    @Bean
    public WebhookRegistry webhookRegistry(final WebhookDAO webhookDAO) {
        return new WebhookRegistryImpl(webhookDAO);
    }

    @Bean
    public EventStore eventStore() {
        return new EventStoreGeneric(128);
    }

    @Bean
    public AutoGroupAdderListener autoGroupAdderListener(final DirectoryManager directoryManager, final DirectoryDefaultGroupMembershipResolver defaultGroupMembershipResolver, @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection") final EventPublisher eventPublisher) {
        final List<DefaultGroupMembershipResolver> defaultMembershipResolvers = new ArrayList<>();
        defaultMembershipResolvers.add(defaultGroupMembershipResolver);
        return new AutoGroupAdderListener(directoryManager, defaultMembershipResolvers, eventPublisher);
    }

    @Bean
    public DirectoryDefaultGroupMembershipResolver defaultGroupMembershipResolver(final DirectoryManager directoryManager) {
        return new DirectoryDefaultGroupMembershipResolver(directoryManager);
    }

    // Audit
    @Bean
    public AuditService auditService() {
        return new NoOpAuditService();
    }

    @Bean
    public AuditLogChangesetPopulator auditLogChangesetPopulator(final Clock clock, final AuditLogMetadataResolver auditLogMetadataResolver) {
        final AuditLogChangesetPopulatorImpl auditLogChangesetPopulator = new AuditLogChangesetPopulatorImpl(clock);
        auditLogChangesetPopulator.setAuditLogMetadataResolver(auditLogMetadataResolver);
        return auditLogChangesetPopulator;
    }

    @Bean
    public AuditLogContext auditLogContext() {
        return new NoOpAuditLogContext();
    }

    @Bean
    public NoopAuditLogMapper noopAuditLogMapper() {
        return new NoopAuditLogMapper();
    }

    @Bean
    public AuditLogGroupMapper auditLogGroupMapper() {
        return new NoopAuditLogMapper();
    }

    @Bean
    public AuditLogMetadataResolver auditLogMetadataResolver(final ThreadLocalStateAccessor threadLocalStateAccessor, final AuditLogContextInternal auditLogContextInternal) {
        return new AuditLogMetadataResolverImpl(threadLocalStateAccessor, auditLogContextInternal);
    }

    @Bean
    public CrowdThreadLocalStateAccessor crowdThreadLocalStateAccessor() {
        return new CrowdThreadLocalStateAccessor();
    }

    @Bean
    public AuditLogContextInternal auditLogContextInternal() {
        return new AuditLogContextInternalImpl();
    }

    @Bean
    public AuditLogEnabledChecker auditLogEnabledChecker(final AuditLogProperties auditLogSystemProperties, final AuditLogMetadataResolver auditLogMetadataResolver) {
        return new AuditLogEnabledCheckerImpl(auditLogSystemProperties, auditLogMetadataResolver);
    }

    @Bean
    public AuditLogSystemProperties auditLogSystemProperties() {
        return new AuditLogSystemProperties();
    }

    @Bean
    public AuditLogUserMapper auditLogUserMapper(final AuditLogDiffResultMapper diffResultMapper) {
        return new AuditLogUserMapperImpl(diffResultMapper);
    }

    @Bean
    public AuditLogDiffResultMapper diffResultMapper(final AuditLogProperties auditLogSystemProperties) {
        return new AuditLogDiffResultMapperImpl(auditLogSystemProperties);
    }

    // Encryptor
    @Bean
    public Encryptor encryptor() {
        return new Base64Encryptor();
    }

    @Bean
    public DirectoryPasswordsEncryptor directoryPasswordsEncryptor(final Encryptor encryptor) {
        return new DirectoryPasswordsEncryptor(encryptor);
    }

    @Bean
    public DcLicenseChecker dcLicenseChecker(final LicenseService licenseService) {
        return new DcLicenseCheckerImpl(licenseService);
    }

    @Bean
    public Clock clock() {
        return Clock.systemUTC();
    }
}
