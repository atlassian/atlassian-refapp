package it.com.atlassian.plugin.refimpl;

import com.atlassian.webdriver.refapp.page.RefappHomePage;
import com.atlassian.webdriver.refapp.page.RefappLoginPage;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class TestActivityStreams extends AbstractRefappTestCase {
    private RefappLoginPage loginPage;

    @Before
    public final void setUp() {
        loginPage = PRODUCT.gotoLoginPage();
    }

    @After
    public final void tearDown() {
        loginPage.getHeader()
                .logout(RefappHomePage.class);
    }

    @Test
    public void testActivityStreamsPageExistInHeaderWhenLoginAsAdmin() {
        RefappHomePage homePage = loginPage.login("betty", "betty", RefappHomePage.class);

        assertThat("The login must have been successful.", homePage.getHeader().isLoggedIn(), is(true));

        assertThat("Must have logged in as admin.", homePage.getHeader().isAdmin(), is(true));

        assertThat("Activity Streams must be existed in header.", homePage.getHeader().isAtActivityStreamsPage(), is(true));
    }

    @Test
    public void testActivityStreamsPageExistInHeaderWhenLoginAsSysAdmin() {
        RefappHomePage homePage = loginPage.login("admin", "admin", RefappHomePage.class);

        assertThat("The login must have been successful.", homePage.getHeader().isLoggedIn(), is(true));

        assertThat("Must have logged in as sysadmin.", homePage.getHeader().isSysadmin(), is(true));

        assertThat("Activity Streams must be existed in header.", homePage.getHeader().isAtActivityStreamsPage(), is(true));
    }

    @Test
    public void testActivityStreamsPageExistInHeaderWhenLoginAsUser() {
        RefappHomePage homePage = loginPage.login("barney", "barney", RefappHomePage.class);

        assertThat("The login must have been successful.", homePage.getHeader().isLoggedIn(), is(true));

        assertThat("Must not have logged in as admin.", homePage.getHeader().isAdmin(), is(false));

        assertThat("Must not have logged in as sysadmin.", homePage.getHeader().isSysadmin(), is(false));

        assertThat("Activity Streams must be existed in header.", homePage.getHeader().isAtActivityStreamsPage(), is(true));
    }

    @Test
    public void testActivityStreamsPageExistInHeaderWhenLogout() {
        loginPage = loginPage.login("barney", "barney", RefappLoginPage.class);

        RefappHomePage homePage = loginPage.getHeader().logout(RefappHomePage.class);

        assertThat("Must have logged out.", homePage.getHeader().isLoggedIn(), is(false));

        assertThat("Activity Streams must be existed in header when user logged out",
                homePage.getHeader().isAtActivityStreamsPage(), is(true));
    }

    @Test
    public void testActivityStreamsPageExistInHeader() {
        assertThat("Activity Streams must be existed in header", loginPage.getHeader().isAtActivityStreamsPage(), is(true));
    }
}
