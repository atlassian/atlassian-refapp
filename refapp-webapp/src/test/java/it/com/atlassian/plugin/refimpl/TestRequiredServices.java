package it.com.atlassian.plugin.refimpl;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginController;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.metadata.PluginMetadataManager;
import com.atlassian.refapp.api.ConnectionProvider;
import com.atlassian.scheduler.SchedulerService;
import com.atlassian.webdriver.refapp.page.RefappPluginIndexPage;
import org.junit.Test;

import java.util.List;
import java.util.Set;

import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;

/**
 * Integration test that checks the availability of certain services by scraping them from the list on index.jsp.
 */
public class TestRequiredServices extends AbstractRefappTestCase {

    private static final Class<?>[] EXPECTED_SERVICES = {
            ConnectionProvider.class,  // used by spring-scanner ProductFilterUtil to detect refapp
            EventPublisher.class,
            PluginAccessor.class,
            PluginController.class,
            PluginEventManager.class,
            PluginMetadataManager.class,
            SchedulerService.class
    };

    @Test
    public void testRequiredServicesAvailable() {
        // Arrange
        final RefappPluginIndexPage pluginIndexPage = PRODUCT.visit(RefappPluginIndexPage.class);

        // Act
        final Set<String> serviceInterfaces = pluginIndexPage.getServiceInterfaces();

        // Assert
        final List<String> missingServices = stream(EXPECTED_SERVICES)
                .map(Class::getName)
                .filter(serviceClass -> !serviceInterfaces.contains(serviceClass))
                .collect(toList());
        assertThat(missingServices, is(empty()));
    }
}
