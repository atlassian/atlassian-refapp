package it.com.atlassian.plugin.refimpl;

import com.atlassian.healthcheck.checks.plugin.PluginHealthCheckConstants;
import com.atlassian.healthcheck.testsupport.checks.plugin.PluginHealthCheckTestUtils.PluginHealthCheckResult;
import org.codehaus.jackson.JsonNode;
import org.junit.Test;

import java.util.LinkedHashSet;
import java.util.Set;

import static com.atlassian.healthcheck.checks.HealthCheckConstants.HEALTH_CHECK_FAILURE_REASON_FIELD;
import static com.atlassian.healthcheck.checks.HealthCheckConstants.HEALTH_CHECK_NAME_FIELD;
import static com.atlassian.healthcheck.testsupport.checks.plugin.PluginHealthCheckTestUtils.parsePluginHealthCheckResultFailureReason;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.junit.Assert.fail;

/**
 * Tests plugin and module enablement and startup state, using atlassian-healthcheck.
 * <p>
 * In current version of atlassian-healthcheck, these healthchecks don't fail by default, just log warnings
 * and return a successful result with failure reason (until we've rid Cloud of disabled-plugin snowflakes).
 * So we assert on the failure reason string here.
 *
 * @see TestHealthChecks
 */
public class TestPluginStartup {
    /**
     * Use this method for each item in atlassian-healthcheck-whitelist.txt that fails consistently every time.
     * This helps you come back and remove items from the whitelist if they start passing in future.
     */
    private static void expectWhitelistedFailure(final Set<String> actualWhitelistedItems, final String expectedWhitelistedItemToIgnore) {
        if (!actualWhitelistedItems.remove(expectedWhitelistedItemToIgnore)) {
            // Someone fixed the failure we were previously ignoring! Force it to be removed from the ignore list.
            fail("Item is no longer failing (yay!), so please remove its expectWhitelistedFailure call " + "so that we'll be able to detect future failures: '" + expectedWhitelistedItemToIgnore + "'");
        }
    }

    private static String getHealthCheckFailureReasonByName(final String name) {
        final JsonNode statusArray = HealthCheckTestUtils.getHealthChecksStatusArray();

        for (final JsonNode status : statusArray) {
            if (name.equals(status.get(HEALTH_CHECK_NAME_FIELD).asText())) {
                return status.get(HEALTH_CHECK_FAILURE_REASON_FIELD).asText();
            }
        }

        throw new AssertionError("No result for healthcheck: " + name);
    }

    private static Set<String> mutableCopy(final Set<String> input) {
        return new LinkedHashSet<>(input);
    }

    @Test
    public void testDisabledPluginsHealthCheck() {
        final String healthCheck = PluginHealthCheckConstants.DISABLED_PLUGINS_HEALTHCHECK;
        final String failureReason = getHealthCheckFailureReasonByName(healthCheck);

        final PluginHealthCheckResult result = parsePluginHealthCheckResultFailureReason(failureReason);

        final Set<String> failedPluginKeys = result.getFailedItems();
        assertThat(healthCheck + " healthcheck failed because these plugins are unexpectedly disabled: " + failedPluginKeys + ".\n\nFull failure reason: '" + failureReason + "'", failedPluginKeys, empty());

        final Set<String> whitelistedPluginKeys = mutableCopy(result.getWhitelistedItems());
        // If you add anything to atlassian-healthcheck-whitelist.txt, empty() will fail; fix by adding expectWhitelistedFailure here.
        // If they start passing again, expectWhitelistedFailure will fail, helping you come back and clean up here.

        expectWhitelistedFailure(whitelistedPluginKeys, "com.external.plugin.dmz-external-plugin");

        assertThat("New items have been added to atlassian-healthcheck-whitelist.txt, please add expectWhitelistedFailure for them. ", whitelistedPluginKeys, empty());
    }

    @Test
    public void testDisabledPluginModulesHealthCheck() {
        final String healthCheck = PluginHealthCheckConstants.DISABLED_PLUGIN_MODULES_HEALTHCHECK;
        final String failureReason = getHealthCheckFailureReasonByName(healthCheck);

        final PluginHealthCheckResult result = parsePluginHealthCheckResultFailureReason(failureReason);

        final Set<String> failedModuleKeys = result.getFailedItems();
        assertThat(healthCheck + " healthcheck failed because these plugin modules are unexpectedly disabled: " + failedModuleKeys + ".\n\nFull failure reason: '" + failureReason + "'", failedModuleKeys, empty());

        final Set<String> whitelistedModuleKeys = mutableCopy(result.getWhitelistedItems());

        // We don't care about these. See CANL-19
        expectWhitelistedFailure(whitelistedModuleKeys, "com.atlassian.plugins.atlassian-nav-links-plugin:atlassian-nav-links-whitelist");
        expectWhitelistedFailure(whitelistedModuleKeys, "com.atlassian.applinks.applinks-plugin:applinks-whitelist");
        expectWhitelistedFailure(whitelistedModuleKeys, "com.atlassian.upm.atlassian-universal-plugin-manager-plugin:analyticsWhitelist");
        expectWhitelistedFailure(whitelistedModuleKeys, "com.atlassian.templaterenderer.atlassian-template-renderer-velocity1.6-plugin:velocity16-analytic-events-allowlist");

        // We also don't care about these. See BSP-3556
        expectWhitelistedFailure(whitelistedModuleKeys, "com.atlassian.audit.atlassian-audit-plugin:audit-backend-analytics-whitelist");
        expectWhitelistedFailure(whitelistedModuleKeys, "com.atlassian.audit.atlassian-audit-plugin:audit-ui-analytics-whitelist");
        expectWhitelistedFailure(whitelistedModuleKeys, "com.atlassian.plugins.atlassian-plugins-webresource-rest:caching-analytics");
        expectWhitelistedFailure(whitelistedModuleKeys, "com.atlassian.plugins.atlassian-plugins-webresource-rest:resource-hash-analytics");
        expectWhitelistedFailure(whitelistedModuleKeys, "com.atlassian.soy.soy-template-plugin:soy-analytic-events-allowlist");
        expectWhitelistedFailure(whitelistedModuleKeys, "com.atlassian.streams:atlassianStreamsWhitelist");
        expectWhitelistedFailure(whitelistedModuleKeys, "com.atlassian.oauth2.oauth2-client-plugin:oauth2-client-analytics");

        // We also don't care about Jira-specific upgrade task factories (required for Jira ZDU)
        expectWhitelistedFailure(whitelistedModuleKeys, "com.atlassian.audit.atlassian-audit-plugin:audit-upgrade-task-factory");

        // If you add anything to atlassian-healthcheck-whitelist.txt, empty() will fail; fix by adding
        // expectWhitelistedFailure above.
        // If they start passing again, expectWhitelistedFailure will fail, helping you come back and clean up here.
        assertThat("New items have been added to atlassian-healthcheck-whitelist.txt, please add expectWhitelistedFailure for them. ", whitelistedModuleKeys, empty());
    }

    @Test
    public void testDisabledByDefaultPluginsHealthCheck() {
        final String healthCheck = PluginHealthCheckConstants.DISABLED_BY_DEFAULT_PLUGINS_HEALTHCHECK;
        final String failureReason = getHealthCheckFailureReasonByName(healthCheck);

        final PluginHealthCheckResult result = parsePluginHealthCheckResultFailureReason(failureReason);

        final Set<String> failedPluginKeys = result.getFailedItems();
        assertThat(healthCheck + " healthcheck failed because these disabled-by-default plugins are unexpectedly enabled. " + "Preferred fix: change the plugin to always be enabled and not have \"state='disabled'\" in its atlassian-plugin.xml: " + failedPluginKeys + ".\n\nFull failure reason: '" + failureReason + "'", failedPluginKeys, empty());

        final Set<String> whitelistedPluginKeys = mutableCopy(result.getWhitelistedItems());
        // If you add anything to atlassian-healthcheck-whitelist.txt, empty() will fail; fix by adding expectWhitelistedFailure here.
        // If they start passing again, expectWhitelistedFailure will fail, helping you come back and clean up here.
        assertThat("New items have been added to atlassian-healthcheck-whitelist.txt, please add expectWhitelistedFailure for them. ", whitelistedPluginKeys, empty());
    }

    @Test
    public void testDisabledByDefaultPluginModulesHealthCheck() {
        final String healthCheck = PluginHealthCheckConstants.DISABLED_BY_DEFAULT_PLUGIN_MODULES_HEALTHCHECK;
        final String failureReason = getHealthCheckFailureReasonByName(healthCheck);

        final PluginHealthCheckResult result = parsePluginHealthCheckResultFailureReason(failureReason);

        final Set<String> failedModuleKeys = result.getFailedItems();
        assertThat(healthCheck + " healthcheck failed because these disabled-by-default plugin modules are unexpectedly enabled. " + "Preferred fix: change the plugin to have all modules always enabled and not have any \"state='disabled'\" modules in its atlassian-plugin.xml: " + failedModuleKeys + ".\n\nFull failure reason: '" + failureReason + "'", failedModuleKeys, empty());

        final Set<String> whitelistedModuleKeys = mutableCopy(result.getWhitelistedItems());
        // If you add anything to atlassian-healthcheck-whitelist.txt, empty() will fail; fix by adding expectWhitelistedFailure here.
        // If they start passing again, expectWhitelistedFailure will fail, helping you come back and clean up here.
        assertThat("New items have been added to atlassian-healthcheck-whitelist.txt, please add expectWhitelistedFailure for them. ", whitelistedModuleKeys, empty());
    }
}
