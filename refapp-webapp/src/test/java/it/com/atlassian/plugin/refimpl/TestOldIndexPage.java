package it.com.atlassian.plugin.refimpl;

import com.atlassian.webdriver.refapp.page.RefappPluginIndexPage;
import org.junit.Test;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import static java.util.stream.Collectors.toSet;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;

/**
 * WebDriver test of the contents of the "old" index page (which is rendered by {@code index.jsp}).
 */
public class TestOldIndexPage extends AbstractRefappTestCase {

    @Test
    public void testNoBundlesAreOnlyInInstalledState() {
        Set<String> installedPlugins = new HashSet<>();
        // DMZ - External plugin accessing Jackson 2.x.
        // Jackson 2.x is accessible only for internal plugins.
        // dmz-external-plugin is in installed state, because of the BundleException
        // more can be found in the README of the dmz-external-plugin.
        installedPlugins.add("com.atlassian.refapp.dmz-external-plugin");

        // Arrange
        final RefappPluginIndexPage pluginIndexPage = PRODUCT.visit(RefappPluginIndexPage.class);

        // Act
        final Set<RefappPluginIndexPage.Bundle> bundles = pluginIndexPage.getBundles();

        // Assert
        assertFalse("Plugins should be present", bundles.isEmpty());
        final Collection<String> installedBundles = bundles.stream()
                .filter(bundle -> bundle.getState().equals("Installed"))
                .filter(bundle -> !installedPlugins.contains(bundle.getSymbolicName()))
                .map(bundle -> bundle.getSymbolicName() + " " + bundle.getVersion())
                .collect(toSet());
        assertThat("None of the plugins should be just 'Installed'", installedBundles, is(empty()));
    }
}
