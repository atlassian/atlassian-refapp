package it.com.atlassian.refapp.rest;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.jaxrs.JacksonJaxbJsonProvider;
import org.codehaus.jackson.map.ObjectMapper;
import org.eclipse.jetty.util.MultiPartWriter;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.Base64;

import static java.lang.String.format;

/**
 * Rest/Jersey utils, simplified from Confluence com.atlassian.confluence.it.RestHelper and com.atlassian.confluence.rest.client.RestClientFactory
 */
public final class RestUtils {
    private RestUtils() {}

    @Nonnull
    public static WebResource newResource(@Nonnull String url) {
        return newClient().resource(url);
    }

    public static JsonNode getJsonResponse(WebResource resource) {
        final String responseString = getJsonResponseString(resource);
        try {
            return new ObjectMapper().readTree(responseString);
        } catch (IOException e) {
            throw new RuntimeException("Could not parse json response from json string : " + responseString, e);
        }
    }

    public static String getJsonResponseString(WebResource resource) {
        final ClientResponse response = resource
                .header("Authorization", parseBasicAuthHeader("admin", "admin"))
                .get(ClientResponse.class);
        try {
            return response.hasEntity() ? response.getEntity(String.class) : null;
        } finally {
            response.close();
        }
    }

    private static Client newClient() {
        ClientConfig clientConfig = new DefaultClientConfig();
        clientConfig.getSingletons().add(new JacksonJaxbJsonProvider());
        clientConfig.getClasses().add(MultiPartWriter.class);
        return Client.create(clientConfig);
    }

    private static String parseBasicAuthHeader(String username, String password) {
        String encoded = Base64.getEncoder().encodeToString(format("%s:%s", username, password).getBytes());
        return format("Basic %s", encoded);
    }
}
