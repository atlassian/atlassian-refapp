package it.com.atlassian.refapp.rest;

import it.com.atlassian.refapp.RefappTestURLs;
import org.junit.After;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AccessResourceTest {

    public static final String ACCESS_RESOURCE_BASE_PATH = "rest/rest-sal/1/access";
    public static final String ACCESS_RESOURCE_ANONYMOUS_PERMISSION_PATH = "/anonymous";
    public static final String ACCESS_RESOURCE_UNLICENSED_PERMISSION_PATH = "/unlicensed";

    @Test
    public void testAccessResourceIsUpAndRunning() {
        String actualResponse = RestUtils.newResource(RefappTestURLs.BASEURL + ACCESS_RESOURCE_BASE_PATH).get(String.class);
        assertEquals("This is admin access resource", actualResponse);
    }

    @Test
    public void testAccessResourceCanSetGlobalAnonymousAccessPermissionToTrue() {
        RestUtils.newResource(RefappTestURLs.BASEURL + ACCESS_RESOURCE_BASE_PATH + ACCESS_RESOURCE_ANONYMOUS_PERMISSION_PATH + "/true").put();
        String actualResponse = RestUtils.newResource(RefappTestURLs.BASEURL + ACCESS_RESOURCE_BASE_PATH + ACCESS_RESOURCE_ANONYMOUS_PERMISSION_PATH).get(String.class);
        assertEquals("true", actualResponse);
    }

    @Test
    public void testAccessResourceCanSetGlobalAnonymousAccessPermissionToFalse() {
        RestUtils.newResource(RefappTestURLs.BASEURL + ACCESS_RESOURCE_BASE_PATH + ACCESS_RESOURCE_ANONYMOUS_PERMISSION_PATH + "/false").put();
        String actualResponse = RestUtils.newResource(RefappTestURLs.BASEURL + ACCESS_RESOURCE_BASE_PATH + ACCESS_RESOURCE_ANONYMOUS_PERMISSION_PATH).get(String.class);
        assertEquals("false", actualResponse);
    }

    @Test
    public void testAccessResourceCanSetGlobalUnlicensedAccessPermissionToTrue() {
        RestUtils.newResource(RefappTestURLs.BASEURL + ACCESS_RESOURCE_BASE_PATH + ACCESS_RESOURCE_UNLICENSED_PERMISSION_PATH + "/true").put();
        String actualResponse = RestUtils.newResource(RefappTestURLs.BASEURL + ACCESS_RESOURCE_BASE_PATH + ACCESS_RESOURCE_UNLICENSED_PERMISSION_PATH).get(String.class);
        assertEquals("true", actualResponse);
    }

    @Test
    public void testAccessResourceCanSetGlobalUnlicensedAccessPermissionToFalse() {
        RestUtils.newResource(RefappTestURLs.BASEURL + ACCESS_RESOURCE_BASE_PATH + ACCESS_RESOURCE_UNLICENSED_PERMISSION_PATH + "/false").put();
        String actualResponse = RestUtils.newResource(RefappTestURLs.BASEURL + ACCESS_RESOURCE_BASE_PATH + ACCESS_RESOURCE_UNLICENSED_PERMISSION_PATH).get(String.class);
        assertEquals("false", actualResponse);
    }

    @After
    public void tearDown() {
        RestUtils.newResource(RefappTestURLs.BASEURL + ACCESS_RESOURCE_BASE_PATH + ACCESS_RESOURCE_ANONYMOUS_PERMISSION_PATH + "/false").put();
        RestUtils.newResource(RefappTestURLs.BASEURL + ACCESS_RESOURCE_BASE_PATH + ACCESS_RESOURCE_UNLICENSED_PERMISSION_PATH + "/false").put();
    }
}
