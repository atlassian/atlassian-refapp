package com.atlassian.webdriver.refapp.page;

import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.page.HomePage;
import com.atlassian.pageobjects.page.LoginPage;

import javax.inject.Inject;

/**
 *
 */
public class RefappLoginPage extends RefappAbstractPage implements LoginPage {
    @Inject
    PageBinder pageBinder;

    @ElementBy(name = "os_username")
    private PageElement usernameField;
    @ElementBy(name = "os_password")
    private PageElement passwordField;
    @ElementBy(id = "os_login")
    private PageElement loginButton;
    @ElementBy(name = "os_websudo")
    private PageElement webSudoCheckbox;

    public String getUrl() {
        return "/plugins/servlet/login";
    }

    public <M extends Page> M login(String username, String password, Class<M> nextPage) {
        return login(username, password, nextPage, new Object[0]);
    }

    public <M extends Page> M login(String username, String password, Class<M> nextPage, Object... pageArgs) {
        usernameField.type(username);
        passwordField.type(password);
        loginButton.click();

        return HomePage.class.isAssignableFrom(nextPage) ? pageBinder.bind(nextPage, pageArgs) : pageBinder.navigateToAndBind(nextPage, pageArgs);
    }

    /**
     * Logins into refapp.
     *
     * @param bypassWebsudo if true, user will not have to deal with websudo screen again until the automatically given privilege is expired
     */
    public <M extends Page> M login(String username, String password, boolean bypassWebsudo, Class<M> nextPage,  Object... pageArgs) {
        usernameField.type(username);
        passwordField.type(password);

        if (bypassWebsudo) {
            if (!webSudoCheckbox.isSelected()) {
                webSudoCheckbox.click();
            }
        } else if (webSudoCheckbox.isSelected()) {
            webSudoCheckbox.click();
        }

        loginButton.click();

        return HomePage.class.isAssignableFrom(nextPage) ? pageBinder.bind(nextPage, pageArgs) : pageBinder.navigateToAndBind(nextPage, pageArgs);
    }

    public <M extends Page> M loginAsSysAdmin(Class<M> nextPage) {
        return login("admin", "admin", nextPage);
    }

}
