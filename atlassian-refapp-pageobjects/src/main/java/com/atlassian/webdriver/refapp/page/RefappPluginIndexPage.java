package com.atlassian.webdriver.refapp.page;

import com.atlassian.pageobjects.elements.PageElement;
import org.openqa.selenium.By;

import javax.annotation.Nonnull;
import java.util.HashSet;
import java.util.Set;

import static java.util.Collections.unmodifiableSet;
import static java.util.stream.Collectors.toSet;

/**
 * Page object for the "old" index page, rendered by {@code index.jsp}.
 */
public class RefappPluginIndexPage extends RefappAbstractPage {

    public String getUrl() {
        return "/index.jsp";
    }

    /**
     * Returns the plugin keys listed on this page.
     *
     * @return see description
     */
    @Nonnull
    public Set<String> getPluginKeys() {
        return elementFinder.findAll(By.className("plugin-key")).stream()
                .map(PageElement::getText)
                .collect(toSet());
    }

    /**
     * Returns the OSGi bundles listed on this page.
     *
     * @return see description
     */
    public Set<Bundle> getBundles() {
        final Set<Bundle> bundles = new HashSet<>();
        for (final PageElement element : elementFinder.findAll(By.className("bundle"))) {
            final String symbolicName = element.find(By.className("bundle-symbolic-name")).getText();
            final String version = element.find(By.className("bundle-version")).getText();
            final String state = element.find(By.className("bundle-state")).getText();
            final Set<String> serviceInterfaces = new HashSet<>();
            for (PageElement interfaceElement : element.findAll(By.className("bundle-service-interface"))) {
                serviceInterfaces.add(interfaceElement.getText());
            }
            bundles.add(new Bundle(symbolicName, version, state, serviceInterfaces));
        }
        return bundles;
    }

    /**
     * Returns the services provided by the bundles listed on this page.
     *
     * @return the interface class names
     */
    public Set<String> getServiceInterfaces() {
        return getBundles().stream()
                .flatMap(bundle -> bundle.getServiceInterfaces().stream())
                .collect(toSet());
    }

    /**
     * Data transfer object for an OSGi bundle.
     */
    public static class Bundle {

        private final String symbolicName;
        private final String version;
        private final String state;
        private final Set<String> serviceInterfaces;

        public Bundle(final String symbolicName, final String version, final String state,
                      final Set<String> serviceInterfaces) {
            this.symbolicName = symbolicName;
            this.version = version;
            this.state = state;
            this.serviceInterfaces = unmodifiableSet(serviceInterfaces);
        }

        public String getSymbolicName() {
            return symbolicName;
        }

        public String getVersion() {
            return version;
        }

        public String getState() {
            return state;
        }

        public Set<String> getServiceInterfaces() {
            return serviceInterfaces;
        }
    }
}
