package com.atlassian.webdriver.refapp.page;

import com.atlassian.pageobjects.page.HomePage;
import com.atlassian.webdriver.refapp.component.RefappHeader;

/**
 *
 */
public class RefappUsersPage extends RefappAbstractPage implements HomePage<RefappHeader> {
    public String getUrl() {
        return "/plugins/servlet/users";
    }

    public boolean isAtUsersPage() {
        return "Users".equals(driver.getTitle());
    }
}
