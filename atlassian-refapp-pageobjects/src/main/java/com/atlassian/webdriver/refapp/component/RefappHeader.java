package com.atlassian.webdriver.refapp.component;

import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.component.Header;
import com.atlassian.pageobjects.component.WebSudoBanner;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.WebDriverElement;
import com.atlassian.pageobjects.page.HomePage;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;

import javax.inject.Inject;
import java.util.List;
import java.util.Locale;

/**
 *
 */
public class RefappHeader extends WebDriverElement implements Header {

    @Inject
    protected WebDriver driver;

    @Inject
    protected PageBinder pageBinder;

    @ElementBy(id = "login")
    private PageElement loginElement;
    @ElementBy(id = "websudo-banner")
    private PageElement webSudoBanner;
    @ElementBy(id = "websudo-drop")
    private PageElement webSudoDropButton;

    public RefappHeader() {
        super(By.tagName("body"));
    }

    public boolean isAtActivityStreamsPage() {
        final List<PageElement> activityStreamsLinks = findAll(By.id("activityStreamsLink"));

        if (activityStreamsLinks.isEmpty() || activityStreamsLinks.size() > 1) {
            return false;
        }

        return "Activity Streams".equals(activityStreamsLinks.iterator().next().getText());
    }

    public boolean isLoggedIn() {
        String text = loginElement.getText();
        return text != null && text.equals("Logout");
    }

    public boolean isAdmin() {
        return loggedInFullNameContains("admin");
    }

    public boolean isSysadmin() {
        return loggedInFullNameContains("sysadmin");
    }

    private boolean loggedInFullNameContains(String text) {
        if (isLoggedIn()) {
            final String userLowerCase = find(By.id("user")).getText().toLowerCase(Locale.ENGLISH);
            return userLowerCase.contains(text);
        }
        return false;
    }

    public <M extends Page> M logout(Class<M> nextPage) {
        if (isLoggedIn()) {
            loginElement.click();
        }
        return HomePage.class.isAssignableFrom(nextPage) ? pageBinder.bind(nextPage) :
                pageBinder.navigateToAndBind(nextPage);
    }

    @Override
    public WebSudoBanner getWebSudoBanner() {
        return new WebSudoBanner() {

            @Override
            public boolean isShowing() {
                try {
                    return webSudoBanner.isPresent();
                } catch (NoSuchElementException nsee) {
                    return false;
                }
            }

            @Override
            public String getMessage() {
                try {
                    return webSudoBanner.getText();
                } catch (NoSuchElementException nsee) {
                    return null;
                }
            }

            /**
             * Drops the websudo privilege if the websudo banner is displayed otherwise just navigates
             * to the next page.
             * @param nextPage the page to navigate to after websudo privileges have been dropped.
             * @return The nextPage pageObject
             */
            @Override
            public <P extends Page> P dropWebSudo(Class<P> nextPage) {
                if (webSudoDropButton.isPresent()) {
                    webSudoDropButton.click();
                    return HomePage.class.isAssignableFrom(nextPage) ? pageBinder.bind(nextPage) :
                            pageBinder.navigateToAndBind(nextPage);
                } else {
                    return pageBinder.navigateToAndBind(nextPage);
                }
            }
        };
    }
}
