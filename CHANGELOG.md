# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [6.2.8]
- 
- [BSP-5331] Fix translations coming from the WRM

## [6.1.22]

- [BSP-5331] Fix translations coming from the WRM

## [6.1.12]

- [BSP-5035] Fix rendering blowing up because of `I18nResolver`s fetching translations from disabled plugins
- [BSP-5034] Fix `ResourceBundleResolver` not throwing an NPE on null params
- [BSP-5033] Provide a default I18n resolver for web-items instead of warning about the lack of it 

## [5.5.0]

-   REFAPP-540: Overriding SAL to version `4.8.1`.

## [5.4.0]

-   ITAS-113: Add atlassian-profiling at version `3.6.1`
-   REFAPP-521: Upgrade WRM to version `5.4.6`.

## [5.3.0]

-   SPFE-517: Upgrade CSE runtime and modules to the version `2.2.0`

## [5.2.3]

-   SPFE-405: Avoid bundling CSE runtime FE module

## [5.2.2]

## [5.2.1]

## [5.2.0]

-   Added Client-Side Extensions runtime.
-   The home and admin pages are now built in React.
-   The extensions to these pages are now client-side extensions.
-   REFAPP-510 Implementation of SAL's ApplicationProperties.getVersion().
-   REFAPP-514 Allowed changes to batching configuration at runtime.
-   BSP-1994 bump atlassian-upm version (security)
-   BSP-1997 bump platform-pom version (security)

## [5.1.3]

## [5.1.2]

## [5.1.1]

## [5.0.0]

## [5.0.6] - 2020-04-28

### Fixed

-   BSP-1195 Bumped minor version of commons-codec to 1.13

## [5.0.5] - 2019-12-05

### Changed

-   REFAPP-509 Added AMD shim for jQuery

### Fixed

-   BSP-696 Bumped bugfix version of platform POM, to 5.0.19, to fix semver violations in 5.0.12

## [5.0.4] - 2019-08-27

### Fixed

-   BSP-527 `web-item-plugin` now exposes `DynamicWebInterfaceManager` as an OSGi service

## [5.0.3] - 2019-08-05

### Changed

-   BSP-504 Bumped minor version of AUI, to 8.3.4
-   BSP-504 Bumped minor version of jslibs, to 1.4.1

### Fixed

-   BSP-504 Bumped bugfix version of platform POM, to 5.0.10
-   BSP-504 Bumped bugfix version of Spring Scanner, to 2.1.10
-   BSP-504 Bumped bugfix version of UPM, to 4.0.4
-   BSP-504 Fixed `$.browser` by adding `jquery` plugin to resource batch

## [5.0.2] - 2019-04-12

### Changed

-   Exposed `SchedulerHistoryService` as an OSGi service
-   BSP-370 Bumped bugfix version of platform POM, to 5.0.2

## [5.0.1] - 2019-04-02

### Changed

-   BSP-367 Bumped bugfix version of platform POM, to 5.0.1

## [5.0.0] - 2019-01-09

### Changed

-   REFAPP-505 Server Platform 5.0 compatible, supports Java 11

## [3.3.11] - 2018-05-09

### Fixed

-   [KYAK-66]: Fixed `atlassian-refapp-pageobjects` compatibility with new Ubuntu 16.04 build agents

[6.2.8]: https://bitbucket.org/atlassian/atlassian-refapp/compare/atlassian-refapp-parent-6.2.8%0Datlassian-refapp-parent-6.2.7
[6.1.22]: https://bitbucket.org/atlassian/atlassian-refapp/compare/atlassian-refapp-parent-6.1.22%0Datlassian-refapp-parent-6.1.21
[5.4.0]: https://bitbucket.org/atlassian/atlassian-refapp/compare/atlassian-refapp-parent-5.4.0%0Datlassian-refapp-parent-5.3.0
[5.1.3]: https://bitbucket.org/atlassian/atlassian-refapp/compare/atlassian-refapp-parent-5.1.3%0Datlassian-refapp-parent-5.1.2
[3.3.11]: https://bitbucket.org/atlassian/atlassian-refapp/compare/atlassian-refapp-parent-3.3.11%0Datlassian-refapp-parent-3.3.9

[KYAK-66](https://bulldog.internal.atlassian.com/browse/KYAK-66)
[BSP-5033](https://bulldog.internal.atlassian.com/browse/BSP-5033)
[BSP-5034](https://bulldog.internal.atlassian.com/browse/BSP-5034)
[BSP-5035](https://bulldog.internal.atlassian.com/browse/BSP-5035)
[BSP-5331](https://bulldog.internal.atlassian.com/browse/BSP-5331)
