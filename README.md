# DEPRECATED
This repository was moved to the [DC Platform](https://bitbucket.org/server-platform/dc-platform/src/master/).

If you need to update older branches (`5.3.x`, `5.4.x`, `6.0.x`) you can do it here, but please add the same changes in the new repository as well. 
For all other changes targetting newer branches please use the new repository.

# Atlassian Reference Application (REFAPP)

## Description

The Atlassian Reference Application, or reference implementation, is known as the 'RefApp'. It is a
model implementation of the Atlassian Plugin Framework 2. It takes the form of a simple web
application, which implements the plugin framework and does not do much more than that.

## Ownership

The DC Core Back-End Platform team ([go/abracadabra](https://hello.atlassian.net/wiki/spaces/AB/overview)) owns this
project.

## Atlassian Developer?

All processes, guides, plans, and notes can be found here:

* [All Documentation](https://hello.atlassian.net/wiki/spaces/AB/pages/2405671031/Atlassian+Reference+Application+REFAPP)

Quick links:

* [Committing - The Platform Rules of Engagement (go/proe)](http://go.atlassian.com/proe)
* [EcoBAC - Bamboo build plans for RefApp](https://ecosystem-bamboo.internal.atlassian.com/browse/REFAPP)

## External User?

* [Issues](https://ecosystem.atlassian.net/browse/REFAPP)
* [Docs](https://developer.atlassian.com/display/DOCS/About+the+Atlassian+RefApp)

## Quick start

* `cd <project root>`
* If you haven't yet done so, [set up your Atlassian maven environment](https://bitbucket.org/atlassian/maven-settings/)
* `mvn clean install`
  > Note: If you're using a macOS with an M1 chip or newer and need to run end-to-end tests, please specify the use of
  the Chrome browser by adding `-Dwebdriver.browser=chrome` to your test configuration. For
  example `mvn clean install -Dwebdriver.browser=chrome` This is because the Firefox browser, which is used by default
  in Selenium, is not supported on these systems.
* `cd refapp-webapp`
* `mvn amps:run`
* To run a specific Maven execution defined in pom, use `mvn amps:run@<execution-id>`
* Initially, logging is directed to the terminal. However, once RefApp has started successfully, the logging can be
  found in [refapp-webapp/target/refapp.log](refapp-webapp/target/refapp.log)
* To interact with the product, browse to [http://localhost:5990/refapp/](http://localhost:5990/refapp/)
* To log in to RefApp, RefApp has a default user:
    * Username: admin
    * Password: admin

## Branches

* master: for new features and/or breaking changes
* refapp-6.1.x: uses the new-platform 0.2.x, and tracks the current publicly consumable release of RefApp
* refapp-6.0.x: uses the new-platform 0.1.x, and tracks the current publicly consumable release of RefApp

## Running Tests and Debugging

### Running all tests

* `mvn clean verify` runs all tests including unit tests and integration tests
* The integration test results can be found in the
  directory: [refapp-webapp/target/failsafe-reports](refapp-webapp/target/failsafe-reports)

### Running integration tests

The directory [refapp-webapp/src/test](refapp-webapp/src/test) contains integration tests and the remote test runner
[TestRunner.class](refapp-webapp/src/test/java/it/com/atlassian/plugin/refimpl/TestRunner.java).  
`TestRunner` will find and execute
all integration tests in the test plugin [refapp-test-plugin](refapp-plugins/refapp-test-plugin) by using
[Func Test plugin](https://bitbucket.org/atlassian/functest-plugin/src/master/).

RefApp uses [Func Test plugin](https://bitbucket.org/atlassian/functest-plugin/src/master/)

* Run all tests in a test class:
    * Switch directory to `refapp-webapp` module: `cd refapp-webapp`
    * `mvn clean verify -Dit.test="{className}"`
    * Example: `mvn clean verify -Dit.test="TestRunner"`
* Run single test in a test class:
    * Switch directory to `refapp-webapp` module: `cd refapp-webapp`
    * `mvn clean verify -Dit.test="{className}#{testName}"`
    * Example: `mvn clean verify -Dit.test="TestLogin#testUserLogin"`
  > Note: This command doesn't apply to running single test for functional integration tests
  in [refapp-test-plugin](refapp-plugins/refapp-test-plugin)
  > via [TestRunner](refapp-webapp/src/test/java/it/com/atlassian/plugin/refimpl/TestRunner.java).  
  > Please see below section [Running functional integration tests from UI or endpoints
  ](#markdown-header-running-functional-integration-tests-from-ui-or-endpoints).

### Debugging integration tests

* `mvn clean install` or `mvn clean install -Dwebdriver.browser=chrome` to build RefApp
* `cd refapp-webapp`
* `mvn amps:debug`
* Attach your debugger to the product and put breakpoints in your test cases
    ```text
    Example - set up debugger for Intellij:  
    - In Run/Debug Configurations  
    - Create Remote JVM Debug  
    - Set up host and port  
        Host: localhost   
        Port: 5005 (port is defined in the pom property <jvmargs.debug.template>)  
    - Apply and click the debug button from Intellij to attach the debugger to RefApp  
    ```
* Debug your test runner class as a normal JUnit test by clicking the debug icon of a test
  > Note: If you're using a macOS with an M1 chip or newer and need to debug end-to-end tests, please specify the use of
  the Chrome browser by adding `-Dwebdriver.browser=chrome` to your test running configuration and in VM options.

> Note: If you want to skip all tests while building, you need to use `-Dmaven.test.skip.exec=true` instead of
> `-DskipTests=true` otherwise the plugin test resources won't build.  
> For example: `mvn clean install -Dmaven.test.skip.exec=true`

### Running functional integration tests from UI or endpoints <a id="markdown-header-running-functional-integration-tests-from-ui-or-endpoints"></a>

The [Func Test plugin](https://bitbucket.org/atlassian/functest-plugin/src/master/) provides a REST endpoint that lets
you execute the tests defined by a given `<junit>` module. The
infrastructure for this is provided by the Func Test plugin's RemoteTestRunner client.  
The RemoteTestRunner uses the base URL to pass the test group name to the Func Test plugin, via a REST endpoint in that
plugin.

* Switch directory to refapp-webapp module `cd refapp-webapp`
* Boot up RefApp `mvn amps:run`
* Point your browser
  to [http://localhost:5990/refapp/plugins/servlet/functest](http://localhost:5990/refapp/plugins/servlet/functest), you
  can see all functional integration tests in the browser
* Run all functional integration tests via the endpoint:  
  [http://localhost:5990/refapp/rest/functest/1.0/junit/runTests?outdir=target](http://localhost:5990/refapp/rest/functest/1.0/junit/runTests?outdir=target)
* Run single functional integration test via an endpoint:
    * With xml output:
        * `http://localhost:5990/refapp/rest/functest/1.0/junit/runTests?outdir=target&includes={nameOfPackageOfClass}`
        * Example: [http://localhost:5990/refapp/rest/functest/1.0/junit/runTests?outdir=target&includes=com.
          atlassian.refapp.test.plugin.ao.ActiveObjectsSmokeTest](http://localhost:5990/refapp/rest/functest/1.0/junit/runTests?outdir=target&includes=com.atlassian.refapp.test.plugin.ao.ActiveObjectsSmokeTest)
    * With json output:
        * `http://localhost:5990/refapp/rest/functest/1.0/junit/runTests.json?outdir=target&includes={nameOfPackageOfClass}`
        * Example: [http://localhost:5990/refapp/rest/functest/1.0/junit/runTests.json?outdir=target&includes=com.
          atlassian.refapp.test.plugin.ao.ActiveObjectsSmokeTest](http://localhost:5990/refapp/rest/functest/1.0/junit/runTests.json?outdir=target&includes=com.atlassian.refapp.test.plugin.ao.ActiveObjectsSmokeTest)

### Writing and Debugging integration tests with Quick Reload (reload code changes faster)

QuickReload is an Atlassian P2 plugin to help you build Atlassian P2 plugins quicker. It works by watching output
directories for P2 .jar files to be created, then uploads them into the running Atlassian host application. It aims to
provide the smallest possible time between a piece of code being compiled and linked, to it being loaded and run in the
host application.

* Enable QuickReload by setting `<enableQuickReload>true</enableQuickReload>` in `amps-maven-plugin` section
  in [refapp-webapp/pom.xml](refapp-webapp/pom.xml)
* Compile RefApp from the root directory: `mvn clean install -Dmaven.test.skip.exec=true`
* Switch directory to `refapp-webapp` module: `cd refapp-webapp`
* Debug RefApp `mvn amps:debug`
* In another terminal, switch to `refapp-test-plugin` module: `cd refapp-plugins/refapp-test-plugin`
* Make changes to functional integration tests or write a new one
  in [refapp-test-plugin](refapp-plugins/refapp-test-plugin)
* Run `mvn package`, it only takes 3 to 5 seconds to finish recompile.
* in the browser, go
  to [http://localhost:5990/refapp/plugins/servlet/functest](http://localhost:5990/refapp/plugins/servlet/functest) and
  refresh the page
* The integration tests will appear and the changes will be deployed

Please refer to
the [page](https://hello.atlassian.net/wiki/spaces/AB/pages/2414519675/2023+-+Guide+of+how+to+write+and+debug+integration+tests+in+Refapp+fast+way+with+Quick+Reload)
for more details.

## Embedded Database

By default, RefApp uses H2 as embedded database unless the user specifies an external database via system properties

* `cd refapp-webapp`
* Run `java -jar target/atlassian-refapp-{REFAPP_VERSION}/WEB-INF/lib/h2-{H2_VERSION}.jar`.
  Example: `java -jar target/atlassian-refapp-6.3.0-SNAPSHOT/WEB-INF/lib/h2-1.4.200.jar`
* You can see H2 database login page shows up in the browser
* Log in with default information

## External Database

RefApp will spawn an in-process H2 database on an available port. The JDBC URL, username, and password for connecting to
this database will be output to the logs.

Alternatively, the user may specify an actual external database via Java system properties. See
`com.atlassian.refapp.api.ConnectionProvider` for more information.

An example AMPS configuration follows, which will set these properties for an external PostgreSQL database:

```xml

<plugin>
    <groupId>com.atlassian.maven.plugins</groupId>
    <artifactId>amps-maven-plugin</artifactId>
    ...
    <configuration>
        ...
        <systemProperties>
            <refapp.jdbc.external>true</refapp.jdbc.external>
            <refapp.jdbc.driver.class.name>org.postgresql.Driver</refapp.jdbc.driver.class.name>
            <refapp.jdbc.app.url>jdbc:postgresql://localhost:5432/refappdb</refapp.jdbc.app.url>
            <refapp.jdbc.app.schema>refappschema</refapp.jdbc.app.schema>
            <refapp.jdbc.app.user>refappuser</refapp.jdbc.app.user>
            <refapp.jdbc.app.pass>refapppass</refapp.jdbc.app.pass>
        </systemProperties>
        <libArtifacts>
            <libArtifact>
                <groupId>org.postgresql</groupId>
                <artifactId>postgresql</artifactId>
                <version>42.2.18</version>
            </libArtifact>
        </libArtifacts>
    </configuration>
</plugin>
```

For the local RefApp dev loop, you can apply the above configuration by adding `-Pexternal-postgres` to your `amps:run`
or `amps:debug` command (see above).

Note that when using an external database, the user should ensure that it's available. This can be easily done via the
[sql-maven-plugin](http://www.mojohaus.org/sql-maven-plugin/index.html). If the database requires a schema, RefApp will
create that for itself if necessary.

The following example will provision an external PostgreSQL database, as per the above configuration:

```xml

<plugin>
    <groupId>org.codehaus.mojo</groupId>
    <artifactId>sql-maven-plugin</artifactId>
    <version>1.5</version>

    <configuration>
        <skip>false</skip>
        <driver>org.postgresql.Driver</driver>
        <url>jdbc:postgresql://localhost:5432/postgres</url>
        <username>postgres</username>
        <password>postgres</password>
        <autocommit>true</autocommit>
    </configuration>

    <executions>
        <!-- first invocation - drop the existing database if it's there -->
        <execution>
            <id>drop-db-before-test</id>
            <phase>process-test-resources</phase>
            <goals>
                <goal>execute</goal>
            </goals>
            <configuration>
                <sqlCommand>
                    drop database if exists refappdb;
                    drop user if exists refappuser;
                </sqlCommand>
            </configuration>
        </execution>
        <!-- second invocation - create the database -->
        <execution>
            <id>create-db-before-test</id>
            <phase>process-test-resources</phase>
            <goals>
                <goal>execute</goal>
            </goals>
            <configuration>
                <sqlCommand>
                    create user refappuser with password 'refapppass';
                    create database refappdb with owner refappuser;
                </sqlCommand>
            </configuration>
        </execution>
    </executions>

    <dependencies>
        <dependency>
            <groupId>org.postgresql</groupId>
            <artifactId>postgresql</artifactId>
            <version>9.4.1207</version>
            <scope>compile</scope>
        </dependency>
    </dependencies>
</plugin>
```

See the root [pom.xml](pom.xml) in this project for examples using different database vendors.
